import qbs

import Reusables

Reusables.PublicLibrary {
    Depends { name: "qmlplugin" }

    targetName: qmlplugin.pluginName

    type: base.concat(["qmldir", "qmltypes"])
}
