import qbs

DynamicLibrary {
    Depends { name: "cpp" }

    FileTagger {
        patterns: "*.h"
        fileTags: ["headers"]
    }

    Group {
        fileTagsFilter: ["headers"]
        qbs.install: true
        qbs.installDir: "include/" + product.name
    }

    FileTagger {
        patterns: "*_p.h"
        fileTags: ["private_headers"]
    }

    Group {
        fileTagsFilter: ["private_headers"]
        qbs.install: true
        qbs.installDir: "include/" + product.name + "/private"
    }
}
