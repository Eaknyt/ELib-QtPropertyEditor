import qbs

CppApplication {
    property bool isGuiTest : false
    property bool isQuickTest : false

    cpp.cxxLanguageVersion: project.cppLanguageVersion
    cpp.includePaths: project.includePath
    cpp.libraryPaths: project.libraryAbsInstallPath
    cpp.rpaths: project.libraryAbsInstallPath

    // Qt deps
    Depends {
        name: "Qt"
        submodules: ["core", "testlib"]
    }

    Depends {
        name: "Qt"
        submodules: ["gui", "widgets"]
        condition: isGuiTest
    }

    Depends {
        name: "Qt"
        submodules: ["qml", "quick", "quicktest"]
        condition: isQuickTest
    }

    // Inner deps
    Depends {
        name: "PECore"
    }

    // Groups
    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "tests"
    }
}
