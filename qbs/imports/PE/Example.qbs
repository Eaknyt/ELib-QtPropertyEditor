import qbs

CppApplication {
    id: root

    property var qmlImportPaths: [ qbs.installRoot + "/qml" ]

    cpp.cxxLanguageVersion: project.cppLanguageVersion
    cpp.includePaths: project.includePath
    cpp.libraryPaths: project.libraryAbsInstallPath
    cpp.rpaths: project.libraryAbsInstallPath

    qbs.installDir: project.examplesDir + "/" + name

    FileTagger {
        patterns: "*.*"
        fileTags: ["examplesources"]
    }

    Group {
        fileTagsFilter: ["examplesources"]
        qbs.install: true
        qbs.installDir: root.qbs.installDir
        qbs.installSourceBase: "."
    }

    Group {
        condition: project.installExampleBinaries
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: root.qbs.installDir
    }
}
