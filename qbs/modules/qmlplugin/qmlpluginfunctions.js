function uriToPath(uri)
{
    var ret = uri.replace(/\./g, "/");

    return ret;
}

function libraryTarget(uri)
{
    var ret = uri.replace(/\./g, "").toLowerCase() + "plugin";

    return ret;
}
