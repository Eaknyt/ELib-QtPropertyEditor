import qbs
import qbs.Environment
import qbs.FileInfo
import qbs.ModUtils
import qbs.TextFile

import "qmlpluginfunctions.js" as QmlPluginFuncs

Module {
    property string uri : ""
    property string pluginClassName : ""

    property string pluginInstallRoot : "qml"
    property bool genQmlTypes : true
    property bool useControlsStyles : false // useful for QtQuickControl 1.x

    readonly property string uriPath : QmlPluginFuncs.uriToPath(uri)
    readonly property string pluginName : QmlPluginFuncs.libraryTarget(uri)
    readonly property string pluginInstallDir : pluginInstallRoot + '/' + uriPath

    validate: {
        var validator = new ModUtils.PropertyValidator("qmlplugin");
        validator.setRequiredProperty("uri", uri);
        validator.setRequiredProperty("pluginClassName", pluginClassName);
        validator.validate();
    }

    Depends { name: "cpp" }

    Depends {
        name: "Qt"

        submodules: {
            var ret = [ "qml", "quick" ]

            if (useControlsStyles) {
                ret.push("widgets")
            }

            return ret
        }
    }

    FileTagger {
        patterns: "*.qml"
        fileTags: ["qml"]
    }

    Group {     // Install library files
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: product.qmlplugin.pluginInstallDir
    }

    // Create a resource file for qml files that need to be registered by the plugin
    Rule {
        inputs: "qml"
        multiplex: true

        Artifact {
            filePath: product.sourceDirectory + '/' + product.qmlplugin.pluginName.toLowerCase() + ".qrc"
            fileTags: "qrc"
        }

        prepare: {
            var cmd = new JavaScriptCommand();

            cmd.highlight = "codegen";
            cmd.description = "Generating the QML plugin resource file for the uri " + product.qmlplugin.uri;
            cmd.sourceCode = function() {
                var buildPath = product.qbs.installRoot + product.qmlplugin.pluginInstallDir;
                var qrcSourcePath = output.filePath;
                var qrcFile = new TextFile(qrcSourcePath, TextFile.WriteOnly);

                var content = "<RCC>" + '\n';

                content += '<qresource prefix="' + '/'
                        + product.qmlplugin.uriPath + '/">'
                        + '\n';

                var resourceList = inputs["qml"];
                for (var idx in resourceList) {
                    var resourceFile = resourceList[idx];

                    var resourceFileAbsPath = resourceFile["filePath"];
                    var resourceFileRelPath = FileInfo.relativePath(product.sourceDirectory,
                                                                    resourceFileAbsPath);

                    content += '<file alias="' + resourceFile["fileName"] + '">'
                            + resourceFileRelPath + '</file>'
                            + '\n';
                }

                content += "</qresource>" + '\n' + "</RCC>" + '\n';

                qrcFile.write(content);
                qrcFile.close();
            }

            return cmd;
        }
    }

    // Generate the qmldir file
    Rule {
        multiplex: true

        Artifact {
            filePath: product.sourceDirectory + '/' + 'qmldir'
            fileTags: "qmldir"
        }

        prepare: {
            var cmd = new JavaScriptCommand();

            cmd.description = "Generating the qmldir file for the uri " + product.qmlplugin.uri;
            cmd.highlight = "filegen";

            cmd.sourceCode = function() {
                var buildPath = product.qbs.installRoot + product.qmlplugin.pluginInstallDir;
                var qmldirOutputPath = output.filePath;
                var qmldirFile = new TextFile(qmldirOutputPath, TextFile.WriteOnly);

                var content = "module " + product.qmlplugin.uri + '\n';
                content += "plugin " + product.qmlplugin.pluginName + '\n';
                content += "classname " + product.qmlplugin.pluginClassName + '\n';

                qmldirFile.write(content);
                qmldirFile.close();
            }

            return cmd;
        }
    }

    // Generate the qmltypes file
    Rule {
        condition: product.qmlplugin.genQmlTypes
        multiplex: true
        auxiliaryInputs: ["dynamiclibrary", "qmldir"]

        Artifact {
            filePath: product.sourceDirectory + '/' + 'plugins.qmltypes'
            fileTags: "qmltypes"
        }

        prepare: {
            var qtPath = Environment.getEnv("QTDIR");
            var qmltypesOutputPath = output.filePath;
            var qmlplugindumpPath = qtPath + '/bin/' + 'qmlplugindump';

            var args = [];
            args.push("-nonrelocatable");
            args.push(product.qmlplugin.uri);
            args.push("1.0");

            var qmlPluginsAbsRoot = product.qbs.installRoot + '/' + product.qmlplugin.pluginInstallRoot;
            args.push(qmlPluginsAbsRoot);

            var cmd = new Command(qmlplugindumpPath, args);
            cmd.stdoutFilePath = qmltypesOutputPath;

            cmd.description = "Generating the qmltypes file for " + product.qmlplugin.uri;
            cmd.highlight = "filegen";

            return cmd;
        }
    }
}
