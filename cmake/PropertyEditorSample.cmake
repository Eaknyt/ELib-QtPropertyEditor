macro(add_propertyeditor_sample sampleName)
    set (PE_SAMPLE_NAME sample_${sampleName})

    project(${PE_SAMPLE_NAME})

    INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src)

    # Qt5
    find_package(Qt5Widgets REQUIRED)

    # Rules
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${INSTALL_DIR}/samples/)

    add_executable(${PE_SAMPLE_NAME} ${SOURCES})

    target_link_libraries(${PE_SAMPLE_NAME} Qt5::Widgets
                          propertyeditorbackend widgetpropertyeditor)
endmacro()
