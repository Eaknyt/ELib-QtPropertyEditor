macro(add_propertyeditor_testcase testName)
    set(PE_TEST_NAME tst_${testName})

    project(${PE_TEST_NAME})

    INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src)

    enable_testing()

    # Qt5
    set(CMAKE_AUTOMOC true)
    set(CMAKE_INCLUDE_CURRENT_DIR ON)

    find_package(Qt5 COMPONENTS Core Test REQUIRED)

    # Rules
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${INSTALL_DIR}/tests/)

    add_executable(${PE_TEST_NAME} ${SOURCES})
    add_test(${PE_TEST_NAME} ${PE_TEST_NAME})

    target_link_libraries(${PE_TEST_NAME} Qt5::Core Qt5::Test
                          propertyeditorbackend)
endmacro()
