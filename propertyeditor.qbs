import qbs
import qbs.FileInfo

Project {
    qbsSearchPaths: "qbs"

    property string cppLanguageVersion: "c++14"

    property string examplesDir : "examples"
    property string includeDir : "include"
    property string libDir : "lib"
    property string qmlDir : "qml"

    readonly property string includePath : qbs.installRoot + "/" + includeDir

    readonly property string libraryAbsInstallPath: FileInfo.joinPaths(qbs.installRoot, libDir)

    readonly property bool enableExamples: true
    readonly property bool enableTests: true

    readonly property bool installExampleBinaries: true

    references: [
        "examples/examples.qbs",
        "src/src.qbs",
        "tests/tests.qbs"
    ]
}
