#ifndef PECORE_PROPERTYFACTORY_H
#define PECORE_PROPERTYFACTORY_H

#include <memory>

#include <QtCore/QMetaType>

#include <PECore/pecore_global.h>

namespace PE {

struct uptr;
namespace Core {

class AbstractPropertyHandler;
class Property;
class PropertyFactoryPrivate;

class PECORE_EXPORT PropertyFactory
{
public:
    PropertyFactory();
    ~PropertyFactory();

    std::unique_ptr<Property> createProperty(int propType, const QString &name);

    template <typename T>
    std::unique_ptr<Property> createProperty(const QString &name);

    void registerTypeHandler(std::unique_ptr<AbstractPropertyHandler> &&handler);
    void unregisterTypeHandler(int type);

    AbstractPropertyHandler *handlerForType(int propType) const;

    std::vector<int> supportedTypes() const;

    QString valueToString(Property *prop) const;

    static PropertyFactory *defaultFactory();
    static void setDefaultFactory(PropertyFactory *factory);

private:
    const std::unique_ptr<PropertyFactoryPrivate> d;
};


template <typename T>
std::unique_ptr<Property> PropertyFactory::createProperty(const QString &name)
{
    const int propType = qMetaTypeId<T>();

    return createProperty(propType, name);
}

} // namespace Core
} // namespace PE

#endif // PECORE_PROPERTYFACTORY_H
