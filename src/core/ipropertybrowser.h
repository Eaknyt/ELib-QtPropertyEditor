#ifndef PECORE_IPROPERTYBROWSER_H
#define PECORE_IPROPERTYBROWSER_H

class QString;
class QVariant;

namespace PE {
namespace Core {

class Property;

template <class UIBase>
class IPropertyBrowser : public UIBase
{
public:
    IPropertyBrowser(UIBase *parent = nullptr) :
        UIBase(parent)
    {}

    virtual UIBase *createEditor(Property *prop, UIBase *parent) const = 0;
    virtual void destroyEditor(Property *prop) const = 0;

protected:
    virtual void addPropertyField(Property *prop) = 0;
    virtual void removePropertyField(Property *prop) = 0;
    virtual void updatePropertyField(Property *prop) = 0;

    virtual void updatePropertyFieldValue(Property *prop,
                                          const QVariant &value);

    virtual void updatePropertyFieldHint(Property *prop,
                                         const QString &name,
                                         const QVariant &value);

    virtual void updatePropertyFieldReadOnly(Property *prop, bool readOnly);

    virtual void updatePropertyFieldResettable(Property *prop,
                                               bool resettable);

    virtual void updatePropertyFieldDefaultValue(Property *prop,
                                                 const QVariant &defaultValue);
};


/* Non-pure virtual methods definitions */
template<class UIBase>
void IPropertyBrowser<UIBase>::updatePropertyFieldValue(Property *prop,
                                                        const QVariant &value)
{
    Q_UNUSED(prop);
    Q_UNUSED(value);
}

template<class UIBase>
void IPropertyBrowser<UIBase>::updatePropertyFieldHint(Property *prop,
                                                       const QString &name,
                                                       const QVariant &value)
{
    Q_UNUSED(prop);
    Q_UNUSED(name);
    Q_UNUSED(value);
}

template<class UIBase>
void IPropertyBrowser<UIBase>::updatePropertyFieldReadOnly(Property *prop,
                                                           bool readOnly)
{
    Q_UNUSED(prop);
    Q_UNUSED(readOnly);
}

template<class UIBase>
void IPropertyBrowser<UIBase>::updatePropertyFieldResettable(Property *prop,
                                                             bool resettable)
{
    Q_UNUSED(prop);
    Q_UNUSED(resettable);
}

template<class UIBase>
void IPropertyBrowser<UIBase>::updatePropertyFieldDefaultValue
(Property *prop, const QVariant &defaultValue)
{
    Q_UNUSED(prop);
    Q_UNUSED(defaultValue);
}

} // namespace Core
} // namespace PE

#endif // PECORE_IPROPERTYBROWSER_H
