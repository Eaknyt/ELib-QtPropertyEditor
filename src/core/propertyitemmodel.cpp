#include "propertyitemmodel.h"

#include <PECore/property.h>
#include <PECore/propertyfactory.h>
#include <PECore/propertymanager.h>


namespace PE {
namespace Core {


////////////////////// PropertyItemPrivate //////////////////////

class PropertyItemPrivate
{
public:
    PropertyItemPrivate();

    Property *prop;
    PropertyItemModel *model;
};

PropertyItemPrivate::PropertyItemPrivate() :
    prop(nullptr),
    model(nullptr)
{}


////////////////////// PropertyItem //////////////////////

PropertyItem::PropertyItem(Property *prop) :
    d(new PropertyItemPrivate)
{
    Q_ASSERT(prop);

    d->prop = prop;
}

PropertyItem::~PropertyItem()
{}

Property *PropertyItem::property() const
{
    return d->prop;
}

QVariant PropertyItem::data(int column, int role) const
{
    QVariant ret;

    const PropertyManager *manager = d->prop->manager();

    // This trick is necessary to enable compatibility with QtQuick views
    switch (role) {
    case PropertyItemModel::PropertyTypeRole:
        ret = d->prop->type();
        break;
    case PropertyItemModel::PropertyReadOnlyRole:
        ret = d->prop->isReadOnly();
        break;
    case PropertyItemModel::PropertyResettableRole:
        ret = d->prop->isResettable();
        break;
    case PropertyItemModel::PropertyValueRole:
        ret = d->prop->value();
        break;
    case Qt::DisplayRole:
    case PropertyItemModel::PropertyValueStringRole:
    {
        PropertyFactory *propFactory = manager->propertyFactory();

        if (!propFactory) {
            propFactory = PropertyFactory::defaultFactory();
        }

        ret = propFactory->valueToString(d->prop);
        break;
    }
    case PropertyItemModel::PropertyHintsRole:
        ret = d->prop->hints();
        break;
    case PropertyItemModel::PropertyDefaultValueRole:
        ret = d->prop->defaultValue();
        break;
    }

    if (column == 0) {
        switch (role) {
        case Qt::DisplayRole:
            ret = d->prop->name();
            break;
        case Qt::ToolTipRole:
            ret = d->prop->toolTip();
            break;
        case Qt::StatusTipRole:
            ret = d->prop->statusTip();
            break;
        case Qt::WhatsThisRole:
            ret = d->prop->whatsThis();
            break;
        default:
            break;
        }
    }
    else if (column == 1) {
        switch (role) {
        case Qt::DisplayRole:
        {
            PropertyFactory *propFactory = manager->propertyFactory();

            if (!propFactory) {
                propFactory = PropertyFactory::defaultFactory();
            }

            ret = propFactory->valueToString(d->prop);
            break;
        }
        default:
            break;
        }
    }

    return ret;
}


////////////////////// PropertyItemModelPrivate //////////////////////

class PropertyItemModelPrivate
{
public:
    PropertyItemModelPrivate(PropertyItemModel *qq);
    ~PropertyItemModelPrivate();

    void deleteItems();

    PropertyItemModel *q_ptr;
    QList<PropertyItem *> items;

    std::pair<QVariant, QVariant> headerData;
};

PropertyItemModelPrivate::PropertyItemModelPrivate(PropertyItemModel *qq) :
    q_ptr(qq),
    items(),
    headerData(QObject::tr("Property"), QObject::tr("Value"))
{}

PropertyItemModelPrivate::~PropertyItemModelPrivate()
{
    deleteItems();
}

void PropertyItemModelPrivate::deleteItems()
{
    qDeleteAll(items);
}


////////////////////// PropertyItemModel //////////////////////

PropertyItemModel::PropertyItemModel(QObject *parent) :
    QAbstractItemModel(parent),
    d(new PropertyItemModelPrivate(this))
{}

PropertyItemModel::~PropertyItemModel()
{}

PropertyItem *PropertyItemModel::item(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return nullptr;
    }

    return static_cast<PropertyItem *>(index.internalPointer());
}

QModelIndex PropertyItemModel::indexFromItem(PropertyItem *item,
                                             int column) const
{
    if (!item) {
        return QModelIndex();
    }

    if (!index(0, column, QModelIndex()).isValid()) {
        return QModelIndex();
    }

    return createIndex(d->items.indexOf(item), column, item);
}

Property *PropertyItemModel::propertyFromIndex(const QModelIndex &index) const
{
    const PropertyItem *propItem = item(index);

    Property *ret = nullptr;

    if (propItem) {
        ret = propItem->property();
    }

    return ret;
}

void PropertyItemModel::addItem(PropertyItem *item)
{
    insertItem(d->items.count(), item);
}

void PropertyItemModel::insertItem(int row, PropertyItem *item)
{
    beginInsertRows(QModelIndex(), row, row);

    item->d->model = this;
    d->items.insert(row, item);

    endInsertRows();
}

void PropertyItemModel::removeItem(PropertyItem *item)
{
    QModelIndex idx = indexFromItem(item, 0);
    Q_ASSERT(idx.isValid());

    const int row = idx.row();

    beginRemoveRows(QModelIndex(), row, row);

    d->items.removeAt(row);
    delete item;

    endRemoveRows();
}

void PropertyItemModel::updateItem(PropertyItem *item)
{
    const QModelIndex idx1 = indexFromItem(item, 0);
    const QModelIndex idx2 = indexFromItem(item, 1);

    QVector<int> roles {
        Qt::DisplayRole, Qt::ToolTipRole, Qt::StatusTipRole ,Qt::WhatsThisRole
    };

    Q_EMIT dataChanged(idx1, idx2, roles);
}

void PropertyItemModel::updateItemReadOnly(PropertyItem *item)
{
    const QModelIndex idx1 = indexFromItem(item, 0);
    const QModelIndex idx2 = indexFromItem(item, 1);

    QVector<int> roles {
        PropertyItemModel::PropertyReadOnlyRole
    };

    Q_EMIT dataChanged(idx1, idx2, roles);
}

void PropertyItemModel::updateItemResettable(PropertyItem *item)
{
    const QModelIndex idx1 = indexFromItem(item, 0);
    const QModelIndex idx2 = indexFromItem(item, 1);

    QVector<int> roles {
        PropertyItemModel::PropertyResettableRole
    };

    Q_EMIT dataChanged(idx1, idx2, roles);
}

void PropertyItemModel::updateItemValue(PropertyItem *item)
{
    const QModelIndex idx1 = indexFromItem(item, 0);
    const QModelIndex idx2 = indexFromItem(item, 1);

    QVector<int> roles {
        Qt::DisplayRole,
        PropertyItemModel::PropertyValueStringRole,
        PropertyItemModel::PropertyValueRole
    };

    Q_EMIT dataChanged(idx1, idx2, roles);
}

void PropertyItemModel::updateItemHints(PropertyItem *item)
{
    const QModelIndex idx = indexFromItem(item, 1);

    QVector<int> roles {
        PropertyItemModel::PropertyHintsRole
    };

    Q_EMIT dataChanged(idx.parent().child(0, 0), idx, roles);
}

void PropertyItemModel::updateItemDefaultValue(PropertyItem *item)
{
    const QModelIndex idx = indexFromItem(item, 1);

    QVector<int> roles {
        PropertyItemModel::PropertyDefaultValueRole
    };

    Q_EMIT dataChanged(idx.parent().child(0, 0), idx, roles);
}

void PropertyItemModel::clear()
{
    beginResetModel();

    d->deleteItems();

    endResetModel();
}

QVariant PropertyItemModel::headerData(int section,
                                       Qt::Orientation orientation,
                                       int role) const
{
    if (orientation != Qt::Horizontal) {
        return QVariant();
    }

    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    QVariant ret;

    if (section == 0) {
        ret = d->headerData.first;
    }
    else if (section == 1) {
        ret = d->headerData.second;
    }

    return ret;
}

bool PropertyItemModel::setHeaderData(int section, Qt::Orientation orientation,
                                      const QVariant &value, int role)
{
    if (orientation != Qt::Horizontal) {
        return false;
    }

    if (role != Qt::DisplayRole) {
        return false;
    }

    if (section == 0) {
        d->headerData.first = value;
    }
    else if (section == 1) {
        d->headerData.second = value;
    }

    Q_EMIT headerDataChanged(Qt::Horizontal, 0, 1);

    return true;
}

QHash<int, QByteArray> PropertyItemModel::roleNames() const
{
    QHash<int, QByteArray> ret {
        {Qt::DisplayRole, "display"},
        {Qt::ToolTipRole, "toolTip"},
        {Qt::StatusTipRole, "statusTip"},
        {Qt::WhatsThisRole, "whatsThis"},
        {PropertyItemModel::PropertyTypeRole, "type"},
        {PropertyItemModel::PropertyReadOnlyRole, "isReadOnly"},
        {PropertyItemModel::PropertyResettableRole, "isResettable"},
        {PropertyItemModel::PropertyValueRole, "value"},
        {PropertyItemModel::PropertyValueStringRole, "valueString"},
        {PropertyItemModel::PropertyHintsRole, "hints"},
        {PropertyItemModel::PropertyDefaultValueRole, "defaultValue"}
    };

    return ret;
}

int PropertyItemModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid()) {
        return d->items.count();
    }

    return 0;
}

int PropertyItemModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return 2;
}

Qt::ItemFlags PropertyItemModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags ret = QAbstractItemModel::flags(index);;

    PropertyItem *propItem = item(index);

    if (!propItem) {
        return ret;
    }

    if (propItem->property()->isReadOnly()) {
        ret ^= Qt::ItemIsEnabled;
    }

    if (index.column() == 1) {
        ret |= Qt::ItemIsEditable;
    }

    return ret;
}

QVariant PropertyItemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    PropertyItem *propItem = item(index);

    if (propItem) {
        return propItem->data(index.column(), role);
    }

    return QVariant();
}

QModelIndex PropertyItemModel::index(int row, int column,
                                     const QModelIndex &parent) const
{
    if (row < 0 || column < 0 || column >= columnCount(parent)) {
        return QModelIndex();
    }

    Q_ASSERT(!parent.isValid());

    if (hasIndex(row, column, parent)) {
        return createIndex(row, column, d->items.at(row));
    }

    return QModelIndex();
}

QModelIndex PropertyItemModel::parent(const QModelIndex &index) const
{
    Q_UNUSED(index);

    return QModelIndex();
}

bool PropertyItemModel::hasChildren(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    if (!parent.isValid()) {
        return true;
    }

    return false;
}

} // namespace Core
} // namespace PE
