import qbs
import qbs.Environment

import Reusables

Reusables.PublicLibrary {
    name: "PECore"
    targetName: "pecore"

    cpp.defines: "PE_CORE_LIB"
    cpp.cxxLanguageVersion: project.cppLanguageVersion
    cpp.includePaths: [ project.includePath ]

    Depends {
        name: "Qt"
        submodules: [ "core" ]
    }

    files: [
        "private/property_p.h",
        "private/propertymanager_p.h",
        "private/smartptrcomparators_p.h",
        "abstractpropertyhandler.cpp",
        "abstractpropertyhandler.h",
        "ipropertybrowser.h",
        "pecore_constants.h",
        "pecore_global.h",
        "pecore_helpers.cpp",
        "pecore_helpers.h",
        "property.h",
        "property.cpp",
        "propertyfactory.cpp",
        "propertyfactory.h",
        "propertymanager.h",
        "propertymanager.cpp",
        "propertyitemmodel.cpp",
        "propertyitemmodel.h"
    ]

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: project.libDir
    }
}
