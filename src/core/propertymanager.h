#ifndef PECORE_PROPERTYMANAGER_H
#define PECORE_PROPERTYMANAGER_H

#include <memory>
#include <vector>

#include <QtCore/QObject>

#include <PECore/pecore_global.h>

namespace PE {
namespace Core {

class Property;
class PropertyFactory;
class PropertyManagerPrivate;

class PECORE_EXPORT PropertyManager : public QObject
{
    Q_OBJECT

public:
    explicit PropertyManager(QObject *parent = nullptr);
    ~PropertyManager();

    Property *addProperty(int propType, const QString &name);

    template <typename T>
    Property *addProperty(const QString &name);

    Property *addProperty(std::unique_ptr<Property> &&prop);
    void removeProperty(Property *&prop);

    std::vector<Property *> properties() const;

    int count() const;
    bool contains(Property *prop) const;

    PropertyFactory *propertyFactory() const;
    void setPropertyFactory(PropertyFactory *factory);

public Q_SLOTS:
    void clear();

    void setValue(Property *prop, const QVariant &value);

    void setHint(Property *prop, const QString &name,
                 const QVariant &value);

Q_SIGNALS:
    void propertyAdded(Property *prop);
    void propertyRemoved(Property *prop);
    void propertyChanged(Property *prop);
    void propertyReadOnlyChanged(Property *prop, bool readOnly);
    void propertyResetChanged(Property *prop, bool resettable);
    void propertyValueChanged(Property *prop, const QVariant &value);
    void propertyHintChanged(Property *prop, const QString &name,
                             const QVariant &value);
    void propertyHintsChanged(Property *prop, const QVariantMap &hints);
    void propertyDefaultValueChanged(Property *prop,
                                     const QVariant &defaultValue);

private:
    Q_DISABLE_COPY(PropertyManager)

    friend class Property;
    const QScopedPointer<PropertyManagerPrivate> d;
};


template <typename T>
Property *PropertyManager::addProperty(const QString &name)
{
    return addProperty(qMetaTypeId<T>(), name);
}

} // namespace Core
} // namespace PE

#endif // PECORE_PROPERTYMANAGER_H
