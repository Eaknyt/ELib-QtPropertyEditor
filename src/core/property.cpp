#include "property.h"

#include <PECore/private/property_p.h>
#include <PECore/private/propertymanager_p.h>

#include <PECore/propertymanager.h>

namespace PE {
namespace Core {


////////////////////// PropertyPrivate //////////////////////

PropertyPrivate::PropertyPrivate(PropertyManager *theManager) :
    manager(theManager),
    type(QMetaType::UnknownType),
    value(),
    hints(),
    isResettable(false),
    defaultValue(),
    name(),
    toolTip(),
    statusTip(),
    whatsThis(),
    isReadOnly(false)
{}


////////////////////// Property //////////////////////

/*!
 \class Property

 \brief The Property class represents a property in the context of a
 PropertyManager.

 It is not comparable to a Qt property or a QML property.

 A Property is created by a PropertyManager, given its name.

 You can customize the attributes, like property name, tooltip...of an property
 using the following functions :

 setName(),
 setToolTip(),
 setStatusTip(),
 setWhatsThis()

 \sa PropertyManager
 */

/*!
 \brief Constructs a Property instance and add it to the given \p manager.

 \note This constructor is protected and should be only used by
 PropertyManager.

 \sa PropertyManager
*/
Property::Property(int propType, const QString &name) :
    d(std::make_unique<PropertyPrivate>(nullptr))
{
    setType(propType);
    setName(name);
}

Property::Property(PropertyManager *manager) :
    d(std::make_unique<PropertyPrivate>(manager))
{}

/*!
 \brief Destroys the Property instance.
*/
Property::~Property()
{}

/*!
 \brief Returns the type of this property.
*/
int Property::type() const
{
    return d->type;
}

void Property::setType(int type)
{
    Q_ASSERT(type != QMetaType::UnknownType);

    if (d->type != type) {
        d->type = type;

        auto initialVal = QVariant(QVariant::Type(type));

        d->value = initialVal;
        d->defaultValue = initialVal;
    }
}

/*!
 \brief Returns the value of this property.

 \sa Property::setValue()
*/
QVariant Property::value() const
{
    return d->value;
}

/*!
 \brief Sets the value of this property to the given \p value.

 \sa Property::value()
 \sa PropertyManager::propertyValueChanged()
*/
void Property::setValue(const QVariant &value)
{
    const QVariant oldVal = d->value;

    Q_ASSERT_X(oldVal.userType() == value.userType(), Q_FUNC_INFO,
               "The property types are different");

    if (oldVal != value) {
        d->value = value;

        if (d->manager) {
            Q_EMIT d->manager->propertyValueChanged(this, value);
        }
    }
}

/*!
 \brief Returns the value of hint \p name associated with this property.

 \sa Property::setHint()
 \sa Property::hints()
*/
QVariant Property::hint(const QString &name) const
{
    Q_ASSERT(d->hints.contains(name));

    return d->hints.value(name);
}

/*!
 \brief Sets the hint \p name to the given \p value.

 \sa Property::hint()
 \sa PropertyManager::propertyHintChanged()
*/
void Property::setHint(const QString &name, const QVariant &value)
{
    const QVariant oldVal = d->hints.value(name);

    Q_ASSERT_X(oldVal.userType() == value.userType(), Q_FUNC_INFO,
               "The hint types are different");

    if (oldVal != value) {
        d->hints[name] = value;

        if (d->manager) {
            Q_EMIT d->manager->propertyHintChanged(this, name, value);
        }
    }
}

/*!
 \brief Returns the hints of this property.

 \sa Property::hint()
 \sa Property::setHint()
*/
QVariantMap Property::hints() const
{
    return d->hints;
}

void Property::setHints(const QVariantMap &hints)
{
    if (d->hints != hints) {
        d->hints = hints;

        if (d->manager) {
            Q_EMIT d->manager->propertyHintsChanged(this, hints);
        }
    }
}

/*!
 \brief Returns true if the hint specified by its \p name is a hint of this
 property, otherwise returns false.

 \sa Property::hint()
 \sa Property::setHint()
*/
bool Property::hasHint(const QString &name) const
{
    return d->hints.contains(name);
}

/*!
 \brief Returns true if the property is resettable, otherwise returns false.

 \sa Property::setResettable()
*/
bool Property::isResettable() const
{
    return d->isResettable;
}

/*!
 \brief Makes the property resettable if \p resettable is true, otherwise makes
 it non-resettable.

 \sa Property::isResettable()
*/
void Property::setResettable(bool resettable)
{
    if (d->isResettable != resettable) {
        d->isResettable = resettable;

        if (d->manager) {
            Q_EMIT d->manager->propertyResetChanged(this, resettable);
        }
    }
}

/*!
 \brief Returns the default value of this property.

 \sa Property::setDefaultValue()
*/
QVariant Property::defaultValue() const
{
    return d->defaultValue;
}

/*!
 \brief Sets the default value of this property to \p value.

 \sa Property::defaultValue()
*/
void Property::setDefaultValue(const QVariant &value)
{
    Q_ASSERT(d->defaultValue.userType() == value.userType());

    if (d->defaultValue != value) {
        d->defaultValue = value;

        if (d->manager) {
            Q_EMIT d->manager->propertyDefaultValueChanged(this, value);
        }
    }
}

/*!
 \brief Sets the value of this property to the default value.

 If the has no default value, does nothing.

 \sa Property::defaultValue()
 \sa Property::setDefaultValue()
 \sa Property::isResettable()
 \sa Property::setResettable()
*/
void Property::reset()
{
    if (isResettable()) {
        setValue(d->defaultValue);
    }
}

/*!
 \brief Returns the property name.

 \sa Property::setName()
*/
QString Property::name() const
{
    return d->name;
}

/*!
 \brief Set the property name to the given \p name.

 \sa Property::name()
*/
void Property::setName(const QString &name)
{
    if (d->name != name) {
        d->name = name;

        if (d->manager) {
            Q_EMIT d->manager->propertyChanged(this);
        }
    }
}

/*!
 \brief Returns the property's tooltip.

 \sa Property::setToolTip()
*/
QString Property::toolTip() const
{
    return d->toolTip;
}

/*!
 \brief Set the property's tooltip to \p toolTip.

 \sa Property::toolTip()
*/
void Property::setToolTip(const QString &tooltip)
{
    if (d->toolTip != tooltip) {
        d->toolTip = tooltip;

        if (d->manager) {
            Q_EMIT d->manager->propertyChanged(this);
        }
    }
}

/*!
 \brief Returns the property status tip.

 \sa Property::setStatusTip()
*/
QString Property::statusTip() const
{
    return d->statusTip;
}

/*!
 \brief Set the property status tip to \p text.

 \sa Property::statusTip()
*/
void Property::setStatusTip(const QString &text)
{
    if (d->statusTip != text) {
        d->statusTip = text;

        if (d->manager) {
            Q_EMIT d->manager->propertyChanged(this);
        }
    }
}

/*!
 \brief Returns the property Whats This.

 \sa Property::setWhatsThis()
*/
QString Property::whatsThis() const
{
    return d->whatsThis;
}

/*!
 \brief Set the property Whats This to \p text.

 \sa Property::whatsThis()
*/
void Property::setWhatsThis(const QString &text)
{
    if (d->whatsThis != text) {
        d->whatsThis = text;

        if (d->manager) {
            Q_EMIT d->manager->propertyChanged(this);
        }
    }
}

/*!
 \brief Returns true if this property is read-only, otherwise returns false.

 \sa Property::setReadOnly()
*/
bool Property::isReadOnly() const
{
    return d->isReadOnly;
}

/*!
 \brief Sets this property's enable state to \p readOnly.

 \sa Property::isReadOnly()
*/
void Property::setReadOnly(bool readOnly)
{
    if (d->isReadOnly != readOnly) {
        d->isReadOnly = readOnly;

        if (d->manager) {
            Q_EMIT d->manager->propertyReadOnlyChanged(this, readOnly);
        }
    }
}

/*!
 \brief Returns the manager that handles this property.

 \sa PropertyManager
*/
const PropertyManager *Property::manager() const
{
    return d->manager;
}

} // namespace Core
} // namespace PE
