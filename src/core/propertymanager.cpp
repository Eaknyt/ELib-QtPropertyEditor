#include "propertymanager.h"

#include <QtCore/QDebug>

#include <PECore/private/property_p.h>
#include <PECore/private/propertymanager_p.h>

#include <PECore/property.h>
#include <PECore/propertyfactory.h>


namespace {

const char PROPERTY_NOT_HANDLED_ERROR_MSG[] =
        "The property is not owned by the manager";

} // anon namespace


namespace PE {
namespace Core {


////////////////////// PropertyManagerPrivate //////////////////////

PropertyManagerPrivate::PropertyManagerPrivate(PropertyManager *qq) :
    q(qq),
    propertyFactory(nullptr),
    properties()
{}

/*! \cond PRIVATE */
/*!
 \brief Remove any reference to the property \p prop in the manager's internals
 and returns an iterator positioned at the next item in the properties set.
*/
std::set<PropertyUPtr>::iterator PropertyManagerPrivate::destroyProperty
(Property *&prop)
{
    std::set<PropertyUPtr>::iterator toRemove = properties.find(prop);
    std::set<PropertyUPtr>::iterator nextIt = properties.erase(toRemove);

    return nextIt;
}

void PropertyManagerPrivate::assertManagerContainsProp(Property *prop) const
{
    Q_ASSERT(prop);

    Q_ASSERT_X(q->contains(prop), Q_FUNC_INFO,
               PROPERTY_NOT_HANDLED_ERROR_MSG);
}

/*! \endcond */

void PropertyManagerPrivate::clearProperties()
{
    std::set<PropertyUPtr>::iterator it = properties.begin();

    while (it != properties.end()) {
        Property *prop = it->get();

        Q_EMIT q->propertyRemoved(prop);

        it = destroyProperty(prop);
    }
}


////////////////////// PropertyManager //////////////////////

/*!
 \class PropertyManager

 \brief The PropertyManager class provides a way to manage property entries.

 It may be basically seen as a wrapper around a map which associates a property
 with a value. A value can be of any type supported by QVariant.

 \sa Property
 */

/*!
 \brief Constructs a PropertyManager instance with the given \p parent.
*/
PropertyManager::PropertyManager(QObject *parent) :
    QObject(parent),
    d(new PropertyManagerPrivate(this))
{}

/*!
 \brief Destroys the PropertyManager instance.
*/
PropertyManager::~PropertyManager()
{
    clear();
}

Property *PropertyManager::addProperty(int propType, const QString &name)
{
    PropertyFactory *propFactory = (d->propertyFactory)
            ? d->propertyFactory
            : PropertyFactory::defaultFactory();

    Q_ASSERT (propFactory);

    if (!QMetaType::isRegistered(propType)) {
        return nullptr;
    }

    std::unique_ptr<Property> prop =
            propFactory->createProperty(propType, name);

    return addProperty(std::move(prop));
}

Property *PropertyManager::addProperty(std::unique_ptr<Property> &&prop)
{
    Property *ret = prop.get();

    d->properties.insert(std::move(prop));

    ret->d->manager = this;

    Q_EMIT propertyAdded(ret);

    return ret;
}

/*!
 \brief Remove the property \p prop from the manager.

 \note This function does not free the memory allocated for the property.

 \sa PropertyManager::propertyRemoved()
*/
void PropertyManager::removeProperty(Property *&prop)
{
    Q_ASSERT(prop);

    Q_EMIT propertyRemoved(prop);

    d->destroyProperty(prop);

    prop = nullptr;
}

/*!
 \brief Returns the set of properties owned by the manager.
*/
std::vector<Property *> PropertyManager::properties() const
{
    std::vector<Property *> ret(d->properties.size());

    std::transform(d->properties.begin(), d->properties.end(),
                   ret.begin(), [] (const auto &prop) {
        return prop.get();
    });

    return ret;
}

/*!
 \brief Returns the number of properties currently handled by the manager.
*/
int PropertyManager::count() const
{
    return d->properties.size();
}

/*!
 \brief Returns true if the manager contains the property \p prop,
 otherwise returns false.
*/
bool PropertyManager::contains(Property *prop) const
{
    auto propFound = d->properties.find(prop);

    return propFound != d->properties.end();
}

/*!
 \brief Remove all properties handled by the manager.
*/
void PropertyManager::clear()
{
    d->clearProperties();
}

/*!
 \brief Sets the property value of \p prop to the given \p value.

 Then emits the signal propertyValueChanged().

 The property \p prop must be owned by the manager.

 \sa Property::setValue()
 \sa PropertyManager::propertyValueChanged()
*/
void PropertyManager::setValue(Property *prop, const QVariant &value)
{
    d->assertManagerContainsProp(prop);

    prop->setValue(value);
}

/*!
 \brief Sets the hint \p name of \p prop to the given \p value.

 Then emits the signal propertyHintChanged().

 The property \p prop must be owned by the manager.

 \sa Property::hint()
 \sa PropertyManager::propertyHintChanged()
*/
void PropertyManager::setHint(Property *prop, const QString &name,
                              const QVariant &value)
{
    d->assertManagerContainsProp(prop);

    prop->setHint(name, value);
}

PropertyFactory *PropertyManager::propertyFactory() const
{
    return d->propertyFactory;
}

void PropertyManager::setPropertyFactory(PropertyFactory *factory)
{
    if (d->propertyFactory != factory) {
        d->propertyFactory = factory;
    }
}

/*!
 \fn void PropertyManager::propertyAdded(Property *prop)

 This signal is emitted when a property is added to the manager with
 addProperty().

 \sa PropertyManager::addProperty()
*/

/*!
 \fn void PropertyManager::propertyRemoved(Property *prop)

 This signal is emitted when a property is removed from the manager with
 removeProperty().

 \sa PropertyManager::removeProperty()
*/

/*!
 \fn void PropertyManager::propertyChanged(Property *prop)

 This signal is emitted when an attribute (like property name,
 tooltip, status tip...) of the property \p prop is changed.
*/

/*!
 \fn void PropertyManager::propertyValueChanged(Property *prop,
                                                const QVariant &value)

 This signal is emitted when the value associated with the property \p prop is
 changed. The given \p value represents the new value of the property.

 \sa PropertyManager::setValue()
*/

/*!
 \fn void PropertyManager::propertyHintChanged(Property *prop,
                                               const QString &name,
                                               const QVariant &value)

 This signal is emitted when the hint associated with the property
 \p prop is changed. \p value represents the new value of the hint.

 \sa PropertyManager::setHint()
*/

/*!
 \fn void PropertyManager::propertyReadOnlyChanged(Property *prop)

 This signal is emitted the property \p prop is set to read only or not.
*/

/*!
 \fn void PropertyManager::propertyResetChanged(Property *prop)

 This signal is emitted the property \p prop is set as resettable or not.
*/

} // namespace Core
} // namespace PE
