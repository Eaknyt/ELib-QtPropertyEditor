#ifndef PECORE_CONSTANTS_H
#define PECORE_CONSTANTS_H

namespace PE {
namespace Core {

const char RESET_BTN_OBJECT_NAME[] = "pe_propertyeditor_resetBtn";

} // namespace Core
} // namespace PE

#endif // PECORE_CONSTANTS_H
