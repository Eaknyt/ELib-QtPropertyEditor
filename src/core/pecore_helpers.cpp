#include "pecore_helpers.h"

#include <QtCore/QDebug>
#include <QtCore/QMetaType>

namespace PE {
namespace Core {
namespace Debug {

void qCritical_noEditorFactoryFound(int type, const QString &className)
{
    QString typeName(QMetaType::typeName(type));

    if (typeName.isNull()) {
        typeName = QString::number(type);
    }

    qCritical().noquote() << className
                          << "> No editor associated with the type"
                          << typeName;
}

} // namespace Debug
} // namespace Core
} // namespace PE
