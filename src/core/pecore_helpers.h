#ifndef PECORE_HELPERS_H
#define PECORE_HELPERS_H

class QString;

namespace PE {
namespace Core {
namespace Debug {

void qCritical_noEditorFactoryFound(int type, const QString &className);

} // namespace Debug
} // namespace Core
} // namespace PE

#endif // PECORE_HELPERS_H
