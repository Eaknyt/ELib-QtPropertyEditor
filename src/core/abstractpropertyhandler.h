#ifndef PECORE_ABSTRACTPROPERTYHANDLER_H
#define PECORE_ABSTRACTPROPERTYHANDLER_H

#include <QtCore/QMetaType>

#include <PECore/pecore_global.h>

namespace PE {
namespace Core {

class Property;

class PECORE_EXPORT AbstractPropertyHandler
{
public:
    AbstractPropertyHandler();
    virtual ~AbstractPropertyHandler() {}

    virtual int type() const = 0;

    virtual void initProperty(Property *prop, const QString &name);

    virtual QString valueToString(const Property *prop) const;

    virtual QVariantMap defaultHints() const;
};

template <typename T>
class PECORE_EXPORT PropertyHandler : public AbstractPropertyHandler
{
public:
    explicit PropertyHandler() :
        AbstractPropertyHandler()
    {}

    constexpr int type() const override final
    {
        return qMetaTypeId<T>();
    }
};

} // namespace Core
} // namespace PE

#endif // PECORE_ABSTRACTPROPERTYHANDLER_H
