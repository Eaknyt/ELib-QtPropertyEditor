#include "propertyfactory.h"

#include <QtCore/QDebug>

#include <PECore/private/property_p.h>

#include <PECore/abstractpropertyhandler.h>
#include <PECore/property.h>


namespace PE {
namespace Core {


////////////////////// PropertyFactoryPrivate //////////////////////

class PropertyFactoryPrivate
{
public:
    PropertyFactoryPrivate(PropertyFactory *qq);

    /* Methods */
    void clearTypeHandlers();

    void initProperty(int propType, Property *prop,
                      const QString &name) const;

    void assignDefaultHintsToProperty(Property *prop);

    /* Attributes */
    PropertyFactory *q;

    static PropertyFactory initialDefaultFactory;
    static PropertyFactory *defaultFactory;
    std::map<int, std::unique_ptr<AbstractPropertyHandler>> typeToTypeHandler;
};

PropertyFactory PropertyFactoryPrivate::initialDefaultFactory;
PropertyFactory *PropertyFactoryPrivate::defaultFactory = &initialDefaultFactory;

PropertyFactoryPrivate::PropertyFactoryPrivate(PropertyFactory *qq) :
    q(qq),
    typeToTypeHandler()
{}

void PropertyFactoryPrivate::clearTypeHandlers()
{
    typeToTypeHandler.clear();
}

void PropertyFactoryPrivate::initProperty(int propType, Property *prop,
                                          const QString &name) const
{
    AbstractPropertyHandler *typeHandler = q->handlerForType(propType);

    if (typeHandler) {
        typeHandler->initProperty(prop, name);
    }
    else {
        prop->setName(name);
    }
}

void PropertyFactoryPrivate::assignDefaultHintsToProperty(Property *prop)
{
    QVariantMap hints;

    AbstractPropertyHandler *typeHandler = q->handlerForType(prop->type());

    if (typeHandler) {
        hints = typeHandler->defaultHints();
    }

    prop->setHints(hints);
}


////////////////////// PropertyFactory //////////////////////

PropertyFactory::PropertyFactory() :
    d(std::make_unique<PropertyFactoryPrivate>(this))
{}

PropertyFactory::~PropertyFactory()
{
    d->clearTypeHandlers();
}

/*!
    \brief Creates a property with the type \p propType and the given
    \p name.

    Returns an observer pointer to it.

    \warning \p propType must be a value from QMetaType::Type or represent a
    type registered as a QMetaType using Q_DECLARE_METATYPE. If not, the
    function returns a null pointer.

    \note If you want to provide a user-defined type id as \p propType, use
    qMetaTypeId<YourType>() or use the template method.
*/
std::unique_ptr<Property> PropertyFactory::createProperty(int propType,
                                                               const QString &name)
{
    // Check that the type is supported by QVariant
    if (!QMetaType::isRegistered(propType)) {
        qCritical() << Q_FUNC_INFO << "Unknown QMetaType :" << propType;

        return std::unique_ptr<Property>();
    }

    auto ret = std::make_unique<Property>(propType, name);

    d->initProperty(propType, ret.get(), name);
    d->assignDefaultHintsToProperty(ret.get());

    return ret;
}

/*!
    \brief Registers a type handler for the property type defined in the given
    \p handler.

    If the property type is already associated with a type handler, this type
    handler is removed and the new handler takes its place.

    \sa AbstractPropertyHandler
*/
void PropertyFactory::registerTypeHandler(std::unique_ptr<AbstractPropertyHandler> &&handler)
{
    const int type = handler->type();

    auto typeHandlerFound = d->typeToTypeHandler.find(type);

    if (typeHandlerFound != d->typeToTypeHandler.end()) {
        unregisterTypeHandler(type);
    }

    d->typeToTypeHandler.insert(std::make_pair(type, std::move(handler)));
}

void PropertyFactory::unregisterTypeHandler(int type)
{
    auto typeHandlerIt = d->typeToTypeHandler.find(type);

    if (typeHandlerIt == d->typeToTypeHandler.end()) {
        return;
    }

    d->typeToTypeHandler.erase(typeHandlerIt);
}

AbstractPropertyHandler *PropertyFactory::handlerForType(int propType) const
{
    AbstractPropertyHandler *ret = nullptr;

    auto typeHandlerFound = d->typeToTypeHandler.find(propType);

    if (typeHandlerFound != d->typeToTypeHandler.end()) {
        ret = typeHandlerFound->second.get();
    }

    return ret;
}

std::vector<int> PropertyFactory::supportedTypes() const
{
    std::vector<int> ret(d->typeToTypeHandler.size());

    std::transform(d->typeToTypeHandler.begin(), d->typeToTypeHandler.end(),
                   ret.begin(), [=] (auto &p) {
        return p.first;
    });

    return ret;
}

/*!
    \brief Returns a string representation of the value of the property \p prop.

    If the manager find a type handler associated with the property type, it will
    use the overriden AbstractPropertyHandler::valueToString() function to
    provide this representation, otherwise it will use QVariant::toString().

    The property \p prop must be owned by the manager.
 */
QString PropertyFactory::valueToString(Property *prop) const
{
    AbstractPropertyHandler *typeHandler = handlerForType(prop->type());

    QString ret;

    if (!typeHandler) {
        ret = prop->value().toString();
    }
    else {
        ret = typeHandler->valueToString(prop);
    }

    return ret;
}

PropertyFactory *PropertyFactory::defaultFactory()
{
    return PropertyFactoryPrivate::defaultFactory;
}

void PropertyFactory::setDefaultFactory(PropertyFactory *factory)
{
    if (PropertyFactoryPrivate::defaultFactory != factory) {
        PropertyFactoryPrivate::defaultFactory = factory;
    }
}

} // namespace Core
} // namespace PE
