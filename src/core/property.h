#ifndef PECORE_PROPERTY_H
#define PECORE_PROPERTY_H

#include <memory>

#include <QtCore/QVariant>

#include <PECore/pecore_global.h>

namespace PE {
namespace Core {

class PropertyPrivate;
class PropertyManager;

class PECORE_EXPORT Property
{
public:
    Property(int propType, const QString &name);
    Property(PropertyManager *manager);
    ~Property();

    int type() const;

    QVariant value() const;
    void setValue(const QVariant &value);

    QVariant hint(const QString &name) const;
    void setHint(const QString &name, const QVariant &value);

    QVariantMap hints() const;
    void setHints(const QVariantMap &hints);

    bool hasHint(const QString &name) const;

    bool isResettable() const;
    void setResettable(bool resettable);

    QVariant defaultValue() const;
    void setDefaultValue(const QVariant &value);

    void reset();

    QString name() const;
    void setName(const QString &name);

    QString toolTip() const;
    void setToolTip(const QString &tooltip);

    QString statusTip() const;
    void setStatusTip(const QString &text);

    QString whatsThis() const;
    void setWhatsThis(const QString &text);

    bool isReadOnly() const;
    void setReadOnly(bool readOnly);

    const PropertyManager *manager() const;

protected:
    void setType(int type);

private:
    Q_DISABLE_COPY(Property)

    friend class PropertyManager;

    const std::unique_ptr<PropertyPrivate> d;
};

} // namespace Core
} // namespace PE

#endif // PECORE_PROPERTY_H
