#ifndef PECORE_PROPERTYMANAGER_P_H
#define PECORE_PROPERTYMANAGER_P_H

#include <map>
#include <memory>
#include <set>

#include <QtCore/QMap>
#include <QtCore/QSet>

#include <PECore/private/smartptrcomparators_p.h>

namespace PE {
namespace Core {

using PropertyUPtr = std::unique_ptr<Property>;

class PropertyFactory;

// \cond PRIVATE
class PropertyManagerPrivate
{
public:
    PropertyManagerPrivate(PropertyManager *qq);

    std::set<PropertyUPtr>::iterator destroyProperty(Property *&prop);

    void assertManagerContainsProp(Property *prop) const;

    void clearProperties();

    /* variables */
    PropertyManager *q;

    PropertyFactory *propertyFactory;

    std::set<PropertyUPtr, UniquePtrComparator<Property>> properties;
};
// \endcond

} // namespace Core
} // namespace PE

#endif // PECORE_PROPERTYMANAGER_P_H
