#ifndef PECORE_PROPERTY_P_H
#define PECORE_PROPERTY_P_H

#include <QtCore/QString>
#include <QtCore/QVariant>

namespace PE {
namespace Core {

class PropertyManager;

// \cond PRIVATE
/*!
 \class PropertyPrivate

 \internal
*/
class PropertyPrivate
{
public:
    PropertyPrivate(PropertyManager *theManager);

    PropertyManager *manager;

    int type;
    QVariant value;
    QVariantMap hints;

    bool isResettable;
    QVariant defaultValue;

    QString name;
    QString toolTip;
    QString statusTip;
    QString whatsThis;
    bool isReadOnly;
};
// \endcond

} // namespace Core
} // namespace PE

#endif // PECORE_PROPERTY_P_H
