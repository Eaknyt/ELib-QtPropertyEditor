#ifndef PECORE_SMARTPTRCOMPARATORS_H
#define PECORE_SMARTPTRCOMPARATORS_H

#include <functional>
#include <memory>

// https://stackoverflow.com/questions/18939882/raw-pointer-lookup-for-sets-of-unique-ptrs
template <class T>
struct UniquePtrComparator
{
    using is_transparent = std::true_type;

    struct ComparatorHelper
    {
        T *m_ptr;

        ComparatorHelper() :
            m_ptr(nullptr)
        {}

        ComparatorHelper(const ComparatorHelper &) = default;

        ComparatorHelper(T *p) :
            m_ptr(p)
        {}

        template<class U, class...Ts>
        ComparatorHelper(const std::unique_ptr<U, Ts...> &up) :
            m_ptr(up.get())
        {}

        bool operator<(ComparatorHelper o) const
        {
            return std::less<T*>()(m_ptr, o.m_ptr);
        }
    };

    bool operator()(const ComparatorHelper &&lhs,
                    const ComparatorHelper &&rhs) const
    {
        return lhs < rhs;
    }
};

#endif // PECORE_SMARTPTRCOMPARATORS_H
