#ifndef PECORE_PROPERTYITEMMODEL_H
#define PECORE_PROPERTYITEMMODEL_H

#include <QtCore/QAbstractItemModel>

#include <PECore/pecore_global.h>

namespace PE {
namespace Core {

class Property;
class PropertyItemPrivate;
class PropertyItemModelPrivate;

class PECORE_EXPORT PropertyItem
{
public:
    PropertyItem(Property *prop);
    ~PropertyItem();

    Property *property() const;

    QVariant data(int column, int role) const;

private:
    friend class PropertyItemModel;
    const QScopedPointer<PropertyItemPrivate> d;
};


class PECORE_EXPORT PropertyItemModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum PropertyItemModelRole
    {
        PropertyTypeRole = Qt::UserRole + 1,
        PropertyReadOnlyRole,
        PropertyResettableRole,
        PropertyValueRole,
        PropertyValueStringRole,
        PropertyHintsRole,
        PropertyDefaultValueRole
    };

    PropertyItemModel(QObject *parent = nullptr);
    ~PropertyItemModel();

    PropertyItem *item(const QModelIndex &index) const;
    QModelIndex indexFromItem(PropertyItem *item, int column = 0) const;

    Property *propertyFromIndex(const QModelIndex &index) const;

    void addItem(PropertyItem *item);
    void insertItem(int row, PropertyItem *item);

    void removeItem(PropertyItem *item);

    void updateItem(PropertyItem *item);
    void updateItemReadOnly(PropertyItem *item);
    void updateItemResettable(PropertyItem *item);
    void updateItemValue(PropertyItem *item);
    void updateItemHints(PropertyItem *item);
    void updateItemDefaultValue(PropertyItem *item);

    void clear();

    void setHeaderNames();

    // QAbstractItemModel interface
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant &value, int role) override;

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;

    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    bool hasChildren(const QModelIndex &parent) const override;

private:
    const QScopedPointer<PropertyItemModelPrivate> d;
};

} // namespace Core
} // namespace PE

#endif // PECORE_PROPERTYITEMMODEL_H
