#include "abstractpropertyhandler.h"

#include <QtCore/QVariantMap>

#include <PECore/property.h>

namespace PE {
namespace Core {

/*!
 \class AbstractPropertyHandler

 \brief The AbstractPropertyHandler class provides an base class to
 customize properties of a given type.
 */

/*!
 \fn virtual int AbstractPropertyHandler::type() const = 0

 Returns the meta type id used to identify this type handler.

 In the base class this is a pure virtual function.
*/

/*!
 \brief Initialize the property \p prop.

 Reimplement this method to customize a property, e.g. add sub
 properties to it.

 The default implementation just sets the name of \p prop to the given \p name.
*/
AbstractPropertyHandler::AbstractPropertyHandler()
{}

void AbstractPropertyHandler::initProperty(Property *prop,
                                           const QString &name)
{
    prop->setName(name);
}

/*!
 \brief Returns a string representation of the given \p value.

 Reimplement this method to provide a string representation of a custom
 property type.

 The default implementation uses the method QVariant::toString().
*/
QString AbstractPropertyHandler::valueToString(const Property *prop) const
{
    return prop->value().toString();
}

/*!
 Returns the names and values of the default hints of any property which has
 the same type than the type handler.

 The default implementation returns a null QVariantMap.

 \sa AbstractPropertyHandler::type()
*/
QVariantMap AbstractPropertyHandler::defaultHints() const
{
    return QVariantMap();
}

} // namespace Core
} // namespace PE
