#ifndef PECORE_GLOBAL_H
#define PECORE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(PE_CORE_LIB)
#  define PECORE_EXPORT Q_DECL_EXPORT
#else
#  define PECORE_EXPORT Q_DECL_IMPORT
#endif

#endif // PECORE_GLOBAL_H
