import qbs

import Reusables

Reusables.QmlPlugin {
    name: "PEQuick"

    qmlplugin.uri: "PE.Quick"
    qmlplugin.pluginClassName: "PEQuickPlugin"
    qmlplugin.useControlsStyles: true

    cpp.defines: "PEQUICKPLUGIN_LIN"
    cpp.cxxLanguageVersion: project.cppLanguageVersion
    cpp.includePaths: [ project.includePath ]
    cpp.libraryPaths: project.libraryAbsInstallPath
    cpp.rpaths: project.libraryAbsInstallPath

    Depends { name: "PECore" }
    Depends { name: "PECoreExtras" }

    files: [
        "qml/TreePropertyBrowser.qml",
        "private/quickabstractpropertybrowser_p.h",
        "pequick_global.h",
        "plugin.cpp",
        "plugin.h",
        "propertymetatypehelper.h",
        "quickabstractpropertybrowser.cpp",
        "quickabstractpropertybrowser.h",
        "quickeditorincubator.cpp",
        "quickeditorincubator.h",
        "quickmvpropertybrowser.cpp",
        "quickmvpropertybrowser.h",
        "quickeditorfactory.cpp",
        "quickeditorfactory.h",
    ]
}

