#ifndef PEQUICK_QUICKMVPROPERTYBROWSER_H
#define PEQUICK_QUICKMVPROPERTYBROWSER_H

#include <PEQuick/quickabstractpropertybrowser.h>

class QAbstractItemModel;

namespace PE {

namespace Core {
class Property;
} // namespace Core

namespace Quick {

class QuickMVPropertyBrowserPrivate;

class QuickMVPropertyBrowser : public QuickAbstractPropertyBrowser
{
    Q_OBJECT

public:
    QuickMVPropertyBrowser(QQuickItem *parent = nullptr);
    ~QuickMVPropertyBrowser();

    Q_INVOKABLE QAbstractItemModel *itemModel() const;

protected:
    Q_INVOKABLE
    QQuickItem *createEditor(const QModelIndex &index, QQuickItem *parent);

    Q_INVOKABLE
    void destroyEditor(const QModelIndex &index);

protected:
    QuickMVPropertyBrowser(QuickMVPropertyBrowserPrivate &dd,
                           QQuickItem *parent = nullptr);

    // IPropertyBrowser interface
    void addPropertyField(Core::Property *prop) override;
    void removePropertyField(Core::Property *prop) override;
    void updatePropertyField(Core::Property *prop) override;
    void updatePropertyFieldValue(Core::Property *prop,
                                  const QVariant &value) override;
    void updatePropertyFieldHint(Core::Property *prop, const QString &name,
                                 const QVariant &value) override;
    void updatePropertyFieldReadOnly(Core::Property *prop, bool readOnly) override;
    void updatePropertyFieldResettable(Core::Property *prop, bool resettable) override;
    void updatePropertyFieldDefaultValue(Core::Property *prop,
                                      const QVariant &defaultValue) override;

private:
    Q_DECLARE_PRIVATE(QuickMVPropertyBrowser)
};

} // namespace Quick
} // namespace PE

#endif // PEQUICK_QUICKMVPROPERTYBROWSER_H
