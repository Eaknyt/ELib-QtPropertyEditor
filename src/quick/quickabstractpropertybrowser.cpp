#include "quickabstractpropertybrowser.h"

#include <QtCore/QtGlobal>

#include <QtQml/QQmlContext>
#include <QtQml/QQmlEngine>
#include <QtQml/QQmlProperty>

#include <PECore/property.h>
#include <PECore/propertymanager.h>

#include <PEQuick/private/quickabstractpropertybrowser_p.h>

#include <PEQuick/quickeditorfactory.h>

namespace PE {

using namespace Core;

namespace Quick {


////////////////////// QuickAbstractPropertyBrowserPrivate /////////////////////////

QuickAbstractPropertyBrowserPrivate::QuickAbstractPropertyBrowserPrivate
(QuickAbstractPropertyBrowser *qq) :
    q_ptr(qq),
    manager(nullptr),
    editorFactoryManager(nullptr)
{}

void QuickAbstractPropertyBrowserPrivate::connectFactories(bool connect) const
{
    QQmlListReference factories(editorFactoryManager, "factories");

    for (int i = 0; i < factories.count(); ++i) {
        auto c = qobject_cast<QuickEditorFactory *>(factories.at(i));

        if (connect) {
            connectEditorFactory(c);
        }
        else {
            disconnectEditorFactory(c);
        }
    }
}

void QuickAbstractPropertyBrowserPrivate::connectEditorFactory(QuickEditorFactory *factory) const
{
    if (factory && manager) {
        q_ptr->connect(factory, &QuickEditorFactory::valueChanged,
                       manager, &PropertyManager::setValue);
    }
}

void QuickAbstractPropertyBrowserPrivate::disconnectEditorFactory(QuickEditorFactory *factory) const
{
    if (factory && manager) {
        q_ptr->disconnect(factory, &QuickEditorFactory::valueChanged,
                          manager, &PropertyManager::setValue);
    }
}

void QuickAbstractPropertyBrowserPrivate::connectFactoryManager() const
{
    if (editorFactoryManager) {
        q_ptr->connect(editorFactoryManager, &QuickEditorFactoryManager::editorFactoryAdded,
                       q_ptr, &QuickAbstractPropertyBrowser::onEditorFactoryAdded);
    }
}

void QuickAbstractPropertyBrowserPrivate::disconnectFactoryManager() const
{
    if (editorFactoryManager) {
        q_ptr->disconnect(editorFactoryManager, &QuickEditorFactoryManager::editorFactoryAdded,
                          q_ptr, &QuickAbstractPropertyBrowser::onEditorFactoryAdded);
    }
}


////////////////////// QuickAbstractPropertyBrowser /////////////////////////

/*!
 \class The QuickAbstractPropertyBrowser class provides
*/

/*!
 \brief
*/
QuickAbstractPropertyBrowser::QuickAbstractPropertyBrowser(QQuickItem *parent) :
    IPropertyBrowser<QQuickItem>(parent),
    d_ptr(new QuickAbstractPropertyBrowserPrivate(this))
{}

QuickAbstractPropertyBrowser::QuickAbstractPropertyBrowser
(QuickAbstractPropertyBrowserPrivate &dd, QQuickItem *parent) :
    IPropertyBrowser<QQuickItem>(parent),
    d_ptr(&dd)
{}

/*!
 \brief
*/
QuickAbstractPropertyBrowser::~QuickAbstractPropertyBrowser()
{}

/*!
 \brief
*/
PropertyManager *QuickAbstractPropertyBrowser::manager() const
{
    return d_ptr->manager;
}

/*!
 \brief
*/
void QuickAbstractPropertyBrowser::setManager(PropertyManager *manager)
{
    Q_D(QuickAbstractPropertyBrowser);

    if (d->manager != manager && manager) {
        disconnectManager();
        d->connectFactories(false);

        d->manager = manager;

        d->connectFactories(true);
        connectManager();

        Q_EMIT managerChanged(manager);
    }
}

/*!
 \brief
*/
QuickEditorFactoryManager *QuickAbstractPropertyBrowser::editorFactoryManager() const
{
    return d_ptr->editorFactoryManager;
}

/*!
 \brief
*/
void QuickAbstractPropertyBrowser::setEditorFactoryManager(QuickEditorFactoryManager *factoryManager)
{
    Q_D(QuickAbstractPropertyBrowser);

    if (d->editorFactoryManager != factoryManager && factoryManager) {
        d->disconnectFactoryManager();
        d->connectFactories(false);

        d->editorFactoryManager = factoryManager;

        d->connectFactories(true);
        d->connectFactoryManager();

        Q_EMIT editorFactoryManagerChanged(factoryManager);
    }
}

void QuickAbstractPropertyBrowser::connectManager()
{
    Q_D(QuickAbstractPropertyBrowser);

    if (d->manager) {
        connect(d->manager, &PropertyManager::propertyAdded,
                this, &QuickAbstractPropertyBrowser::onPropertyAdded);

        connect(d->manager, &PropertyManager::propertyRemoved,
                this, &QuickAbstractPropertyBrowser::onPropertyRemoved);

        connect(d->manager, &PropertyManager::propertyChanged,
                this, &QuickAbstractPropertyBrowser::onPropertyChanged);

        connect(d->manager, &PropertyManager::propertyValueChanged,
                this, &QuickAbstractPropertyBrowser::onPropertyValueChanged);

        connect(d->manager, &PropertyManager::propertyHintChanged,
                this, &QuickAbstractPropertyBrowser::onPropertyHintChanged);

        connect(d->manager, &PropertyManager::propertyReadOnlyChanged,
                this, &QuickAbstractPropertyBrowser::onPropertyReadOnlyChanged);

        connect(d->manager, &PropertyManager::propertyResetChanged,
                this, &QuickAbstractPropertyBrowser::onPropertyResetChanged);

        connect(d->manager, &PropertyManager::propertyDefaultValueChanged,
                this, &QuickAbstractPropertyBrowser::onPropertyDefaultValueChanged);
    }
}

void QuickAbstractPropertyBrowser::disconnectManager()
{
    Q_D(QuickAbstractPropertyBrowser);

    if (d->manager) {
        disconnect(d->manager, &PropertyManager::propertyAdded,
                   this, &QuickAbstractPropertyBrowser::onPropertyAdded);

        disconnect(d->manager, &PropertyManager::propertyRemoved,
                   this, &QuickAbstractPropertyBrowser::onPropertyRemoved);

        disconnect(d->manager, &PropertyManager::propertyChanged,
                   this, &QuickAbstractPropertyBrowser::onPropertyChanged);

        disconnect(d->manager, &PropertyManager::propertyValueChanged,
                   this, &QuickAbstractPropertyBrowser::onPropertyValueChanged);

        disconnect(d->manager, &PropertyManager::propertyHintChanged,
                   this, &QuickAbstractPropertyBrowser::onPropertyHintChanged);

        disconnect(d->manager, &PropertyManager::propertyReadOnlyChanged,
                   this, &QuickAbstractPropertyBrowser::onPropertyReadOnlyChanged);

        disconnect(d->manager, &PropertyManager::propertyResetChanged,
                   this, &QuickAbstractPropertyBrowser::onPropertyResetChanged);

        disconnect(d->manager, &PropertyManager::propertyDefaultValueChanged,
                   this, &QuickAbstractPropertyBrowser::onPropertyDefaultValueChanged);
    }
}

QQuickItem *QuickAbstractPropertyBrowser::createEditor(Property *prop,
                                                       QQuickItem *parent) const
{
    Q_D(const QuickAbstractPropertyBrowser);

    const int type = prop->type();

    QuickEditorFactory *factory = d->editorFactoryManager->editorFactory(type);

    if (!factory) {
        return nullptr;
    }

    QQuickItem *editor = factory->addProperty(prop, parent);
    Q_ASSERT(editor);

    QQmlEngine::setObjectOwnership(editor, QQmlEngine::CppOwnership);

    editor->setEnabled(!prop->isReadOnly());
    editor->forceActiveFocus();

    return editor;
}

void QuickAbstractPropertyBrowser::destroyEditor(Core::Property *prop) const
{
    Q_D(const QuickAbstractPropertyBrowser);

    const int type = prop->type();

    QuickEditorFactory *factory = d->editorFactoryManager->editorFactory(type);
    factory->removeProperty(prop);
}

void QuickAbstractPropertyBrowser::onPropertyAdded(Property *prop)
{
    // Virtual call
    addPropertyField(prop);
}

void QuickAbstractPropertyBrowser::onPropertyRemoved(Property *prop)
{
    // Virtual call
    removePropertyField(prop);
}

void QuickAbstractPropertyBrowser::onPropertyChanged(Property *prop)
{
    // Virtual call
    updatePropertyField(prop);
}

void QuickAbstractPropertyBrowser::onPropertyValueChanged(Property *prop,
                                                          const QVariant &value)
{
    // Virtual call
    updatePropertyFieldValue(prop, value);
}

void QuickAbstractPropertyBrowser::onPropertyHintChanged(Property *prop,
                                                         const QString &name,
                                                         const QVariant &value)
{
    // Virtual call
    updatePropertyFieldHint(prop, name, value);
}

void QuickAbstractPropertyBrowser::onPropertyReadOnlyChanged(Property *prop,
                                                             bool readOnly)
{
    Q_D(QuickAbstractPropertyBrowser);

    if (!d->editorFactoryManager) {
        return;
    }

    QuickEditorFactory *editorFactory =
            d->editorFactoryManager->editorFactory(prop->type());

    if (!editorFactory) {
        return;
    }

    QQuickItem *editor = editorFactory->editorFromProperty(prop);

    if (editor) {
        editor->setEnabled(!readOnly);
    }

    // Virtual call
    updatePropertyFieldReadOnly(prop, readOnly);
}

void QuickAbstractPropertyBrowser::onPropertyResetChanged(Property *prop,
                                                          bool resettable)
{
    // Virtual call
    updatePropertyFieldResettable(prop, resettable);
}

void QuickAbstractPropertyBrowser::onPropertyDefaultValueChanged(Property *prop,
                                                                 const QVariant &defaultValue)
{
    // Virtual call
    updatePropertyFieldDefaultValue(prop, defaultValue);
}

void QuickAbstractPropertyBrowser::onEditorFactoryAdded(QuickEditorFactory *factory) const
{
    Q_D(const QuickAbstractPropertyBrowser);

    d->connectEditorFactory(factory);
}

/*!
 \fn void managerChanged(PropertyManager *manager)

 This signal is emitted
*/

/*!
 \fn void QuickAbstractPropertyBrowser::connectManager()

 \sa disconnectManager()
*/

/*!
 \fn void QuickAbstractPropertyBrowser::disconnectManager()

 \sa connectManager()
*/

} // namespace Quick
} // namespace PE
