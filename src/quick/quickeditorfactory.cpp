#include "quickeditorfactory.h"

#include <QtQml/QQmlComponent>
#include <QtQml/QQmlProperty>

#include <QtQuick/QQuickItem>

#include <PECore/pecore_helpers.h>
#include <PECore/property.h>
#include <PECore/propertyfactory.h>
#include <PECore/propertymanager.h>

#include <PEQuick/quickeditorincubator.h>


namespace PE {

using namespace Core;

namespace Quick {

using PropertyToEditor = QMap<Property *, QQuickItem *>;
using EditorToProperty = QMap<QQuickItem *, Property *>;


////////////////////// QuickEditorFactoryPrivate //////////////////////

class QuickEditorFactoryPrivate
{
public:
    QuickEditorFactoryPrivate();

    int type;
    QQmlComponent *delegate;

    void registerPropertyAndEditor(Property *prop, QQuickItem *editor);
    PropertyToEditor::iterator unregisterProperty(PropertyToEditor::iterator it);

    PropertyToEditor propertyToEditor;
    EditorToProperty editorToProperty;
};

QuickEditorFactoryPrivate::QuickEditorFactoryPrivate() :
    type(QMetaType::UnknownType),
    delegate(nullptr),
    propertyToEditor(),
    editorToProperty()
{}

void QuickEditorFactoryPrivate::registerPropertyAndEditor(Property *prop,
                                                          QQuickItem *editor)
{
    propertyToEditor.insert(prop, editor);
    editorToProperty.insert(editor, prop);
}

PropertyToEditor::iterator QuickEditorFactoryPrivate::unregisterProperty
(PropertyToEditor::iterator it)
{
    if (it == propertyToEditor.end()) {
        return it;
    }

    QQuickItem *editor = (*it);

    if (!editor) {
        return ++it;
    }

    editorToProperty.remove(editor);

    if (editor->isVisible()) {
        editor->setVisible(false);
        editor->disconnect();

        editor->deleteLater();
    }

    return propertyToEditor.erase(it);
}


////////////////////// QuickEditorFactory //////////////////////

QuickEditorFactory::QuickEditorFactory(QObject *parent) :
    QObject(parent),
    d(new QuickEditorFactoryPrivate)
{}

QuickEditorFactory::~QuickEditorFactory()
{
    auto it = d->propertyToEditor.begin();

    while (it != d->propertyToEditor.end()) {
        it = d->unregisterProperty(it);
    }
}

int QuickEditorFactory::type() const
{
    return d->type;
}

void QuickEditorFactory::setType(int type)
{
    Q_ASSERT_X(QMetaType::isRegistered(type), Q_FUNC_INFO,
               "An editor factory cannot target an invalid QMetaType");

    if (d->type != type) {
        d->type = type;

        Q_EMIT typeChanged(type);
    }
}

QQmlComponent *QuickEditorFactory::editor() const
{
    return d->delegate;
}

void QuickEditorFactory::setEditor(QQmlComponent *delegate)
{
    if (d->delegate != delegate) {
        d->delegate = delegate;

        Q_EMIT editorChanged(delegate);
    }
}

QQuickItem *QuickEditorFactory::addProperty(Property *prop,
                                            QQuickItem *parent)
{
    Q_ASSERT(type() == prop->type());

    QuickEditorIncubator incubator(this, prop, QQmlIncubator::Synchronous);

    QQmlComponent *myDelegate = editor();
    QQmlContext *ctx = myDelegate->creationContext();

    myDelegate->create(incubator, ctx);

    auto editor = qobject_cast<QQuickItem *>(incubator.object());
    editor->setParentItem(parent);

    d->registerPropertyAndEditor(prop, editor);

    return editor;
}

void QuickEditorFactory::removeProperty(Property *prop)
{
    PropertyToEditor::iterator it = d->propertyToEditor.find(prop);

    d->unregisterProperty(it);
}

QQuickItem *QuickEditorFactory::editorFromProperty(Property *prop) const
{
    return d->propertyToEditor.value(prop);
}

QVariantMap QuickEditorFactory::propertyData(Property *prop) const
{
    Q_ASSERT(prop->type() == d->type);

    PropertyFactory *propFactory = prop->manager()->propertyFactory();

    if (!propFactory) {
        propFactory = PropertyFactory::defaultFactory();
    }

    QVariantMap ret {
        {"display", prop->name()},
        {"toolTip", prop->toolTip()},
        {"statusTip", prop->statusTip()},
        {"whatsThis", prop->whatsThis()},

        {"type", prop->type()},
        {"isReadOnly", prop->isReadOnly()},
        {"isResettable", prop->isResettable()},
        {"value", prop->value()},
        {"valueString", propFactory->valueToString(prop)},
        {"defaultValue", prop->defaultValue()},
        {"hints", prop->hints()}
    };

    return ret;
}

void QuickEditorFactory::updateEditor(QQuickItem *editor) const
{
    Property *prop = d->editorToProperty.value(editor);

    if (prop) { // Avoid segfault during the editor initialization
        QQmlProperty::write(editor, "propertyData", propertyData(prop));
    }
}

void QuickEditorFactory::updateProperty(QQuickItem *editor,
                                        const QVariant &value)
{
    Property *prop = d->editorToProperty.value(editor);

    if (prop) { // Avoid segfault during the editor initialization
        Q_EMIT valueChanged(prop, value);

        updateEditor(editor);
    }
}


////////////////////// QuickEditorFactoryManagerPrivate //////////////////////

class QuickEditorFactoryManagerPrivate
{
public:
    QuickEditorFactoryManagerPrivate();

    int factoryToType(QuickEditorFactory *factory) const;
    void updateTypeToEditorMapping(QuickEditorFactory *factory, int newType);

    QList<QuickEditorFactory *> factories;
    QMap<int, QuickEditorFactory *> typeToFactory;
};

QuickEditorFactoryManagerPrivate::QuickEditorFactoryManagerPrivate() :
    factories(),
    typeToFactory()
{}

int QuickEditorFactoryManagerPrivate::factoryToType(QuickEditorFactory *factory) const
{
    auto it = typeToFactory.cbegin();

    while (it != typeToFactory.cend()) {
        int type = it.key();

        if (it.value() == factory) {
            return type;
        }

        ++it;
    }

    return QMetaType::UnknownType;
}

void QuickEditorFactoryManagerPrivate::updateTypeToEditorMapping
(QuickEditorFactory *factory, int newType)
{
    int oldType = factoryToType(factory);

    int removedCount = typeToFactory.remove(oldType);
    Q_ASSERT(removedCount <= 1);

    typeToFactory[newType] = factory;
}


////////////////////// QuickEditorFactoryManager //////////////////////

QuickEditorFactoryManager::QuickEditorFactoryManager(QObject *parent) :
    QObject(parent),
    d(new QuickEditorFactoryManagerPrivate)
{}

QuickEditorFactoryManager::~QuickEditorFactoryManager()
{}

QQmlListProperty<QuickEditorFactory> QuickEditorFactoryManager::factories()
{
    return QQmlListProperty<QuickEditorFactory>(this, &d->factories,
                                                QuickEditorFactoryManager::qmlAppendFactory,
                                                QuickEditorFactoryManager::qmFactoryCount,
                                                QuickEditorFactoryManager::qmlFactoryAt,
                                                QuickEditorFactoryManager::qmlClearFactories);
}

QuickEditorFactory *QuickEditorFactoryManager::editorFactory(int type) const
{
    if (!d->typeToFactory.contains(type)) {
        Core::Debug::qCritical_noEditorFactoryFound(type, "QuickEditorFactory");

        return nullptr;
    }

    QuickEditorFactory *factory = d->typeToFactory.value(type);
    Q_ASSERT(factory);

    return factory;
}

void QuickEditorFactoryManager::qmlAppendFactory(QQmlListProperty<QuickEditorFactory> *list,
                                                 QuickEditorFactory *factory)
{
    Q_ASSERT(factory);

    auto self = static_cast<QuickEditorFactoryManager *>(list->object);

    if (self->d->factories.contains(factory)) {
        self->d->factories.removeOne(factory);
    }

    self->d->factories.append(factory);

    Q_ASSERT(self->d->factories.count(factory) == 1);

    // Update the map (type -> editor)
    int editorType = factory->type();

    self->d->typeToFactory[editorType] = factory;

    // Connect the editor to the factory
    connect(factory, &QuickEditorFactory::typeChanged,
            self, [=] (int newType) {
        self->d->updateTypeToEditorMapping(factory, newType);
    });

    Q_EMIT self->editorFactoryAdded(factory);
}

QuickEditorFactory *QuickEditorFactoryManager::qmlFactoryAt(QQmlListProperty<QuickEditorFactory> *list,
                                                            int index)
{
    auto self = static_cast<QuickEditorFactoryManager *>(list->object);

    return self->d->factories.at(index);
}

int QuickEditorFactoryManager::qmFactoryCount(QQmlListProperty<QuickEditorFactory> *list)
{
    auto self = static_cast<QuickEditorFactoryManager *>(list->object);

    return self->d->factories.count();
}

void QuickEditorFactoryManager::qmlClearFactories(QQmlListProperty<QuickEditorFactory> *list)
{
    auto self = static_cast<QuickEditorFactoryManager *>(list->object);

    self->d->factories.clear();
}

} // namespace Quick
} // namespace PE
