#ifndef PEQUICK_PEQUICKPLUGIN_H
#define PEQUICK_PEQUICKPLUGIN_H

#include <QtQml/QQmlExtensionPlugin>

#include <PEQuick/pequick_global.h>

namespace PE {
namespace Quick {

class PEQUICKPLUGIN_EXPORT PEQuickPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri) override;
    void initializeEngine(QQmlEngine *engine, const char *uri) override;
};

} // namespace Quick
} // namespace PE

#endif // PEQUICK_PEQUICKPLUGIN_H
