import QtQuick 2.7
import QtQuick.Controls 1.4

import PE.Quick 1.0 as PEQuick

PEQuick.MVPropertyBrowser {
    property alias headerVisible : treeView.headerVisible
    property alias style: treeView.style

    property var currentIndex : undefined

    property Item __currentEditorRowItem : null
    property Item __currentEditor : null

    id: root

    function __closeCurrentEditor()
    {
        if (__currentEditor) {
            root.destroyEditor(currentIndex);
        }

        currentEditorRect.visible = false;
    }

    function onClicked(index)
    {
        if (currentIndex !== index) {
            __closeCurrentEditor();
        }
    }

    function onValueDoubleClicked(index)
    {
        currentIndex = index;

        __closeCurrentEditor();

        __currentEditor = root.createEditor(index, currentEditorRect);
        __currentEditor.anchors.fill = currentEditorRect;

        __currentEditorRowItem = treeView.__currentRowItem;

        currentEditorRect.visible = true;
    }

    function commitAndClose()
    {
        if (__currentEditor) {
            __currentEditor.focus = false;
        }

        __closeCurrentEditor();

        currentEditorRect.visible = false;
    }

    TreeView {
        id: treeView

        anchors.fill: parent

        model: root.itemModel()

        rowDelegate: Rectangle {
            SystemPalette { id: palette; colorGroup: SystemPalette.Active }

            visible: styleData.selected || styleData.alternate
            height: 19
            color: (styleData.selected) ? palette.highlight
                                        : (styleData.alternate) ? palette.alternateBase
                                                                : palette.base
        }

        Rectangle {
            id: currentEditorRect

            parent: __currentEditorRowItem

            visible: false
            color: "white"

            x: nameColumn.width
            width: valueColumn.width
            height: (__currentEditorRowItem) ? __currentEditorRowItem.height
                                             : 0
        }

        Rectangle {
            id: resizeHandle

            x: nameColumn.width
            width: 1
            height: treeView.height
            color: "#ccc"

            MouseArea {
                anchors.fill: parent
                anchors.leftMargin: -4
                anchors.rightMargin: -4
                cursorShape: Qt.SplitHCursor
                preventStealing: true

                onPositionChanged: {
                    var viewCoords = mapToItem(treeView, mouse.x, mouse.y);
                    nameColumn.width = viewCoords.x;
                }
            }
        }

        TableViewColumn {
            id: nameColumn
            title: "Property"
            role: "display"

            movable: false

            width: treeView.viewport.width / 2
        }

        TableViewColumn {
            id: valueColumn
            title: "Value"
            role: "valueString"

            movable: false

            width: treeView.viewport.width - nameColumn.width
        }

        // Event handling
        onClicked: root.onClicked(index)
        onDoubleClicked: root.onValueDoubleClicked(index)

        Keys.onReturnPressed: root.commitAndClose()
        Keys.onEnterPressed: root.commitAndClose()
        Keys.onEscapePressed: root.commitAndClose()
    }
}
