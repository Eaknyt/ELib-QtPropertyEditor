#ifndef PEQUICK_QUICKABSTRACTPROPERTYBROWSER_H
#define PEQUICK_QUICKABSTRACTPROPERTYBROWSER_H

#include <QtQuick/QQuickItem>

#include <PECore/ipropertybrowser.h>

namespace PE {

namespace Core {
class PropertyManager;
} // namespace Core

namespace Quick {

class QuickAbstractPropertyBrowserPrivate;
class QuickEditorFactory;
class QuickEditorFactoryManager;

class QuickAbstractPropertyBrowser : public Core::IPropertyBrowser<QQuickItem>
{
    Q_OBJECT

    Q_PROPERTY(PE::Core::PropertyManager *manager READ manager WRITE setManager NOTIFY managerChanged)
    Q_PROPERTY(PE::Quick::QuickEditorFactoryManager *editorFactoryManager
               READ editorFactoryManager WRITE setEditorFactoryManager
               NOTIFY editorFactoryManagerChanged)

public:
    virtual ~QuickAbstractPropertyBrowser();

    Core::PropertyManager *manager() const;
    QuickEditorFactoryManager *editorFactoryManager() const;

    Q_INVOKABLE
    QQuickItem *createEditor(Core::Property *prop,
                             QQuickItem *parent) const override;

    Q_INVOKABLE
    void destroyEditor(Core::Property *prop) const override;

public Q_SLOTS:
    void setManager(Core::PropertyManager *manager);
    void setEditorFactoryManager(QuickEditorFactoryManager *factoryManager);

Q_SIGNALS:
    void managerChanged(Core::PropertyManager *);
    void editorFactoryManagerChanged(QuickEditorFactoryManager *);

protected:
    explicit QuickAbstractPropertyBrowser(QQuickItem *parent = nullptr);
    QuickAbstractPropertyBrowser(QuickAbstractPropertyBrowserPrivate &dd,
                                 QQuickItem *parent = nullptr);

    virtual void connectManager();
    virtual void disconnectManager();

private Q_SLOTS:
    void onPropertyAdded(Core::Property *prop);
    void onPropertyRemoved(Core::Property *prop);
    void onPropertyChanged(Core::Property *prop);
    void onPropertyValueChanged(Core::Property *prop,
                                const QVariant &value);
    void onPropertyHintChanged(Core::Property *prop,
                               const QString &name,
                               const QVariant &value);
    void onPropertyReadOnlyChanged(Core::Property *prop, bool readOnly);
    void onPropertyResetChanged(Core::Property *prop, bool resettable);
    void onPropertyDefaultValueChanged(Core::Property *prop,
                                       const QVariant &defaultValue);

private Q_SLOTS:
    void onEditorFactoryAdded(QuickEditorFactory *factory) const;

protected:
    Q_DISABLE_COPY(QuickAbstractPropertyBrowser)
    Q_DECLARE_PRIVATE(QuickAbstractPropertyBrowser)

    const QScopedPointer<QuickAbstractPropertyBrowserPrivate> d_ptr;
};

} // namespace Quick
} // namespace PE

#endif // PEQUICK_QUICKABSTRACTPROPERTYBROWSER_H
