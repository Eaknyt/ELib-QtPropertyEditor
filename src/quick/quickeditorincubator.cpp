#include "quickeditorincubator.h"

#include <QtQml/QQmlProperty>

#include <QtQuick/QQuickItem>

#include <PECore/property.h>

#include <PEQuick/quickeditorfactory.h>

namespace PE {

using namespace Core;

namespace Quick {

QuickEditorIncubator::QuickEditorIncubator(const QuickEditorFactory *factory,
                                           Property *prop,
                                           IncubationMode mode) :
    QQmlIncubator(mode),
    m_factory(factory),
    m_property(prop)
{}

void QuickEditorIncubator::setInitialState(QObject *object)
{
    auto editor = qobject_cast<QQuickItem *>(object);

    QQmlProperty::write(editor, "propertyData",
                        m_factory->propertyData(m_property));
}

} // namespace Quick
} // namespace PE
