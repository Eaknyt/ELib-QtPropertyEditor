#ifndef PEQUICK_QUICKEDITORINCUBATOR_H
#define PEQUICK_QUICKEDITORINCUBATOR_H

#include <QtQml/QQmlIncubator>

namespace PE {

namespace Core {
class Property;
} // namespace Core

namespace Quick {

class QuickEditorFactory;

class QuickEditorIncubator : public QQmlIncubator
{
public:
    QuickEditorIncubator(const QuickEditorFactory *factory,
                         Core::Property *prop,
                         QQmlIncubator::IncubationMode mode);

protected:
    void setInitialState(QObject *object) override;

private:
    const QuickEditorFactory *m_factory;
    Core::Property *m_property;
};

} // namespace Quick
} // namespace PE

#endif // PEQUICK_QUICKEDITORINCUBATOR_H
