#ifndef PEQUICK_QUICKEDITORFACTORY_H
#define PEQUICK_QUICKEDITORFACTORY_H

#include <QtCore/QObject>

#include <QtQml/QQmlListProperty>

#include <PEQuick/pequick_global.h>

class QQmlComponent;
class QQuickItem;

namespace PE {
namespace Core {
class Property;
} // namespace Core

namespace Quick {

class QuickEditorFactoryPrivate;
class QuickEditorFactoryManagerPrivate;

class PEQUICKPLUGIN_EXPORT QuickEditorFactory : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QQmlComponent *editor READ editor WRITE setEditor NOTIFY editorChanged)

public:
    explicit QuickEditorFactory(QObject *parent = nullptr);
    ~QuickEditorFactory();

    int type() const;
    void setType(int type);

    QQmlComponent *editor() const;
    void setEditor(QQmlComponent *editor);

    QQuickItem *addProperty(Core::Property *prop, QQuickItem *parent);
    void removeProperty(Core::Property *prop);

    QQuickItem *editorFromProperty(Core::Property *prop) const;

    QVariantMap propertyData(Core::Property *prop) const;

protected:
    void updateEditor(QQuickItem *editor) const;

protected Q_SLOTS:
    void updateProperty(QQuickItem *editor, const QVariant &value);

Q_SIGNALS:
    void typeChanged(int type);
    void editorChanged(QQmlComponent *editor);

    void valueChanged(Core::Property *prop, const QVariant &value);

private:
    const QScopedPointer<QuickEditorFactoryPrivate> d;
};

class PEQUICKPLUGIN_EXPORT QuickEditorFactoryManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<PE::Quick::QuickEditorFactory> factories READ factories)
    Q_CLASSINFO("DefaultProperty", "factories")

public:
    explicit QuickEditorFactoryManager(QObject *parent = nullptr);
    ~QuickEditorFactoryManager();

    QQmlListProperty<QuickEditorFactory> factories();

    QuickEditorFactory *editorFactory(int type) const;

Q_SIGNALS:
    void editorFactoryAdded(QuickEditorFactory *);

private:
    static void qmlAppendFactory(QQmlListProperty<QuickEditorFactory> *list,
                                 QuickEditorFactory *factory);
    static QuickEditorFactory *qmlFactoryAt(QQmlListProperty<QuickEditorFactory> *list,
                                            int index);
    static int qmFactoryCount(QQmlListProperty<QuickEditorFactory> *list);
    static void qmlClearFactories(QQmlListProperty<QuickEditorFactory> *list);

private:
    const QScopedPointer<QuickEditorFactoryManagerPrivate> d;
};

} // namespace Quick
} // namespace PE

#endif // PEQUICK_QUICKEDITORFACTORY_H
