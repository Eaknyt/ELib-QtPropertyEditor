#ifndef PEQUICK_QUICKABSTRACTPROPERTYBROWSER_P_H
#define PEQUICK_QUICKABSTRACTPROPERTYBROWSER_P_H

namespace PE {

namespace Core {
class PropertyManager;
} // namespace Core

namespace Quick {

class QuickEditorFactory;
class QuickEditorFactoryManager;

class QuickAbstractPropertyBrowserPrivate
{
public:
    QuickAbstractPropertyBrowserPrivate(QuickAbstractPropertyBrowser *qq);

    /* Methods */
    void connectFactoryManager() const;
    void disconnectFactoryManager() const;

    void connectFactories(bool connect) const;
    void connectEditorFactory(QuickEditorFactory *factory) const;
    void disconnectEditorFactory(QuickEditorFactory *factory) const;

    /* Attributes */
    QuickAbstractPropertyBrowser *q_ptr;
    Core::PropertyManager *manager;
    QuickEditorFactoryManager *editorFactoryManager;
};

} // namespace Quick
} // namespace PE

#endif // PEQUICK_QUICKABSTRACTPROPERTYBROWSER_P_H
