#ifndef PEQUICK_PROPERTYMETATYPEHELPER_H
#define PEQUICK_PROPERTYMETATYPEHELPER_H

#include <QtCore/QMetaType>
#include <QtCore/QObject>

#include <PECoreExtras/enumtype.h>

namespace PE {
namespace Quick {

class PropertyMetaTypeHelper : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int enumMetaType READ enumMetaType CONSTANT)

public:
    enum Type {
        Bool = QMetaType::Bool,
        Int = QMetaType::Int,
        Double = QMetaType::Double,
        String = QMetaType::QString,
        Url = QMetaType::QUrl,
        Vector2D = QMetaType::QVector2D,
        Vector3D = QMetaType::QVector3D
    };
    Q_ENUM(Type)

    int enumMetaType() const
    {
        return qMetaTypeId<Core::Extras::EnumPropertyType>();
    }
};

} // namespace Quick
} // namespace PE

#endif // PEQUICK_PROPERTYMETATYPEHELPER_H
