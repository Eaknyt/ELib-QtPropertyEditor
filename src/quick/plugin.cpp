#include "plugin.h"

#include <QtQml/qqml.h>

#include <PECore/propertymanager.h>

#include <PEQuick/propertymetatypehelper.h>
#include <PEQuick/quickeditorfactory.h>
#include <PEQuick/quickmvpropertybrowser.h>

static void initResources()
{
    Q_INIT_RESOURCE(pequickplugin);
}

static QObject *singletonMetaTypeProvider(QQmlEngine *engine,
                                          QJSEngine *scriptEngine)
{
    Q_UNUSED(engine);
    Q_UNUSED(scriptEngine);

    auto singleton = new PE::Quick::PropertyMetaTypeHelper;

    return singleton;
}

namespace PE {

using namespace Core;

namespace Quick {

// Add your file-based qml types here
static const struct {
    const char *type;
    int major, minor;
} qmldir [] = {
    // { "fileName", majorVersion, minorVersion },
    { "TreePropertyBrowser", 1, 0 }
};

void PEQuickPlugin::registerTypes(const char *uri)
{
    initResources();

    // @uri PE.Quick
    for (int i = 0; i < int(sizeof(qmldir) / sizeof(qmldir[0])); i++) {
        QString path = QStringLiteral("qrc:/PE/Quick/");

        qmlRegisterType(QUrl(path + qmldir[i].type + QStringLiteral(".qml")),
                        uri,
                        qmldir[i].major, qmldir[i].minor,
                        qmldir[i].type);
    }

    qmlRegisterUncreatableType<Core::PropertyManager>(uri, 1, 0, "__PropertyManager", "PropertyManager is not instanciable from QML");
    qmlRegisterType<QuickEditorFactory>(uri, 1, 0, "EditorFactory");
    qmlRegisterType<QuickEditorFactoryManager>(uri, 1, 0, "EditorFactoryManager");
    qmlRegisterSingletonType<PropertyMetaTypeHelper>(uri, 1, 0, "MetaType", singletonMetaTypeProvider);

    qmlRegisterType<QuickMVPropertyBrowser>(uri, 1, 0, "MVPropertyBrowser");
}

void PEQuickPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_UNUSED(engine);
    Q_UNUSED(uri);
}

} // namespace Quick
} // namespace PE
