#include "quickmvpropertybrowser.h"

#include <PECore/property.h>
#include <PECore/propertyitemmodel.h>
#include <PECore/propertymanager.h>

#include <PEQuick/private/quickabstractpropertybrowser_p.h>

namespace PE {

using namespace Core;

namespace Quick {


////////////////////// QuickMVPropertyBrowserPrivate //////////////////////

class QuickMVPropertyBrowserPrivate : public QuickAbstractPropertyBrowserPrivate
{
public:
    QuickMVPropertyBrowserPrivate(QuickMVPropertyBrowser *qq);

    PropertyItemModel *model;
    QMap<Property *, PropertyItem *> propertyToItem;
};

QuickMVPropertyBrowserPrivate::QuickMVPropertyBrowserPrivate(QuickMVPropertyBrowser *qq) :
    QuickAbstractPropertyBrowserPrivate(qq),
    model(nullptr),
    propertyToItem()
{}


////////////////////// QuickMVPropertyBrowser //////////////////////

/*!
 \class The QuickAbstractPropertyBrowser class provides
*/

/*!
 \brief
*/
QuickMVPropertyBrowser::QuickMVPropertyBrowser(QQuickItem *parent) :
    QuickAbstractPropertyBrowser(*new QuickMVPropertyBrowserPrivate(this),
                                 parent)
{
    Q_D(QuickMVPropertyBrowser);

    d->model = new PropertyItemModel(this);
}

QuickMVPropertyBrowser::~QuickMVPropertyBrowser()
{
    Q_D(QuickMVPropertyBrowser);

    d->propertyToItem.clear();
}

QuickMVPropertyBrowser::QuickMVPropertyBrowser(QuickMVPropertyBrowserPrivate &dd,
                                               QQuickItem *parent) :
    QuickAbstractPropertyBrowser(dd, parent)
{}

/*!
 \brief
*/
QAbstractItemModel *QuickMVPropertyBrowser::itemModel() const
{
    Q_D(const QuickMVPropertyBrowser);

    return d->model;
}

QQuickItem *QuickMVPropertyBrowser::createEditor(const QModelIndex &index,
                                                 QQuickItem *parent)
{
    Q_D(QuickMVPropertyBrowser);

    Property *prop = d->model->item(index)->property();

    return QuickAbstractPropertyBrowser::createEditor(prop, parent);
}

void QuickMVPropertyBrowser::destroyEditor(const QModelIndex &index)
{
    Q_D(QuickMVPropertyBrowser);

    Property *prop = d->model->item(index)->property();

    QuickAbstractPropertyBrowser::destroyEditor(prop);
}

/*!
 \brief
*/
void QuickMVPropertyBrowser::addPropertyField(Property *prop)
{
    Q_D(QuickMVPropertyBrowser);

    auto item = new PropertyItem(prop);

    d->model->addItem(item);

    d->propertyToItem.insert(prop, item);
}

/*!
 \brief
*/
void QuickMVPropertyBrowser::removePropertyField(Property *prop)
{
    Q_D(QuickMVPropertyBrowser);

    PropertyItem *toRemove = d->propertyToItem.value(prop);
    Q_ASSERT(toRemove);

    const int removed = d->propertyToItem.remove(prop);
    Q_ASSERT(removed);

    d->model->removeItem(toRemove);
}

/*!
 \brief
*/
void QuickMVPropertyBrowser::updatePropertyField(Property *prop)
{
    Q_D(QuickMVPropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItem(item);
}

/*!
 \brief
*/
void QuickMVPropertyBrowser::updatePropertyFieldValue(Property *prop,
                                                      const QVariant &value)
{
    Q_UNUSED(value);

    Q_D(QuickMVPropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemValue(item);
}

/*!
 \brief
*/
void QuickMVPropertyBrowser::updatePropertyFieldHint(Property *prop,
                                                     const QString &name,
                                                     const QVariant &value)
{
    Q_UNUSED(name);
    Q_UNUSED(value);

    Q_D(QuickMVPropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemHints(item);
}

void QuickMVPropertyBrowser::updatePropertyFieldReadOnly(Property *prop,
                                                         bool readOnly)
{
    Q_UNUSED(readOnly);

    Q_D(QuickMVPropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemReadOnly(item);
}

void QuickMVPropertyBrowser::updatePropertyFieldResettable(Property *prop,
                                                           bool resettable)
{
    Q_UNUSED(resettable);

    Q_D(QuickMVPropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemResettable(item);
}

void QuickMVPropertyBrowser::updatePropertyFieldDefaultValue
(Property *prop, const QVariant &defaultValue)
{
    Q_UNUSED(defaultValue);

    Q_D(QuickMVPropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemDefaultValue(item);
}

} // namespace Quick
} // namespace PE
