import QtQuick 2.7
import QtQuick.Controls 1.4 as QQControls
import QtQuick.Dialogs 1.2

import PE.Quick.Extras 1.0 as PEQuickExtras

Item {
    id: urlEditor

    property url url : ""
    property string title : ""
    property url folder
    property var nameFilters : []
    property bool selectFolder : false

    readonly property color __defaultTextColor : lineEdit.textColor

    implicitWidth: lineEdit.implicitWidth + selectBtn.implicitWidth
    implicitHeight: lineEdit.implicitHeight

    QQControls.TextField {
        id: lineEdit

        anchors.fill: parent
        anchors.rightMargin: 45

        readOnly: true
    }

    QQControls.Button {
        id: selectBtn

        anchors.left: lineEdit.right
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top

        text: "..."

        onClicked: {
            fileDialog.open();
        }
    }

    FileDialog {
        id: fileDialog

        title: urlEditor.title
        folder: urlEditor.folder
        nameFilters: urlEditor.nameFilters
        selectFolder: urlEditor.selectFolder

        onAccepted: {
            lineEdit.text = PEQuickExtras.Utils.resolvedLocalUrl(fileUrl);

            urlEditor.url = fileUrl;
        }
    }
}
