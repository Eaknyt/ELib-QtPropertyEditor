import QtQuick 2.7
import QtQuick.Controls 1.4 as QQControls

FocusScope {
    id: dimEditor

    property alias value: dimSpinBox.value
    property alias decimals: dimSpinBox.decimals
    property alias minimumValue : dimSpinBox.minimumValue
    property alias maximumValue : dimSpinBox.maximumValue
    property alias stepSize : dimSpinBox.stepSize

    property alias indicatorColor : dimIndicator.color
    property alias indicatorText : dimText.text

    property alias style : dimSpinBox.style

    signal dimValueChanged(double value)

    implicitWidth: dimSpinBox.implicitWidth + dimIndicator.width
    implicitHeight: dimSpinBox.implicitHeight

    Rectangle {
        id: dimIndicator

        anchors.left: dimEditor.left
        anchors.top: dimEditor.top
        anchors.bottom: dimEditor.bottom

        width: dimText.implicitWidth + 4
        radius: 1

        Text {
            id: dimText

            anchors.fill: dimIndicator
            verticalAlignment: Qt.AlignVCenter
            horizontalAlignment: Qt.AlignHCenter

            color: "white"
        }
    }

    QQControls.SpinBox {
        id: dimSpinBox

        focus: true

        decimals: 2

        anchors.left: dimIndicator.right
        anchors.top: dimEditor.top
        anchors.bottom: dimEditor.bottom
        anchors.right: dimEditor.right

        onValueChanged: {
            dimEditor.dimValueChanged(value)
        }
    }
}

