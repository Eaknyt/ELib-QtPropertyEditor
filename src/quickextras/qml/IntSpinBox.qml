import QtQuick.Controls 1.4 as QQControls

QQControls.SpinBox {
    property int intValue : value

    decimals: 0
}
