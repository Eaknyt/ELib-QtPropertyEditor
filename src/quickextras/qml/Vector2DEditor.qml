import QtQuick 2.7
import QtQuick.Controls 1.4 as QQControls
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1 as QQLayouts

import PE.Quick.Extras 1.0 as PEQuickExtras

FocusScope {
    id: vec2dEditor

    property alias xMin : xEditor.minimumValue
    property alias xMax : xEditor.maximumValue

    property alias yMin : yEditor.minimumValue
    property alias yMax : yEditor.maximumValue

    property alias xStepSize : xEditor.stepSize
    property alias yStepSize : yEditor.stepSize

    property int decimals : 2

    property SpinBoxStyle style : null

    property vector2d value : Qt.vector2d(xEditor.value, yEditor.value)

    implicitWidth: layout.implicitWidth
    implicitHeight: layout.implicitHeight

    onStyleChanged: {
        xEditor.style = style;
        yEditor.style = style;
    }

    QQLayouts.RowLayout {
        id: layout

        spacing: 3

        anchors.fill: vec2dEditor

        PEQuickExtras.VectorDimSpinBox {
            id: xEditor

            focus: true

            value: vec2dEditor.value.x

            indicatorColor: "red"
            indicatorText: "X"

            QQLayouts.Layout.fillWidth: true
            QQLayouts.Layout.fillHeight: true

            onDimValueChanged: {
                var newValue = Qt.vector2d(value, yEditor.value);

                vec2dEditor.value = newValue;
            }
        }

        PEQuickExtras.VectorDimSpinBox {
            id: yEditor

            value: vec2dEditor.value.y

            indicatorColor: Qt.rgba(0.24, 0.73, 0.13, 1.0)
            indicatorText: "Y"

            QQLayouts.Layout.fillWidth: true
            QQLayouts.Layout.fillHeight: true

            onDimValueChanged: {
                var newValue = Qt.vector2d(xEditor.value, value);

                vec2dEditor.value = newValue;
            }
        }
    }
}
