import QtQuick 2.7
import QtQuick.Controls 1.4 as QQControls
import QtQuick.Layouts 1.1 as QQLayouts
import QtQuick.Controls.Styles 1.4

import PE.Quick.Extras 1.0 as PEQuickExtras

FocusScope {
    id: vec3dEditor

    property alias xMin : xEditor.minimumValue
    property alias xMax : xEditor.maximumValue

    property alias yMin : yEditor.minimumValue
    property alias yMax : yEditor.maximumValue

    property alias zMin : zEditor.minimumValue
    property alias zMax : zEditor.maximumValue

    property alias xStepSize : xEditor.stepSize
    property alias yStepSize : yEditor.stepSize
    property alias zStepSize : zEditor.stepSize

    property int decimals : 2

    property SpinBoxStyle style : null

    property vector3d value : Qt.vector3d(xEditor.value, yEditor.value,
                                          zEditor.value)

    implicitWidth: layout.implicitWidth
    implicitHeight: layout.implicitHeight

    onStyleChanged: {
        xEditor.style = style;
        yEditor.style = style;
        zEditor.style = style;
    }

    QQLayouts.RowLayout {
        id: layout

        spacing: 3

        anchors.fill: vec3dEditor

        PEQuickExtras.VectorDimSpinBox {
            id: xEditor

            focus: true

            value: vec3dEditor.value.x

            indicatorColor: "red"
            indicatorText: "X"

            QQLayouts.Layout.fillWidth: true
            QQLayouts.Layout.fillHeight: true

            onDimValueChanged: {
                var newValue = Qt.vector3d(value, yEditor.value,
                                           zEditor.value);

                vec3dEditor.value = newValue;
            }
        }

        PEQuickExtras.VectorDimSpinBox {
            id: yEditor

            value: vec3dEditor.value.y

            indicatorColor: Qt.rgba(0.24, 0.73, 0.13, 1.0)
            indicatorText: "Y"

            QQLayouts.Layout.fillWidth: true
            QQLayouts.Layout.fillHeight: true

            onDimValueChanged: {
                var newValue = Qt.vector3d(xEditor.value, value,
                                           zEditor.value);

                vec3dEditor.value = newValue;
            }
        }

        PEQuickExtras.VectorDimSpinBox {
            id: zEditor

            value: vec3dEditor.value.z

            indicatorColor: Qt.rgba(0.26, 0.54, 1.0, 1.0)
            indicatorText: "Z"

            QQLayouts.Layout.fillWidth: true
            QQLayouts.Layout.fillHeight: true

            onDimValueChanged: {
                var newValue = Qt.vector3d(xEditor.value, yEditor.value,
                                           value);

                vec3dEditor.value = newValue;
            }
        }
    }
}
