import QtQuick 2.7
import QtQuick.Controls 1.4 as QQControls
import QtQuick.Layouts 1.1 as QQLayouts

import PE.Quick 1.0 as PEQuick
import PE.Quick.Extras 1.0 as PEQuickExtras

PEQuick.EditorFactoryManager {
    factories: [
        PEQuick.EditorFactory {
            id: checkBoxFactory
            type: PEQuick.MetaType.Bool

            editor: QQControls.CheckBox {
                property var propertyData

                checked: propertyData.value

                onCheckedChanged: {
                    checkBoxFactory.updateProperty(this, checked);
                }
            }
        },

        PEQuick.EditorFactory {
            id: spinBoxFactory
            type: PEQuick.MetaType.Int

            editor: PEQuickExtras.IntSpinBox {
                property var propertyData

                value: propertyData.value

                minimumValue: propertyData.hints.minimum
                maximumValue: propertyData.hints.maximum
                stepSize: propertyData.hints.stepSize
                suffix: propertyData.hints.suffix

                property bool setOnFly : propertyData.hints.setOnFly

                onIntValueChanged: {
                    if (setOnFly) {
                        spinBoxFactory.updateProperty(this, intValue);
                    }
                }

                onEditingFinished: {
                    if (!setOnFly) {
                        spinBoxFactory.updateProperty(this, intValue);
                    }
                }
            }
        },

        PEQuick.EditorFactory {
            id: doubleSpinBoxFactory
            type: PEQuick.MetaType.Double

            editor: QQControls.SpinBox {
                property var propertyData

                value: propertyData.value

                minimumValue: propertyData.hints.minimum
                maximumValue: propertyData.hints.maximum
                stepSize: propertyData.hints.stepSize
                decimals: propertyData.hints.decimals
                suffix: propertyData.hints.suffix

                property bool setOnFly : propertyData.hints.setOnFly

                onValueChanged: {
                    if (setOnFly) {
                        doubleSpinBoxFactory.updateProperty(this, value);
                    }
                }

                onEditingFinished: {
                    if (!setOnFly) {
                        doubleSpinBoxFactory.updateProperty(this, value);
                    }
                }
            }
        },

        PEQuick.EditorFactory {
            id: textFieldFactory
            type: PEQuick.MetaType.String

            editor: QQControls.TextField {
                property var propertyData

                text: propertyData.value

                validator: RegExpValidator {
                    regExp: propertyData.hints.regex
                }

                echoMode: propertyData.hints.echoMode
                readOnly: propertyData.hints.readOnly
                placeholderText: propertyData.hints.placeholderText

                property bool setOnFly : propertyData.hints.setOnFly

                function emitValueChanged()
                {
                    textFieldFactory.updateProperty(this, text);
                }

                function connectSetOnFly()
                {
                    if (setOnFly) {
                        textChanged.connect(emitValueChanged);
                        editingFinished.disconnect(emitValueChanged);
                    }
                    else {
                        editingFinished.connect(emitValueChanged);
                        textChanged.disconnect(emitValueChanged);
                    }
                }

                onSetOnFlyChanged: connectSetOnFly()
                Component.onCompleted: connectSetOnFly()
            }
        },

        PEQuick.EditorFactory {
            id: comboBoxFactory
            type: PEQuick.MetaType.enumMetaType

            editor: QQControls.ComboBox {
                property var propertyData

                //TODO handle currentIndex
                model: propertyData.hints.possibleValues

                onCurrentTextChanged: {
                    comboBoxFactory.updateProperty(this, currentText)
                }
            }
        },

        PEQuick.EditorFactory {
            id: urlEditorFactory
            type: PEQuick.MetaType.Url

            editor: PEQuickExtras.UrlEditor {
                property var propertyData

                url: propertyData.value

                title: propertyData.hints.title
                folder: propertyData.hints.folder
                nameFilters: propertyData.hints.nameFilters
                selectFolder: propertyData.hints.selectFolder

                onUrlChanged: {
                    urlEditorFactory.updateProperty(this, url);
                }
            }
        },

        PEQuick.EditorFactory {
            id: vec2dEditorFactory
            type: PEQuick.MetaType.Vector2D

            editor: PEQuickExtras.Vector2DEditor {
                property var propertyData

                value: propertyData.value

                xMin: propertyData.hints.xMin
                xMax: propertyData.hints.xMax
                yMin: propertyData.hints.yMin
                yMax: propertyData.hints.yMax
                decimals: propertyData.hints.decimals
                xStepSize: propertyData.hints.stepSize
                yStepSize: propertyData.hints.stepSize

                onValueChanged: {
                    vec2dEditorFactory.updateProperty(this, value)
                }
            }
        },

        PEQuick.EditorFactory {
            id: vec3dEditorFactory
            type: PEQuick.MetaType.Vector3D

            editor: PEQuickExtras.Vector3DEditor {
                property var propertyData

                value: propertyData.value

                xMin: propertyData.hints.xMin
                xMax: propertyData.hints.xMax
                yMin: propertyData.hints.yMin
                yMax: propertyData.hints.yMax
                zMin: propertyData.hints.zMin
                zMax: propertyData.hints.zMax
                decimals: propertyData.hints.decimals
                xStepSize: propertyData.hints.stepSize
                yStepSize: propertyData.hints.stepSize
                zStepSize: propertyData.hints.stepSize

                onValueChanged: {
                    vec3dEditorFactory.updateProperty(this, value)
                }
            }
        }
    ]
}
