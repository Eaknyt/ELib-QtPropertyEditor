#include "pequickextrasutils.h"

#include <QtCore/QFileInfo>
#include <QtCore/QString>
#include <QtCore/QtGlobal>

namespace PE {
namespace Quick {
namespace Extras {

PEQuickExtrasUtil::PEQuickExtrasUtil(QObject *parent) :
    QObject(parent)
{}

QUrl PEQuickExtrasUtil::resolvedLocalUrl(const QUrl &fileUrl)
{
    QString urlString = fileUrl.toString();


#if defined (Q_OS_WIN)
    urlString.remove("file:///");
#elif defined (Q_OS_LINUX) || defined(Q_OS_UNIX) || defined (Q_OS_OSX)
    urlString.remove("file://");
#endif

    return urlString;
}

bool PEQuickExtrasUtil::urlsIsValid(const QUrl &fileUrl)
{
    return QFileInfo::exists(fileUrl.toString());
}

} // namespace Extras
} // namespace Quick
} // namespace PE
