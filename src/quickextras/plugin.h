#ifndef PEQUICKEXTRAS_PEQUICKEXTRASPLUGIN_H
#define PEQUICKEXTRAS_PEQUICKEXTRASPLUGIN_H

#include <QtQml/QQmlExtensionPlugin>

#include <PEQuickExtras/pequickextras_global.h>

namespace PE {
namespace Quick {
namespace Extras {

class PEQUICKEXTRAS_EXPORT PEQuickExtrasPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri) override;
    void initializeEngine(QQmlEngine *engine, const char *uri) override;
};

} // namespace Extras
} // namespace Quick
} // namespace PE

#endif // PEQUICKEXTRAS_PEQUICKEXTRASPLUGIN_H
