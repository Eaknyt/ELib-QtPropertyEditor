import qbs

import Reusables

Reusables.QmlPlugin {
    name: "PEQuickExtras"

    qmlplugin.uri: "PE.Quick.Extras"
    qmlplugin.pluginClassName: "PEQuickExtrasPlugin"
    qmlplugin.useControlsStyles: true

    cpp.defines: "PEQUICKEXTRAS_LIB"
    cpp.cxxLanguageVersion: project.cppLanguageVersion
    cpp.includePaths: [ project.includePath ]
    cpp.libraryPaths: project.libraryAbsInstallPath
    cpp.rpaths: project.libraryAbsInstallPath

    Depends { name: "PECore" }
    Depends { name: "PEQuick" }

    files: [
        "qml/*.qml",
        "plugin.cpp",
        "plugin.h",
        "pequickextras_global.h",
        "pequickextrasutils.cpp",
        "pequickextrasutils.h"
    ]
}
