#include "plugin.h"

#include <QtQml/qqml.h>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlEngine>

#include <PEQuickExtras/pequickextrasutils.h>

static void initResources()
{
    Q_INIT_RESOURCE(pequickextrasplugin);
}

static QObject *singletonUtilsProvider(QQmlEngine *engine,
                                       QJSEngine *scriptEngine)
{
    Q_UNUSED(engine);
    Q_UNUSED(scriptEngine);

    auto singleton = new PE::Quick::Extras::PEQuickExtrasUtil;

    return singleton;
}

namespace PE {
namespace Quick {
namespace Extras {

// Add your file-based qml types here
static const struct {
    const char *type;
    int major, minor;
} qmldir [] = {
    // { "fileName", majorVersion, minorVersion },
    { "IntSpinBox", 1, 0 },
    { "UrlEditor", 1, 0},
    { "QQC1_EditorFactoryManager", 1, 0},
    { "VectorDimSpinBox", 1, 0},
    { "Vector2DEditor", 1, 0},
    { "Vector3DEditor", 1, 0}
};

void PEQuickExtrasPlugin::registerTypes(const char *uri)
{
    initResources();

    // @uri PE.Quick.Extras
    for (int i = 0; i < int(sizeof(qmldir) / sizeof(qmldir[0])); i++) {
        QString path = QStringLiteral("qrc:/PE/Quick/Extras/");

        qmlRegisterType(QUrl(path + qmldir[i].type + QStringLiteral(".qml")),
                        uri,
                        qmldir[i].major, qmldir[i].minor,
                        qmldir[i].type);
    }

    qmlRegisterSingletonType<PEQuickExtrasPlugin>(uri, 1, 0, "Utils",
                                                  singletonUtilsProvider);
}

void PEQuickExtrasPlugin::initializeEngine(QQmlEngine *engine,
                                             const char *uri)
{
    Q_UNUSED(engine);
    Q_UNUSED(uri);
}

} // namespace Extras
} // namespace Quick
} // namespace PE
