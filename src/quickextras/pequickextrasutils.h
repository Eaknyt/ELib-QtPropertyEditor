#ifndef PEQUICKEXTRAS_QUICKPEEXTRASUTIL_H
#define PEQUICKEXTRAS_QUICKPEEXTRASUTIL_H

#include <QtCore/QObject>
#include <QtCore/QUrl>

namespace PE {
namespace Quick {
namespace Extras {

class PEQuickExtrasUtil : public QObject
{
    Q_OBJECT

public:
    PEQuickExtrasUtil(QObject *parent = nullptr);

    Q_INVOKABLE QUrl resolvedLocalUrl(const QUrl &fileUrl);
    Q_INVOKABLE bool urlsIsValid(const QUrl &fileUrl);
};

} // namespace Extras
} // namespace Quick
} // namespace PE

#endif // PEQUICKEXTRAS_QUICKPEEXTRASUTIL_H
