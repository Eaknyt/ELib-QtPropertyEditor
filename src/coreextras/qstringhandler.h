#ifndef PECOREEXTRAS_PROPERTYQSTRINGHANDLER_H
#define PECOREEXTRAS_PROPERTYQSTRINGHANDLER_H

#include <PECore/abstractpropertyhandler.h>

#include <PECoreExtras/pecoreextras_global.h>

namespace PE {
namespace Core {

class Property;

namespace Extras {

class PECOREEXTRAS_EXPORT PropertyQStringHandler : public PropertyHandler<QString>
{
public:
    PropertyQStringHandler();

    QString valueToString(const Property *prop) const override;
    QVariantMap defaultHints() const override;
};

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYQSTRINGHANDLER_H

