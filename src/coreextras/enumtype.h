#ifndef PECOREEXTRAS_ENUMTYPE_H
#define PECOREEXTRAS_ENUMTYPE_H

#include <QtCore/QMetaType>

namespace PE {
namespace Core {
namespace Extras {

struct EnumPropertyType
{
    EnumPropertyType() :
        currentIndex(0)
    {}

    EnumPropertyType (int i) :
        currentIndex(i)
    {}

    int currentIndex;
};

} // namespace Extras
} // namespace Core
} // namespace PE

Q_DECLARE_METATYPE(PE::Core::Extras::EnumPropertyType)

#endif // PECOREEXTRAS_ENUMTYPE_H
