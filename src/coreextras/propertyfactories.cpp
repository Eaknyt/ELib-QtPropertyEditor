#include "propertyfactories.h"

#include <PECore/propertyfactory.h>

#include <PECoreExtras/boolhandler.h>
#include <PECoreExtras/doublehandler.h>
#include <PECoreExtras/enumhandler.h>
#include <PECoreExtras/inthandler.h>
#include <PECoreExtras/qstringhandler.h>
#include <PECoreExtras/qurlhandler.h>
#include <PECoreExtras/qvector2dhandler.h>
#include <PECoreExtras/qvector3dhandler.h>

namespace PE {
namespace Core {
namespace Extras {

Core::PropertyFactory *qtPropertyFactory()
{
    static PropertyFactory ret;

    static std::vector<std::unique_ptr<AbstractPropertyHandler>> typeHandlers;

    if (typeHandlers.empty()) {
        typeHandlers.push_back(std::make_unique<PropertyBoolHandler>());
        typeHandlers.push_back(std::make_unique<PropertyIntHandler>());
        typeHandlers.push_back(std::make_unique<PropertyDoubleHandler>());
        typeHandlers.push_back(std::make_unique<PropertyQStringHandler>());
        typeHandlers.push_back(std::make_unique<PropertyEnumHandler>());
        typeHandlers.push_back(std::make_unique<PropertyUrlHandler>());
        typeHandlers.push_back(std::make_unique<PropertyVector2DHandler>());
        typeHandlers.push_back(std::make_unique<PropertyVector3DHandler>());

        for (std::unique_ptr<AbstractPropertyHandler> &handler : typeHandlers) {
            ret.registerTypeHandler(std::move(handler));
        }

    }

    return &ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE
