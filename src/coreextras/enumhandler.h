#ifndef PECOREEXTRAS_PROPERTYENUMHANDLER_H
#define PECOREEXTRAS_PROPERTYENUMHANDLER_H

#include <PECore/abstractpropertyhandler.h>

#include <PECoreExtras/enumtype.h>
#include <PECoreExtras/pecoreextras_global.h>

namespace PE {
namespace Core {

class Property;

namespace Extras {

class PECOREEXTRAS_EXPORT PropertyEnumHandler : public PropertyHandler<EnumPropertyType>
{
public:
    PropertyEnumHandler();

    QString valueToString(const Property *prop) const override;
    QVariantMap defaultHints() const override;
};

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYENUMHANDLER_H

