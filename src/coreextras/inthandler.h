#ifndef PECOREEXTRAS_PROPERTYINTLHANDLER_H
#define PECOREEXTRAS_PROPERTYINTLHANDLER_H

#include <PECore/abstractpropertyhandler.h>

#include <PECoreExtras/pecoreextras_global.h>

namespace PE {
namespace Core {

class Property;

namespace Extras {

class PECOREEXTRAS_EXPORT PropertyIntHandler : public PropertyHandler<int>
{
public:
    PropertyIntHandler();

    QString valueToString(const Property *prop) const override;
    QVariantMap defaultHints() const override;
};

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYINTLHANDLER_H
