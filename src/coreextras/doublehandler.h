#ifndef PECOREEXTRAS_PROPERTYDOUBLEHANDLER_H
#define PECOREEXTRAS_PROPERTYDOUBLEHANDLER_H

#include <PECore/abstractpropertyhandler.h>

#include <PECoreExtras/pecoreextras_global.h>

namespace PE {
namespace Core {

class Property;

namespace Extras {

class PECOREEXTRAS_EXPORT PropertyDoubleHandler : public PropertyHandler<double>
{
public:
    PropertyDoubleHandler();

    QString valueToString(const Property *prop) const override;
    QVariantMap defaultHints() const override;
};

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYDOUBLEHANDLER_H

