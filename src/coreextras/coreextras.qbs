import qbs

import Reusables

Reusables.PublicLibrary {
    name: "PECoreExtras"
    targetName: "pecoreextras"

    cpp.defines: "PE_COREEXTRAS_LIB"
    cpp.cxxLanguageVersion: project.cppLanguageVersion
    cpp.includePaths: [ project.includePath ]

    Depends {
        name: "Qt"
        submodules: [ "core", "gui" ]
    }

    Depends { name: "PECore" }

    files: [
        "pecoreextras_global.h",
        "boolhandler.cpp",
        "boolhandler.h",
        "propertyfactories.cpp",
        "propertyfactories.h",
        "inthandler.cpp",
        "inthandler.h",
        "doublehandler.cpp",
        "doublehandler.h",
        "qstringhandler.cpp",
        "qstringhandler.h",
        "enumhandler.cpp",
        "enumhandler.h",
        "qurlhandler.cpp",
        "qurlhandler.h",
        "qvector2dhandler.cpp",
        "qvector2dhandler.h",
        "qvector3dhandler.cpp",
        "qvector3dhandler.h",
        "helpers.h",
        "enumtype.h"
    ]

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: project.libDir
    }
}
