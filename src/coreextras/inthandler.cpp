#include "inthandler.h"

#include <PECore/property.h>

namespace PE {

namespace Core {
namespace Extras {

PropertyIntHandler::PropertyIntHandler() :
    PropertyHandler()
{}

QString PropertyIntHandler::valueToString(const Property *prop) const
{
    const int rawValue = prop->value().toInt();
    QString suffix = prop->hints()["suffix"].toString();

    return QString::number(rawValue) + suffix;
}

QVariantMap PropertyIntHandler::defaultHints() const
{
    QVariantMap ret {
        {"minimum", 0},
        {"maximum", 99},
        {"stepSize", 1},
        {"setOnFly", false},
        {"suffix", QString()}
    };

    return ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE

