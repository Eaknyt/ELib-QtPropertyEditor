#include "enumhandler.h"

#include <PECore/property.h>

namespace PE {
namespace Core {
namespace Extras {

PropertyEnumHandler::PropertyEnumHandler() :
    PropertyHandler()
{}

QString PropertyEnumHandler::valueToString(const Property *prop) const
{
    const auto rawValue = prop->value().value<EnumPropertyType>();
    const int index = rawValue.currentIndex;

    return prop->hints()["possibleValues"].toStringList()[index];
}

QVariantMap PropertyEnumHandler::defaultHints() const
{
    QVariantMap ret {
        {"possibleValues", QStringList()}
    };

    return ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE

