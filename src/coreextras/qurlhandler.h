#ifndef PECOREEXTRAS_PROPERTYQURLHANDLER_H
#define PECOREEXTRAS_PROPERTYQURLHANDLER_H

#include <PECore/abstractpropertyhandler.h>

#include <PECoreExtras/pecoreextras_global.h>

namespace PE {
namespace Core {

class Property;

namespace Extras {

class PECOREEXTRAS_EXPORT PropertyUrlHandler :  public PropertyHandler<QUrl>
{
public:
    PropertyUrlHandler();

    QString valueToString(const Property *prop) const override;
    QVariantMap defaultHints() const override;
};

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYQURLHANDLER_H

