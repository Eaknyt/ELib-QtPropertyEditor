#include "qstringhandler.h"

#include <QtGui/QGuiApplication>
#include <QtGui/QStyleHints>

#include <PECore/property.h>

namespace PE {

namespace Core {
namespace Extras {

PropertyQStringHandler::PropertyQStringHandler() :
    PropertyHandler()
{}

QString PropertyQStringHandler::valueToString(const Property *prop) const
{
    const QString text = prop->value().toString();
    const int echoMode = prop->hints()["echoMode"].toInt();

    QString ret;

    switch (echoMode) {
    // QLineEdit::Normal
    case 0:
        ret = text;
        break;
    // QLineEdit::NoEcho
    case 1:
        break;
    // QLineEdit::Password || QLineEdit ::PasswordEchoOnEdit
    case 2:
    case 3:
        ret.fill(QGuiApplication::styleHints()->passwordMaskCharacter(),
                 text.size());
        break;
    default:
        break;
    }

    return ret;
}

QVariantMap PropertyQStringHandler::defaultHints() const
{
    QVariantMap ret {
        {"regex", QRegExp(".*")},
        {"echoMode", 0}, // QLineEdit::Normal
        {"readOnly", false},
        {"placeholderText", QString()},
        {"setOnFly", true},
    };

    return ret;
}


} // namespace Extras
} // namespace Core
} // namespace PE

