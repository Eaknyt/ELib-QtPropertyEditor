#ifndef PECOREEXTRAS_PROPERTYFACTORIES_H
#define PECOREEXTRAS_PROPERTYFACTORIES_H

namespace PE {
namespace Core {

class PropertyFactory;

namespace Extras {

Core::PropertyFactory *qtPropertyFactory();

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYFACTORIES_H
