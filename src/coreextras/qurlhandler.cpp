#include "qurlhandler.h"

#include <QtCore/QUrl>

#include <PECore/property.h>

namespace PE {

namespace Core {
namespace Extras {

PropertyUrlHandler::PropertyUrlHandler() :
    PropertyHandler()
{}

QString PropertyUrlHandler::valueToString(const Property *prop) const
{
    const QUrl url = prop->value().toUrl();

    QString ret;

    if (url.isEmpty()) {
        ret = "<empty>";
    }
    else {
        ret = url.toString();
    }

    return ret;
}

QVariantMap PropertyUrlHandler::defaultHints() const
{
    QVariantMap ret {
        {"title", QString("Select file")},
        {"folder", QUrl()},
        {"nameFilters", QStringList()},
        {"selectFolder", false}
    };

    return ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE

