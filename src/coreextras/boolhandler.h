#ifndef PECOREEXTRAS_PROPERTYBOOLHANDLER_H
#define PECOREEXTRAS_PROPERTYBOOLHANDLER_H

#include <PECore/abstractpropertyhandler.h>

#include <PECoreExtras/pecoreextras_global.h>

namespace PE {
namespace Core {

class Property;

namespace Extras {

class PECOREEXTRAS_EXPORT PropertyBoolHandler : public PropertyHandler<bool>
{
public:
    PropertyBoolHandler();

    QString valueToString(const Property *prop) const override;
};

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYBOOLHANDLER_H
