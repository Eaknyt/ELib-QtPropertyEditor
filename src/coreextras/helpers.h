#ifndef PECOREEXTRAS_HELPERS_H
#define PECOREEXTRAS_HELPERS_H

#include <QtCore/QMetaEnum>
#include <QtCore/QStringList>

namespace PE {
namespace Core {
namespace Extras {

template <typename T>
static QStringList enumKeys()
{
    const QMetaEnum metaEnum = QMetaEnum::fromType<T>();

    QStringList ret;

    for (int i = 0; i < metaEnum.keyCount(); ++i) {
        ret += metaEnum.key(i);;
    }

    return ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_HELPERS_H
