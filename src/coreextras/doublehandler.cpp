#include "doublehandler.h"

#include <PECore/property.h>

namespace PE {

namespace Core {
namespace Extras {

PropertyDoubleHandler::PropertyDoubleHandler() :
    PropertyHandler()
{}

QString PropertyDoubleHandler::valueToString(const Property *prop) const
{
    const double rawValue = prop->value().toDouble();
    QString suffix = prop->hints()["suffix"].toString();

    return QString::number(rawValue) + suffix;
}

QVariantMap PropertyDoubleHandler::defaultHints() const
{
    QVariantMap ret {
        {"minimum", 0.},
        {"maximum", 99.},
        {"stepSize", 1.},
        {"decimals", 2},
        {"setOnFly", false},
        {"suffix", QString()}
    };

    return ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE

