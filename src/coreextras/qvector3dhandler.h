#ifndef PECOREEXTRAS_PROPERTYQVECTOR3DHANDLER_H
#define PECOREEXTRAS_PROPERTYQVECTOR3DHANDLER_H

#include <PECore/abstractpropertyhandler.h>

#include <PECoreExtras/pecoreextras_global.h>

namespace PE {
namespace Core {

class Property;

namespace Extras {

class PECOREEXTRAS_EXPORT PropertyVector3DHandler :
        public PropertyHandler<QVector3D>
{
public:
    PropertyVector3DHandler();

    QString valueToString(const Property *prop) const override;
    QVariantMap defaultHints() const override;
};

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYQVECTOR3DHANDLER_H

