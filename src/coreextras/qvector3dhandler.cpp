#include "qvector3dhandler.h"

#include <QtGui/QVector3D>

#include <PECore/property.h>

namespace PE {

namespace Core {
namespace Extras {

PropertyVector3DHandler::PropertyVector3DHandler() :
    PropertyHandler()
{}

QString PropertyVector3DHandler::valueToString(const Property *prop) const
{
    const auto rawValue = prop->value().value<QVector3D>();

    QString ret = "QVector3D(" + QString::number(rawValue.x()) + ", "
            + QString::number(rawValue.y()) + ", "
            + QString::number(rawValue.z()) + ")";

    return ret;
}

QVariantMap PropertyVector3DHandler::defaultHints() const
{
    const double defaultMin = 0.;
    const double defaultMax = 99.;

    QVariantMap ret {
        {"xMin", defaultMin},
        {"xMax", defaultMax},
        {"yMin", defaultMin},
        {"yMax", defaultMax},
        {"zMin", defaultMin},
        {"zMax", defaultMax},
        {"decimals", 2},
        {"stepSize", 1.}
    };

    return ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE

