#include "qvector2dhandler.h"

#include <QtGui/QVector2D>

#include <PECore/property.h>

namespace PE {

namespace Core {
namespace Extras {

PropertyVector2DHandler::PropertyVector2DHandler() :
    PropertyHandler()
{}

QString PropertyVector2DHandler::valueToString(const Property *prop) const
{
    const auto rawValue = prop->value().value<QVector2D>();

    QString ret = "QVector2D(" + QString::number(rawValue.x()) + ", "
            + QString::number(rawValue.y()) + ")";

    return ret;
}

QVariantMap PropertyVector2DHandler::defaultHints() const
{
    const double defaultMin = 0.;
    const double defaultMax = 99.;

    QVariantMap ret {
        {"xMin", defaultMin},
        {"xMax", defaultMax},
        {"yMin", defaultMin},
        {"yMax", defaultMax},
        {"decimals", 2},
        {"stepSize", 1.}
    };

    return ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE

