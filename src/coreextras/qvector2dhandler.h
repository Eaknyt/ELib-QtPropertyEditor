#ifndef PECOREEXTRAS_PROPERTYQVECTOR2DHANDLER_H
#define PECOREEXTRAS_PROPERTYQVECTOR2DHANDLER_H

#include <PECore/abstractpropertyhandler.h>

#include <PECoreExtras/pecoreextras_global.h>

namespace PE {
namespace Core {

class Property;

namespace Extras {

class PECOREEXTRAS_EXPORT PropertyVector2DHandler :
        public PropertyHandler<QVector2D>
{
public:
    PropertyVector2DHandler();

    QString valueToString(const Property *prop) const override;
    QVariantMap defaultHints() const override;
};

} // namespace Extras
} // namespace Core
} // namespace PE

#endif // PECOREEXTRAS_PROPERTYQVECTOR2DHANDLER_H

