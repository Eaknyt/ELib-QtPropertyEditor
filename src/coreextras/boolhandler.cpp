#include "boolhandler.h"

#include <PECore/property.h>

namespace PE {

namespace Core {
namespace Extras {

PropertyBoolHandler::PropertyBoolHandler() :
    PropertyHandler()
{}

QString PropertyBoolHandler::valueToString(const Property *prop) const
{
    const bool rawValue = prop->value().toBool();

    static QString trueString = "True";
    static QString falseString = "False";

    QString ret = (rawValue) ? trueString : falseString;

    return ret;
}

} // namespace Extras
} // namespace Core
} // namespace PE

