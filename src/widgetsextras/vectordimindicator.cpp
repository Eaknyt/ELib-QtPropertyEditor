#include "vectordimindicator.h"

#include <QtGui/QPaintEvent>
#include <QtGui/QPainter>

namespace PE {
namespace Widgets {
namespace Extras {


////////////////////// VectorDimIndicatorPrivate //////////////////////

class VectorDimIndicatorPrivate
{
public:
    VectorDimIndicatorPrivate(VectorDimIndicator *qq);

    VectorDimIndicator *q;

    QString text;
    QColor color;
};

VectorDimIndicatorPrivate::VectorDimIndicatorPrivate(VectorDimIndicator *qq) :
    q(qq),
    text(),
    color()
{}


////////////////////// VectorDimIndicator //////////////////////

VectorDimIndicator::VectorDimIndicator(QWidget *parent) :
    QWidget(parent),
    d(new VectorDimIndicatorPrivate(this))
{}

VectorDimIndicator::VectorDimIndicator(const QString &text,
                                       const QColor &color,
                                       QWidget *parent) :
    QWidget(parent),
    d(new VectorDimIndicatorPrivate(this))
{
    d->text = text;
    d->color = color;
}

VectorDimIndicator::~VectorDimIndicator()
{}

QString VectorDimIndicator::text() const
{
    return d->text;
}

void VectorDimIndicator::setText(const QString &text)
{
    if (d->text != text) {
        d->text = text;

        Q_EMIT textChanged(text);
    }
}

QColor VectorDimIndicator::color() const
{
    return d->color;
}

void VectorDimIndicator::setColor(const QColor &color)
{
    if (d->color != color) {
        d->color = color;

        Q_EMIT colorChanged(color);
    }
}

QSize VectorDimIndicator::sizeHint() const
{
    const QFontMetrics fm(fontMetrics());

    const int w = fm.width(d->text) + 6;
    const int h = fm.height();

    QSize ret(w, h);

    return ret;
}

QSize VectorDimIndicator::minimumSizeHint() const
{
    return sizeHint();
}

void VectorDimIndicator::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    const QRect eventRect = event->rect();

    const int rectRadius = 2;

    p.save();

    p.setPen(d->color);
    p.setBrush(d->color);
    p.drawRoundedRect(eventRect.adjusted(0, 0, -1, -1),
                      rectRadius, rectRadius);

    p.restore();

    p.setPen(Qt::white);
    p.drawText(eventRect, Qt::AlignHCenter | Qt::AlignVCenter, d->text);
}

} // namespace Extras
} // namespace Widgets
} // namespace PE
