#ifndef PEWIDGETS_WIDGETEDITORFACTORIES_H
#define PEWIDGETS_WIDGETEDITORFACTORIES_H

#include <PECoreExtras/enumtype.h>

#include <PEWidgets/abstractwidgeteditorfactory.h>

class QComboBox;

namespace PE {

namespace Core {
class Property;
} // namespace Core

namespace Widgets {
namespace Extras {

class CheckBoxEditorFactory : public WidgetEditorFactory<bool>
{
    Q_OBJECT

public:
    explicit CheckBoxEditorFactory(QObject *parent = nullptr);

protected:
    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void updateEditor(QWidget *editor, const QVariant &value) const override;
};

class SpinBoxEditorFactory : public WidgetEditorFactory<int>
{
    Q_OBJECT

public:
    explicit SpinBoxEditorFactory(QObject *parent = nullptr);

protected:
    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void updateEditor(QWidget *editor, const QVariant &value) const override;
    void updateEditorHint(QWidget *editor, const QString &name,
                          const QVariant &value) const override;
};

class DoubleSpinBoxEditorFactory : public WidgetEditorFactory<double>
{
    Q_OBJECT

public:
    explicit DoubleSpinBoxEditorFactory(QObject *parent = nullptr);

protected:
    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void updateEditor(QWidget *editor, const QVariant &value) const override;
    void updateEditorHint(QWidget *editor, const QString &name,
                          const QVariant &value) const override;
};

class LineEditEditorFactory : public WidgetEditorFactory<QString>
{
    Q_OBJECT

public:
    explicit LineEditEditorFactory(QObject *parent = nullptr);

protected:
    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void updateEditor(QWidget *editor, const QVariant &value) const override;
    void updateEditorHint(QWidget *editor, const QString &name,
                          const QVariant &value) const override;
};

class ComboBoxEditorFactory : public WidgetEditorFactory<Core::Extras::EnumPropertyType>
{
    Q_OBJECT

public:
    explicit ComboBoxEditorFactory(QObject *parent = nullptr);

protected:
    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void updateEditor(QWidget *editor, const QVariant &value) const override;
    void updateEditorHint(QWidget *editor, const QString &name,
                          const QVariant &value) const override;
private Q_SLOTS:
    void updateEnumProperty(int index);

private:
    void setItems(QComboBox *editor, const QStringList &possibleValues) const;
};

class UrlEditorFactory : public WidgetEditorFactory<QUrl>
{
    Q_OBJECT

public:
    explicit UrlEditorFactory(QObject *parent = nullptr);

protected:
    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void updateEditor(QWidget *editor, const QVariant &value) const override;
    void updateEditorHint(QWidget *editor, const QString &name,
                          const QVariant &value) const override;
};

class Vector2DEditorFactory : public WidgetEditorFactory<QVector2D>
{
    Q_OBJECT

public:
    explicit Vector2DEditorFactory(QObject *parent = nullptr);

protected:
    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void updateEditor(QWidget *editor, const QVariant &value) const override;
    void updateEditorHint(QWidget *editor, const QString &name,
                          const QVariant &value) const override;
};

class Vector3DEditorFactory : public WidgetEditorFactory<QVector3D>
{
    Q_OBJECT

public:
    explicit Vector3DEditorFactory(QObject *parent = nullptr);

protected:
    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void updateEditor(QWidget *editor, const QVariant &value) const override;
    void updateEditorHint(QWidget *editor, const QString &name,
                          const QVariant &value) const override;
};

} // namespace Extras
} // namespace Widgets
} // namespace PE

#endif // PEWIDGETS_WIDGETEDITORFACTORIES_H
