#ifndef PEWIDGETSEXTRAS_EDITORFACTORYMANAGERS_H
#define PEWIDGETSEXTRAS_EDITORFACTORYMANAGERS_H

namespace PE {
namespace Widgets {

class EditorFactoryManager;

namespace Extras {

EditorFactoryManager *qtEditorFactoryManager();

} // namespace Extras
} // namespace Widgets
} // namespace PE

#endif // PEWIDGETSEXTRAS_EDITORFACTORYMANAGERS_H
