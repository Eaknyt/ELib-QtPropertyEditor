import qbs

import Reusables

Reusables.PublicLibrary {
    name: "PEWidgetsExtras"
    targetName: "pewidgetsextras"

    cpp.defines: "PEWIDGETSEXTRAS_LIB"
    cpp.cxxLanguageVersion: project.cppLanguageVersion
    cpp.includePaths: [ project.includePath ]

    Depends {
        name: "Qt"
        submodules: [ "core", "gui", "widgets" ]
    }

    files: [
        "editorfactorymanagers.h",
        "editorfactorymanagers.cpp",
        "pewidgetsextras_global.h",
        "urledit.cpp",
        "urledit.h",
        "vector2dedit.cpp",
        "vector2dedit.h",
        "vector3dedit.cpp",
        "vector3dedit.h",
        "vectordimindicator.cpp",
        "vectordimindicator.h",
        "widgeteditorfactories.cpp",
        "widgeteditorfactories.h",
    ]

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: project.libDir
    }
}
