#include "widgeteditorfactories.h"

#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>

#include <PECore/property.h>
#include <PECore/propertymanager.h>

#include <PEWidgetsExtras/urledit.h>
#include <PEWidgetsExtras/urledit.h>
#include <PEWidgetsExtras/vector2dedit.h>
#include <PEWidgetsExtras/vector3dedit.h>

namespace PE {

using namespace Core;

namespace Widgets {
namespace Extras {

using namespace Extras;


////////////////////// BoolEditorFactory //////////////////////

CheckBoxEditorFactory::CheckBoxEditorFactory(QObject *parent) :
    WidgetEditorFactory<bool>(parent)
{}

QWidget *CheckBoxEditorFactory::createEditor(Property *prop,
                                             QWidget *parent) const
{
    Q_UNUSED(prop);

    auto editor = new QCheckBox(parent);

    connect(editor, &QCheckBox::toggled,
            this, &CheckBoxEditorFactory::updateProperty);

    return editor;
}

void CheckBoxEditorFactory::updateEditor(QWidget *editor,
                                         const QVariant &value) const
{
    auto checkBox = qobject_cast<QCheckBox *>(editor);

    checkBox->setChecked(value.toBool());
}


////////////////////// SpinBoxEditorFactory //////////////////////

SpinBoxEditorFactory::SpinBoxEditorFactory(QObject *parent) :
    WidgetEditorFactory<int>(parent)
{}

QWidget *SpinBoxEditorFactory::createEditor(Property *prop,
                                            QWidget *parent) const
{
    Q_UNUSED(prop);

    auto editor = new QSpinBox(parent);

    connect(editor, QOverload<int>::of(&QSpinBox::valueChanged),
            this, &SpinBoxEditorFactory::updateProperty);

    return editor;
}

void SpinBoxEditorFactory::updateEditor(QWidget *editor,
                                        const QVariant &value) const
{
    auto spBox = qobject_cast<QSpinBox *>(editor);

    spBox->setValue(value.toInt());
}

void SpinBoxEditorFactory::updateEditorHint(QWidget *editor,
                                            const QString &name,
                                            const QVariant &value) const
{
    auto spBox = qobject_cast<QSpinBox *>(editor);

    if (name == "minimum") {
        spBox->setMinimum(value.toInt());
    }
    else if (name == "maximum") {
        spBox->setMaximum(value.toInt());
    }
    else if (name == "stepSize") {
        spBox->setSingleStep(value.toInt());
    }
    else if (name == "suffix") {
        spBox->setSuffix(value.toString());
    }

    //TODO support setOnFly
}


////////////////////// DoubleSpinBoxEditorFactory //////////////////////

DoubleSpinBoxEditorFactory::DoubleSpinBoxEditorFactory(QObject *parent) :
    WidgetEditorFactory<double>(parent)
{}

QWidget *DoubleSpinBoxEditorFactory::createEditor(Property *prop,
                                                  QWidget *parent) const
{
    Q_UNUSED(prop);

    auto editor = new QDoubleSpinBox(parent);

    connect(editor, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this, &DoubleSpinBoxEditorFactory::updateProperty);

    return editor;
}

void DoubleSpinBoxEditorFactory::updateEditor(QWidget *editor,
                                              const QVariant &value) const
{
    auto dspBox = qobject_cast<QDoubleSpinBox *>(editor);

    dspBox->setValue(value.toDouble());
}

void DoubleSpinBoxEditorFactory::updateEditorHint(QWidget *editor,
                                                  const QString &name,
                                                  const QVariant &value) const
{
    auto dspBox = qobject_cast<QDoubleSpinBox *>(editor);

    if (name == "minimum") {
        dspBox->setMinimum(value.toDouble());
    }
    else if (name == "maximum") {
        dspBox->setMaximum(value.toDouble());
    }
    else if (name == "stepSize") {
        dspBox->setSingleStep(value.toDouble());
    }
    else if (name == "decimals") {
        dspBox->setDecimals(value.toInt());
    }
    else if (name == "suffix") {
        dspBox->setSuffix(value.toString());
    }

    //TODO support setOnFly
}


////////////////////// StringEditorFactory //////////////////////

LineEditEditorFactory::LineEditEditorFactory(QObject *parent) :
    WidgetEditorFactory<QString>(parent)
{}

QWidget *LineEditEditorFactory::createEditor(Property *prop,
                                             QWidget *parent) const
{
    Q_UNUSED(prop);

    auto editor = new QLineEdit(parent);

    connect(editor, &QLineEdit::textChanged,
            this, &LineEditEditorFactory::updateProperty);

    return editor;
}

void LineEditEditorFactory::updateEditor(QWidget *editor,
                                         const QVariant &value) const
{
    auto lineEdit = qobject_cast<QLineEdit *>(editor);

    lineEdit->setText(value.toString());
}

void LineEditEditorFactory::updateEditorHint(QWidget *editor,
                                             const QString &name,
                                             const QVariant &value) const
{
    auto lineEdit = qobject_cast<QLineEdit *>(editor);

    if (name == "regex") {
        lineEdit->setValidator(new QRegExpValidator(value.toRegExp(),
                                                    lineEdit));
    }
    else if (name == "echoMode") {
        lineEdit->setEchoMode(QLineEdit::EchoMode(value.toInt()));
    }
    else if (name == "readOnly") {
        lineEdit->setReadOnly(value.toBool());
    }
    else if (name == "placeholderText") {
        lineEdit->setPlaceholderText(value.toString());
    }

    //TODO support setOnFly
}


////////////////////// ComboBoxEditorFactory //////////////////////

ComboBoxEditorFactory::ComboBoxEditorFactory(QObject *parent) :
    WidgetEditorFactory<Core::Extras::EnumPropertyType>(parent)
{}

QWidget *ComboBoxEditorFactory::createEditor(Property *prop,
                                             QWidget *parent) const
{
    Q_UNUSED(prop);

    auto editor = new QComboBox(parent);

    // Necessary hack because the combobox value depends of the count of
    //  its items
    const QString possibleValuesStr = "possibleValues";
    updateEditorHint(editor, possibleValuesStr,
                     prop->hints()[possibleValuesStr]);

    connect(editor, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &ComboBoxEditorFactory::updateEnumProperty);

    return editor;
}

void ComboBoxEditorFactory::updateEditor(QWidget *editor,
                                         const QVariant &value) const
{
    auto rawValue = value.value<Core::Extras::EnumPropertyType>();

    auto cbBox = qobject_cast<QComboBox *>(editor);

    cbBox->setCurrentIndex(rawValue.currentIndex);
}

void ComboBoxEditorFactory::updateEditorHint(QWidget *editor,
                                             const QString &name,
                                             const QVariant &value) const
{
    auto cbBox = qobject_cast<QComboBox *>(editor);

    if (name == "possibleValues") {
        setItems(cbBox, value.toStringList());
    }
}

void ComboBoxEditorFactory::updateEnumProperty(int index)
{
    Core::Extras::EnumPropertyType e(index);;

    updateProperty(QVariant::fromValue(e));
}

void ComboBoxEditorFactory::setItems(QComboBox *editor,
                                     const QStringList &possibleValues) const
{
    QStringList currentValues;

    for (int i = 0; i < editor->count(); ++i) {
        currentValues += editor->itemText(i);
    }

    if (currentValues != possibleValues) {
        editor->clear();
        editor->addItems(possibleValues);
    }
}


////////////////////// UrlEditorFactory //////////////////////

UrlEditorFactory::UrlEditorFactory(QObject *parent) :
    WidgetEditorFactory<QUrl>(parent)
{}

QWidget *UrlEditorFactory::createEditor(Property *prop,
                                        QWidget *parent) const
{
    Q_UNUSED(prop);

    auto editor = new UrlEdit(parent);

    connect(editor, &UrlEdit::urlChanged,
            this, &UrlEditorFactory::updateProperty);

    return editor;

}

void UrlEditorFactory::updateEditor(QWidget *editor,
                                    const QVariant &value) const
{
    auto urlEdit = qobject_cast<UrlEdit *>(editor);

    urlEdit->setUrl(value.toUrl());
}

void UrlEditorFactory::updateEditorHint(QWidget *editor,
                                        const QString &name,
                                        const QVariant &value) const
{
    auto urlEdit = qobject_cast<UrlEdit *>(editor);

    if (name == "title") {
        urlEdit->setTitle(value.toString());
    }
    else if (name == "folder") {
        urlEdit->setFolder(value.toUrl());
    }
    else if (name == "nameFilters") {
        urlEdit->setNameFilters(value.toStringList());
    }
    else if (name == "selectFolder") {
        urlEdit->setSelectFolder(value.toBool());
    }
}


////////////////////// Vector2DEditorFactory //////////////////////

Vector2DEditorFactory::Vector2DEditorFactory(QObject *parent) :
    WidgetEditorFactory<QVector2D>(parent)
{}

QWidget *Vector2DEditorFactory::createEditor(Property *prop,
                                             QWidget *parent) const
{
    Q_UNUSED(prop);

    auto editor = new Vector2DEdit(parent);

    connect(editor, &Vector2DEdit::valueChanged,
            this, &Vector2DEditorFactory::updateProperty);

    return editor;
}

void Vector2DEditorFactory::updateEditor(QWidget *editor,
                                         const QVariant &value) const
{
    auto vec2dEdit = qobject_cast<Vector2DEdit *>(editor);

    vec2dEdit->setValue(value.value<QVector2D>());
}

void Vector2DEditorFactory::updateEditorHint(QWidget *editor,
                                             const QString &name,
                                             const QVariant &value) const
{
    auto vec2dEdit = qobject_cast<Vector2DEdit *>(editor);

    if (name == "xMin") {
        vec2dEdit->setXMin(value.toFloat());
    }
    else if (name == "xMax") {
        vec2dEdit->setXMax(value.toFloat());
    }
    else if (name == "yMin") {
        vec2dEdit->setYMin(value.toFloat());
    }
    else if (name == "yMax") {
        vec2dEdit->setYMax(value.toFloat());
    }
    else if (name == "decimals") {
        vec2dEdit->setDecimals(value.toInt());
    }
    else if (name == "stepSize") {
        vec2dEdit->setStepSize(value.toDouble());
    }
}


////////////////////// Vector3DEditorFactory //////////////////////

Vector3DEditorFactory::Vector3DEditorFactory(QObject *parent) :
    WidgetEditorFactory<QVector3D>(parent)
{}

QWidget *Vector3DEditorFactory::createEditor(Property *prop,
                                             QWidget *parent) const
{
    Q_UNUSED(prop);

    auto editor = new Vector3DEdit(parent);

    connect(editor, &Vector3DEdit::valueChanged,
            this, &Vector3DEditorFactory::updateProperty);

    return editor;
}

void Vector3DEditorFactory::updateEditor(QWidget *editor,
                                         const QVariant &value) const
{
    auto vec3dEdit = qobject_cast<Vector3DEdit *>(editor);

    vec3dEdit->setValue(value.value<QVector3D>());
}

void Vector3DEditorFactory::updateEditorHint(QWidget *editor,
                                             const QString &name,
                                             const QVariant &value) const
{
    auto vec3dEdit = qobject_cast<Vector3DEdit *>(editor);

    if (name == "xMin") {
        vec3dEdit->setXMin(value.toFloat());
    }
    else if (name == "xMax") {
        vec3dEdit->setXMax(value.toFloat());
    }
    else if (name == "yMin") {
        vec3dEdit->setYMin(value.toFloat());
    }
    else if (name == "yMax") {
        vec3dEdit->setYMax(value.toFloat());
    }
    else if (name == "zMin") {
        vec3dEdit->setZMin(value.toFloat());
    }
    else if (name == "zMax") {
        vec3dEdit->setZMax(value.toFloat());
    }
    else if (name == "decimals") {
        vec3dEdit->setDecimals(value.toInt());
    }
    else if (name == "stepSize") {
        vec3dEdit->setStepSize(value.toDouble());
    }
}

} // namespace Extras
} // namespace Widgets
} // namespace PE
