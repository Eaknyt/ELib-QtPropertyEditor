#include "vector3dedit.h"

#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>

#include <PEWidgetsExtras/vectordimindicator.h>

namespace PE {
namespace Widgets {
namespace Extras {


////////////////////// Vector3DEditPrivate //////////////////////

class Vector3DEditPrivate
{
public:
    Vector3DEditPrivate(Vector3DEdit *qq);

    Vector3DEdit *q;

    QDoubleSpinBox *xSpinBox;
    QDoubleSpinBox *ySpinBox;
    QDoubleSpinBox *zSpinBox;
};

Vector3DEditPrivate::Vector3DEditPrivate(Vector3DEdit *qq) :
    q(qq),
    xSpinBox(new QDoubleSpinBox(qq)),
    ySpinBox(new QDoubleSpinBox(qq)),
    zSpinBox(new QDoubleSpinBox(qq))
{}


////////////////////// Vector3DEdit //////////////////////

Vector3DEdit::Vector3DEdit(QWidget *parent) :
    QWidget(parent),
    d(new Vector3DEditPrivate(this))
{
    setFocusPolicy(Qt::StrongFocus);
    setFocusProxy(d->xSpinBox);
    setTabOrder(d->xSpinBox, d->ySpinBox);
    setTabOrder(d->ySpinBox, d->zSpinBox);

    auto layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    auto xIndicator = new VectorDimIndicator("X", Qt::red, this);
    auto yIndicator = new VectorDimIndicator("Y",
                                             QColor::fromRgbF(0.24, 0.73, 0.13),
                                             this);
    auto zIndicator = new VectorDimIndicator("Z",
                                             QColor::fromRgbF(0.26, 0.54, 1.0),
                                             this);

    static const int SPACE_BETWEEN_SPINBOXES = 7;

    layout->addWidget(xIndicator, 0, Qt::AlignLeft);
    layout->addWidget(d->xSpinBox, 1);
    layout->addSpacing(SPACE_BETWEEN_SPINBOXES);

    layout->addWidget(yIndicator, 0, Qt::AlignLeft);
    layout->addWidget(d->ySpinBox, 1);
    layout->addSpacing(SPACE_BETWEEN_SPINBOXES);

    layout->addWidget(zIndicator, 0, Qt::AlignLeft);
    layout->addWidget(d->zSpinBox, 1);

    connect(d->xSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this, [=] (double val) {
        Q_EMIT valueChanged(QVector3D(static_cast<float>(val),
                                      d->ySpinBox->value(),
                                      d->zSpinBox->value()));
    });

    connect(d->ySpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this, [=] (double val) {
        Q_EMIT valueChanged(QVector3D(d->xSpinBox->value(),
                                      static_cast<float>(val),
                                      d->zSpinBox->value()));
    });

    connect(d->zSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this, [=] (double val) {
        Q_EMIT valueChanged(QVector3D(d->xSpinBox->value(),
                                      d->ySpinBox->value(),
                                      static_cast<float>(val)));
    });
}

Vector3DEdit::~Vector3DEdit()
{}

QVector3D Vector3DEdit::value() const
{
    return QVector3D(d->xSpinBox->value(),
                     d->ySpinBox->value(),
                     d->zSpinBox->value());
}

void Vector3DEdit::setValue(const QVector3D &val)
{
    if (value() != val) {
        d->xSpinBox->setValue(val.x());
        d->ySpinBox->setValue(val.y());
        d->zSpinBox->setValue(val.z());

        Q_EMIT valueChanged(val);
    }
}

int Vector3DEdit::decimals() const
{
    return d->xSpinBox->decimals();
}

void Vector3DEdit::setDecimals(int prec)
{
    if (decimals() != prec) {
        d->xSpinBox->setDecimals(prec);
        d->ySpinBox->setDecimals(prec);
        d->zSpinBox->setDecimals(prec);

        Q_EMIT decimalsChanged(prec);
    }
}

float Vector3DEdit::xMin() const
{
    return d->xSpinBox->minimum();
}

void Vector3DEdit::setXMin(float min)
{
    if (xMin() != min) {
        d->xSpinBox->setMinimum(min);

        Q_EMIT xMinChanged(min);
    }
}

float Vector3DEdit::xMax() const
{
    return d->xSpinBox->maximum();
}

void Vector3DEdit::setXMax(float max)
{
    if (xMax() != max) {
        d->xSpinBox->setMaximum(max);

        Q_EMIT xMaxChanged(max);
    }
}

float Vector3DEdit::yMin() const
{
    return d->ySpinBox->minimum();
}

void Vector3DEdit::setYMin(float min)
{
    if (yMin() != min) {
        d->ySpinBox->setMinimum(min);

        Q_EMIT yMinChanged(min);
    }
}

float Vector3DEdit::yMax() const
{
    return d->ySpinBox->maximum();
}

void Vector3DEdit::setYMax(float max)
{
    if (yMax() != max) {
        d->ySpinBox->setMaximum(max);

        Q_EMIT yMaxChanged(max);
    }
}

float Vector3DEdit::zMin() const
{
    return d->zSpinBox->minimum();
}

void Vector3DEdit::setZMin(float min)
{
    if (zMin() != min) {
        d->zSpinBox->setMinimum(min);

        Q_EMIT zMinChanged(min);
    }
}

float Vector3DEdit::zMax() const
{
    return d->zSpinBox->maximum();
}

void Vector3DEdit::setZMax(float max)
{
    if (zMax() != max) {
        d->zSpinBox->setMaximum(max);

        Q_EMIT zMaxChanged(max);
    }
}

float Vector3DEdit::stepSize() const
{
    return d->xSpinBox->singleStep();
}

void Vector3DEdit::setStepSize(float step)
{
    if (d->xSpinBox->singleStep() != step) {
        d->xSpinBox->setSingleStep(step);
        d->ySpinBox->setSingleStep(step);
        d->zSpinBox->setSingleStep(step);

        Q_EMIT stepSizeChanged(step);
    }
}

} // namespace Extras
} // namespace Widgets
} // namespace PE
