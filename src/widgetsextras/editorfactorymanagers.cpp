#include "editorfactorymanagers.h"

#include <PEWidgets/editorfactorymanager.h>

#include <PEWidgetsExtras/widgeteditorfactories.h>

namespace PE {
namespace Widgets {
namespace Extras {

EditorFactoryManager *qtEditorFactoryManager()
{
    static EditorFactoryManager ret;

    static const std::vector<AbstractWidgetEditorFactory *> factories {
        new Widgets::Extras::CheckBoxEditorFactory(&ret),
        new Widgets::Extras::SpinBoxEditorFactory(&ret),
        new Widgets::Extras::DoubleSpinBoxEditorFactory(&ret),
        new Widgets::Extras::LineEditEditorFactory(&ret),
        new Widgets::Extras::ComboBoxEditorFactory(&ret),
        new Widgets::Extras::Vector2DEditorFactory(&ret),
        new Widgets::Extras::Vector3DEditorFactory(&ret),
        new Widgets::Extras::UrlEditorFactory(&ret)
    };

    if (!ret.editorFactories().size()) {
        for (AbstractWidgetEditorFactory *factory : factories) {
            ret.registerEditorFactory(factory);
        }
    }

    return &ret;
}

} // namespace Extras
} // namespace Widgets
} // namespace PE
