#ifndef PEWIDGETSEXTRAS_VECTOR2DEDIT_H
#define PEWIDGETSEXTRAS_VECTOR2DEDIT_H

#include <QtGui/QVector2D>

#include <QtWidgets/QWidget>

#include <PEWidgetsExtras/pewidgetsextras_global.h>

namespace PE {
namespace Widgets {
namespace Extras {

class Vector2DEditPrivate;

class PEWIDGETSEXTRAS_EXPORT Vector2DEdit : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QVector2D value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(int decimals READ decimals WRITE setDecimals NOTIFY decimalsChanged)
    Q_PROPERTY(float xMin READ xMin WRITE setXMin NOTIFY xMinChanged)
    Q_PROPERTY(float xMax READ xMax WRITE setXMax NOTIFY xMaxChanged)
    Q_PROPERTY(float yMin READ yMin WRITE setYMin NOTIFY yMinChanged)
    Q_PROPERTY(float yMax READ yMax WRITE setYMax NOTIFY yMaxChanged)
    Q_PROPERTY(float stepSize READ stepSize WRITE setStepSize NOTIFY stepSizeChanged)

public:
    explicit Vector2DEdit(QWidget *parent = nullptr);
    ~Vector2DEdit();

    QVector2D value() const;

    int decimals() const;

    float xMin() const;
    float xMax() const;

    float yMin() const;
    float yMax() const;

    float stepSize() const;

Q_SIGNALS:
    void valueChanged(const QVector2D &);

    void decimalsChanged(int);

    void xMinChanged(float);
    void xMaxChanged(float);

    void yMinChanged(float);
    void yMaxChanged(float);

    void stepSizeChanged(float);

public Q_SLOTS:
    void setValue(const QVector2D &val);

    void setDecimals(int prec);

    void setXMin(float min);
    void setXMax(float max);

    void setYMin(float min);
    void setYMax(float max);

    void setStepSize(float step);

private:
    const QScopedPointer<Vector2DEditPrivate> d;
};

} // namespace Extras
} // namespace Widgets
} // namespace PE

#endif // PEWIDGETSEXTRAS_VECTOR2DEDIT_H
