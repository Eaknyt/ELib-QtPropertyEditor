#include "vector2dedit.h"

#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>

#include <PEWidgetsExtras/vectordimindicator.h>

namespace PE {
namespace Widgets {
namespace Extras {


////////////////////// Vector2DEditPrivate //////////////////////

class Vector2DEditPrivate
{
public:
    Vector2DEditPrivate(Vector2DEdit *qq);

    Vector2DEdit *q;

    QDoubleSpinBox *xSpinBox;
    QDoubleSpinBox *ySpinBox;
};

Vector2DEditPrivate::Vector2DEditPrivate(Vector2DEdit *qq) :
    q(qq),
    xSpinBox(new QDoubleSpinBox(qq)),
    ySpinBox(new QDoubleSpinBox(qq))
{}


////////////////////// Vector2DEdit //////////////////////

Vector2DEdit::Vector2DEdit(QWidget *parent) :
    QWidget(parent),
    d(new Vector2DEditPrivate(this))
{
    setFocusPolicy(Qt::StrongFocus);
    setFocusProxy(d->xSpinBox);
    setTabOrder(d->xSpinBox, d->ySpinBox);

    auto layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    auto xIndicator = new VectorDimIndicator("X", Qt::red, this);
    auto yIndicator = new VectorDimIndicator("Y",
                                             QColor::fromRgbF(0.24, 0.73, 0.13),
                                             this);

    static const int SPACE_BETWEEN_SPINBOXES = 7;

    layout->addWidget(xIndicator, 0, Qt::AlignLeft);
    layout->addWidget(d->xSpinBox, 1);
    layout->addSpacing(SPACE_BETWEEN_SPINBOXES);

    layout->addWidget(yIndicator, 0, Qt::AlignLeft);
    layout->addWidget(d->ySpinBox, 1);

    connect(d->xSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this, [=] (double val) {
        Q_EMIT valueChanged(QVector2D(static_cast<float>(val),
                                      d->ySpinBox->value()));
    });

    connect(d->ySpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this, [=] (double val) {
        Q_EMIT valueChanged(QVector2D(d->xSpinBox->value(),
                                      static_cast<float>(val)));
    });
}

Vector2DEdit::~Vector2DEdit()
{}

QVector2D Vector2DEdit::value() const
{
    return QVector2D(d->xSpinBox->value(),
                     d->ySpinBox->value());
}

void Vector2DEdit::setValue(const QVector2D &val)
{
    if (value() != val) {
        d->xSpinBox->setValue(val.x());
        d->ySpinBox->setValue(val.y());

        Q_EMIT valueChanged(val);
    }
}

int Vector2DEdit::decimals() const
{
    return d->xSpinBox->decimals();
}

void Vector2DEdit::setDecimals(int prec)
{
    if (decimals() != prec) {
        d->xSpinBox->setDecimals(prec);
        d->ySpinBox->setDecimals(prec);

        Q_EMIT decimalsChanged(prec);
    }
}

float Vector2DEdit::xMin() const
{
    return d->xSpinBox->minimum();
}

void Vector2DEdit::setXMin(float min)
{
    if (xMin() != min) {
        d->xSpinBox->setMinimum(min);

        Q_EMIT xMinChanged(min);
    }
}

float Vector2DEdit::xMax() const
{
    return d->xSpinBox->maximum();
}

void Vector2DEdit::setXMax(float max)
{
    if (xMax() != max) {
        d->xSpinBox->setMaximum(max);

        Q_EMIT xMaxChanged(max);
    }
}

float Vector2DEdit::yMin() const
{
    return d->ySpinBox->minimum();
}

void Vector2DEdit::setYMin(float min)
{
    if (yMin() != min) {
        d->ySpinBox->setMinimum(min);

        Q_EMIT yMinChanged(min);
    }
}

float Vector2DEdit::yMax() const
{
    return d->ySpinBox->maximum();
}

void Vector2DEdit::setYMax(float max)
{
    if (yMax() != max) {
        d->ySpinBox->setMaximum(max);

        Q_EMIT yMaxChanged(max);
    }
}

float Vector2DEdit::stepSize() const
{
    return d->xSpinBox->singleStep();
}

void Vector2DEdit::setStepSize(float step)
{
    if (d->xSpinBox->singleStep() != step) {
        d->xSpinBox->setSingleStep(step);
        d->ySpinBox->setSingleStep(step);

        Q_EMIT stepSizeChanged(step);
    }
}

} // namespace Extras
} // namespace Widgets
} // namespace PE
