#ifndef PEWIDGETSEXTRAS_VECTOR3DEDIT_H
#define PEWIDGETSEXTRAS_VECTOR3DEDIT_H

#include <QtGui/QVector3D>

#include <QtWidgets/QWidget>

#include <PEWidgetsExtras/pewidgetsextras_global.h>

namespace PE {
namespace Widgets {
namespace Extras {

class Vector3DEditPrivate;

class PEWIDGETSEXTRAS_EXPORT Vector3DEdit : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QVector3D value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(int decimals READ decimals WRITE setDecimals NOTIFY decimalsChanged)
    Q_PROPERTY(float xMin READ xMin WRITE setXMin NOTIFY xMinChanged)
    Q_PROPERTY(float xMax READ xMax WRITE setXMax NOTIFY xMaxChanged)
    Q_PROPERTY(float yMin READ yMin WRITE setYMin NOTIFY yMinChanged)
    Q_PROPERTY(float yMax READ yMax WRITE setYMax NOTIFY yMaxChanged)
    Q_PROPERTY(float zMin READ zMin WRITE setZMin NOTIFY zMinChanged)
    Q_PROPERTY(float zMax READ zMax WRITE setZMax NOTIFY zMaxChanged)
    Q_PROPERTY(float stepSize READ stepSize WRITE setStepSize NOTIFY stepSizeChanged)

public:
    explicit Vector3DEdit(QWidget *parent = nullptr);
    ~Vector3DEdit();

    QVector3D value() const;

    int decimals() const;

    float xMin() const;
    float xMax() const;

    float yMin() const;
    float yMax() const;

    float zMin() const;
    float zMax() const;

    float stepSize() const;

Q_SIGNALS:
    void valueChanged(const QVector3D &);

    void decimalsChanged(int);

    void xMinChanged(float);
    void xMaxChanged(float);

    void yMinChanged(float);
    void yMaxChanged(float);

    void zMinChanged(float);
    void zMaxChanged(float);

    void stepSizeChanged(float);

public Q_SLOTS:
    void setValue(const QVector3D &val);

    void setDecimals(int prec);

    void setXMin(float min);
    void setXMax(float max);

    void setYMin(float min);
    void setYMax(float max);

    void setZMin(float min);
    void setZMax(float max);

    void setStepSize(float step);

private:
    const QScopedPointer<Vector3DEditPrivate> d;
};

} // namespace Extras
} // namespace Widgets
} // namespace PE

#endif // PEWIDGETSEXTRAS_VECTOR3DEDIT_H
