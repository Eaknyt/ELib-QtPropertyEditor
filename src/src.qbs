import qbs
import qbs.Environment

Project {
    name: "Sources"

    references: [
        "core/core.qbs",
        "coreextras/coreextras.qbs",
        "quick/quick.qbs",
        "quickextras/quickextras.qbs",
        "widgets/widgets.qbs",
        "widgetsextras/widgetsextras.qbs"
    ]
}
