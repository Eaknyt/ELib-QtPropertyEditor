#ifndef PEWIDGETS_EDITORFACTORYMANAGER
#define PEWIDGETS_EDITORFACTORYMANAGER

#include <QtCore/QObject>

#include <PEWidgets/pewidgets_global.h>

namespace PE {
namespace Widgets {

class AbstractWidgetEditorFactory;
class EditorFactoryManagerPrivate;

class PEWIDGETS_EXPORT EditorFactoryManager : public QObject
{
    Q_OBJECT

public:
    explicit EditorFactoryManager(QObject *parent = nullptr);
    ~EditorFactoryManager();

    AbstractWidgetEditorFactory *editorFactory(int type) const;
    void registerEditorFactory(AbstractWidgetEditorFactory *factory);

    std::vector<AbstractWidgetEditorFactory *> editorFactories() const;

private:
    const QScopedPointer<EditorFactoryManagerPrivate> d;
};

} // namespace Widgets
} // namespace PE

#endif // PEWIDGETS_EDITORFACTORYMANAGER
