#ifndef PEWIDGETS_ABSTRACTPROPERTYBROWSER_P_H
#define PEWIDGETS_ABSTRACTPROPERTYBROWSER_P_H

#include <QtCore/QMap>

class QWidget;

namespace PE {

namespace Core {
class Property;
class PropertyManager;
} // namespace Core

namespace Widgets {

class AbstractPropertyBrowserPrivate
{
public:
    Q_DECLARE_PUBLIC(AbstractPropertyBrowser)

    AbstractPropertyBrowserPrivate(AbstractPropertyBrowser *qq);

    AbstractPropertyBrowser *q_ptr;
    Core::PropertyManager *manager;
    EditorFactoryManager *editorFactoryManager;
};

} // namespace Widgets
} // namespace PE

#endif // PEWIDGETS_ABSTRACTPROPERTYBROWSER_P_H
