#ifndef PEWIDGETS_MVPROPERTYBROWSERITEMDELEGATE_P_H
#define PEWIDGETS_MVPROPERTYBROWSERITEMDELEGATE_P_H

#include <QtWidgets/QStyledItemDelegate>

#include <PEWidgets/abstractpropertybrowser.h>
#include <PEWidgets/pewidgets_global.h>

namespace PE {

namespace Widgets {

class AbstractPropertyBrowser;

class MVPropertyBrowserItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    MVPropertyBrowserItemDelegate(AbstractPropertyBrowser *parent);

    // QAbstractItemDelegate interface
    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const override;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
    void destroyEditor(QWidget *editor,
                       const QModelIndex &index) const override;
    void updateEditorGeometry(QWidget *editor,
                              const QStyleOptionViewItem &option,
                              const QModelIndex &index) const override;
    void setEditorData(QWidget *editor,
                       const QModelIndex &index) const override;

private:
    AbstractPropertyBrowser *m_browser;
};

} // namespace Widgets
} // namespace PE

#endif // PEWIDGETS_MVPROPERTYBROWSERITEMDELEGATE_P_H
