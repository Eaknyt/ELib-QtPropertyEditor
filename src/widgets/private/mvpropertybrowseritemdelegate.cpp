#include "mvpropertybrowseritemdelegate_p.h"

#include <PECore/property.h>
#include <PECore/propertyitemmodel.h>

namespace PE {

using namespace Core;

namespace Widgets {

MVPropertyBrowserItemDelegate::MVPropertyBrowserItemDelegate
(AbstractPropertyBrowser *parent) :
    QStyledItemDelegate(parent),
    m_browser(parent)
{}

QSize MVPropertyBrowserItemDelegate::sizeHint(const QStyleOptionViewItem &option,
                                              const QModelIndex &index) const
{
    QSize size = QStyledItemDelegate::sizeHint(option, index);
    size.rheight() += 5;

    return size;
}

QWidget *MVPropertyBrowserItemDelegate::createEditor(
        QWidget *parent,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const
{
    Q_UNUSED(option);

    auto model = qobject_cast<const PropertyItemModel *>(index.model());

    Property *prop = model->propertyFromIndex(index);
    QWidget *editor = m_browser->createEditor(prop, parent);

    // Include a potential editor wrapper widget
    QWidget *editorParent = editor->parentWidget();

    QWidget *ret = (editorParent != parent) ? editorParent : editor;

    if (ret) {
        ret->setAutoFillBackground(true);
    }

    return ret;
}

void MVPropertyBrowserItemDelegate::destroyEditor(QWidget *editor,
                                                  const QModelIndex &index) const
{
    Q_UNUSED(editor);

    auto model = qobject_cast<const PropertyItemModel *>(index.model());

    Property *prop = model->propertyFromIndex(index);

    m_browser->destroyEditor(prop);
}

void MVPropertyBrowserItemDelegate::updateEditorGeometry(
        QWidget *editor,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const
{
    QStyledItemDelegate::updateEditorGeometry(editor, option, index);
}

void MVPropertyBrowserItemDelegate::setEditorData(QWidget *editor,
                                                  const QModelIndex &index) const
{
    Q_UNUSED(editor);
    Q_UNUSED(index);

    // Since the implementation of PropertyItemModel does not really follow the
    //  guidelines for editable models, it is fundamental to do nothing here
    //  and to not call the base method. Otherwise it leads to an infinite
    //  loop.
}

} // namespace Widgets
} // namespace PE
