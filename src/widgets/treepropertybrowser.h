#ifndef PEWIDGETS_TREEPROPERTYBROWSER_H
#define PEWIDGETS_TREEPROPERTYBROWSER_H

#include <PEWidgets/abstractpropertybrowser.h>
#include <PEWidgets/pewidgets_global.h>

namespace PE {

namespace Core {
class PropertyItem;
} // namespace Core

namespace Widgets {

class TreePropertyBrowserPrivate;

class PEWIDGETS_EXPORT TreePropertyBrowser : public AbstractPropertyBrowser
{
    Q_OBJECT

public:
    explicit TreePropertyBrowser(QWidget *parent = nullptr);
    ~TreePropertyBrowser();

    void setHeaderNames(const QString &propertyHeader,
                        const QString &valueHeader);

    Core::PropertyItem *itemFromProperty(Core::Property *prop) const;
    Core::Property *propertyFromItem(Core::PropertyItem *item) const;

    Core::PropertyItem *currentItem() const;
    void setCurrentItem(Core::PropertyItem *item);

Q_SIGNALS:
    void propertySelected(Core::PropertyItem *propItem);

protected:
    // IPropertyBrowser interface
    void addPropertyField(Core::Property *prop) override;
    void removePropertyField(Core::Property *prop) override;
    void updatePropertyField(Core::Property *prop) override;
    void updatePropertyFieldValue(Core::Property *prop,
                                  const QVariant &value) override;
    void updatePropertyFieldHint(Core::Property *prop, const QString &name,
                                 const QVariant &value) override;
    void updatePropertyFieldReadOnly(Core::Property *prop, bool readOnly) override;
    void updatePropertyFieldResettable(Core::Property *prop, bool resettable) override;
    void updatePropertyFieldDefaultValue(Core::Property *prop,
                                         const QVariant &defaultValue) override;
private:
    Q_DECLARE_PRIVATE(TreePropertyBrowser)
};

} // namespace Widgets
} // namespace PE

#endif // PEWIDGETS_TREEPROPERTYBROWSER_H
