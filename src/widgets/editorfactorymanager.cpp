#include "editorfactorymanager.h"

#include <QtCore/QMap>

#include <PECore/pecore_helpers.h>

#include <PEWidgets/abstractwidgeteditorfactory.h>

namespace PE {
namespace Widgets {


////////////////////// EditorFactoryManagerPrivate //////////////////////

class EditorFactoryManagerPrivate
{
public:
    EditorFactoryManagerPrivate();

    /* Methods */
    void unregisterEditorFactory(int type);

    /* Attributes */
    QMap<int, AbstractWidgetEditorFactory *> typeToEditorFactory;
};

EditorFactoryManagerPrivate::EditorFactoryManagerPrivate() :
    typeToEditorFactory()
{}

void EditorFactoryManagerPrivate::unregisterEditorFactory(int type)
{
    auto factoryIt = typeToEditorFactory.find(type);

    if (factoryIt == typeToEditorFactory.end()) {
        return;
    }

    typeToEditorFactory.erase(factoryIt);
}

////////////////////// EditorFactoryManager //////////////////////

EditorFactoryManager::EditorFactoryManager(QObject *parent) :
    QObject(parent),
    d(new EditorFactoryManagerPrivate)
{}

EditorFactoryManager::~EditorFactoryManager()
{}

AbstractWidgetEditorFactory *EditorFactoryManager::editorFactory(int type) const
{
    if (d->typeToEditorFactory.contains(type)) {
        return d->typeToEditorFactory.value(type);
    }

    Core::Debug::qCritical_noEditorFactoryFound(type, "AbstractPropertyBrowser");

    return nullptr;
}

void EditorFactoryManager::registerEditorFactory(AbstractWidgetEditorFactory *factory)
{
    const int type = factory->type();

    if (d->typeToEditorFactory.contains(type)) {
        d->unregisterEditorFactory(type);
    }

    d->typeToEditorFactory[type] = factory;

    if (factory->parent() != this) {
        factory->setParent(this);
    }
}

std::vector<AbstractWidgetEditorFactory *> EditorFactoryManager::editorFactories() const
{
    std::vector<AbstractWidgetEditorFactory *> ret(d->typeToEditorFactory.begin(),
                                                   d->typeToEditorFactory.end());

    return ret;
}

} // namespace Widgets
} // namespace PE
