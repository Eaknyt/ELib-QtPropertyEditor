#ifndef PEWIDGETS_STANDARDPROPERTYBROWSER_H
#define PEWIDGETS_STANDARDPROPERTYBROWSER_H

#include <PEWidgets/abstractpropertybrowser.h>
#include <PEWidgets/pewidgets_global.h>

namespace PE {
namespace Widgets {

class StandardPropertyBrowserPrivate;

class PEWIDGETS_EXPORT StandardPropertyBrowser : public AbstractPropertyBrowser
{
    Q_OBJECT

public:
    explicit StandardPropertyBrowser(QWidget *parent = nullptr);

protected:
    // IPropertyBrowser interface
    void addPropertyField(Core::Property *prop) override;
    void removePropertyField(Core::Property *prop) override;
    void updatePropertyField(Core::Property *prop) override;

private:
    Q_DECLARE_PRIVATE(StandardPropertyBrowser)
};

} // namespace Widgets
} // namespace PE

#endif // PEWIDGETS_STANDARDPROPERTYBROWSER_H
