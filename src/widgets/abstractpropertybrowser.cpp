#include "abstractpropertybrowser.h"

#include <QtCore/QtGlobal>
#include <QtCore/QDebug>

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QToolButton>

#include <PECore/pecore_constants.h>
#include <PECore/property.h>
#include <PECore/propertymanager.h>

#include <PEWidgets/private/abstractpropertybrowser_p.h>

#include <PEWidgets/abstractwidgeteditorfactory.h>
#include <PEWidgets/editorfactorymanager.h>


namespace {

void qCritical_noEditorFactoryManager(const QString &funcName)
{
    qCritical() << "AbstractPropertyBrowser::" + funcName + "()"
                   + "no editor factory manager set";
}

} // anon namespace


namespace PE {

using namespace Core;

namespace Widgets {


////////////////////// AbstractPropertyBrowserPrivate /////////////////////////

AbstractPropertyBrowserPrivate::AbstractPropertyBrowserPrivate
(AbstractPropertyBrowser *qq) :
    q_ptr(qq),
    manager(nullptr),
    editorFactoryManager(nullptr)
{}


////////////////////// AbstractPropertyBrowser /////////////////////////

AbstractPropertyBrowser::AbstractPropertyBrowser(QWidget *parent) :
    IPropertyBrowser<QWidget>(parent),
    d_ptr(new AbstractPropertyBrowserPrivate(this))
{}

AbstractPropertyBrowser::AbstractPropertyBrowser
(AbstractPropertyBrowserPrivate &dd, QWidget *parent) :
    IPropertyBrowser<QWidget>(parent),
    d_ptr(&dd)
{}

AbstractPropertyBrowser::~AbstractPropertyBrowser()
{}

PropertyManager *AbstractPropertyBrowser::manager() const
{
    return d_ptr->manager;
}

void AbstractPropertyBrowser::setManager(PropertyManager *manager)
{
    Q_D(AbstractPropertyBrowser);

    if (d->manager != manager && manager) {
        disconnectManager();

        d->manager = manager;

        connectManager();

        Q_EMIT managerChanged(manager);
    }
}

EditorFactoryManager *AbstractPropertyBrowser::editorFactoryManager() const
{
    Q_D(const AbstractPropertyBrowser);

    return d->editorFactoryManager;
}

void AbstractPropertyBrowser::setEditorFactoryManager(EditorFactoryManager *manager)
{
    Q_D(AbstractPropertyBrowser);

    if (d->editorFactoryManager != manager) {
        d->editorFactoryManager = manager;

        Q_EMIT editorFactoryManagerChanged(manager);
    }
}

QWidget *AbstractPropertyBrowser::createEditor(Property *prop,
                                               QWidget *parent) const
{
    Q_D(const AbstractPropertyBrowser);

    if (!d->editorFactoryManager) {
        qCritical_noEditorFactoryManager("createEditor");
        return nullptr;
    }

    Q_ASSERT(prop);

    AbstractWidgetEditorFactory *factory =
            d->editorFactoryManager->editorFactory(prop->type());

    if (!factory) {
        return nullptr;
    }

    QWidget *editor = factory->addProperty(prop, parent);
    Q_ASSERT(editor);

    editor->setEnabled(!prop->isReadOnly());

    // Create a placeholder for the editor.
    // It will be useful when switching to another "editor mode", like the
    //  resettable one.
    auto ret = new QWidget(parent);

    auto fullEditorLayout = new QHBoxLayout(ret);
    fullEditorLayout->setContentsMargins(0, 0, 0, 0);

    fullEditorLayout->addWidget(editor);

    ret->setFocusProxy(editor);

    // Add the potential reset button
    //TODO Delegate this
    if (prop->isResettable()) {
        buildResettableEditor(prop, editor, ret);
    }

    return ret;
}

void AbstractPropertyBrowser::destroyEditor(Property *prop) const
{
    Q_D(const AbstractPropertyBrowser);

    if (!d->editorFactoryManager) {
        qCritical_noEditorFactoryManager("destroyEditor");
        return;
    }

    AbstractWidgetEditorFactory *factory =
            d->editorFactoryManager->editorFactory(prop->type());

    if (!factory) {
        return;
    }

    factory->removeProperty(prop);
}

void AbstractPropertyBrowser::connectManager()
{
    Q_D(AbstractPropertyBrowser);

    if (d->manager) {
        connect(d->manager, &PropertyManager::propertyAdded,
                this, &AbstractPropertyBrowser::onPropertyAdded);

        connect(d->manager, &PropertyManager::propertyRemoved,
                this, &AbstractPropertyBrowser::onPropertyRemoved);

        connect(d->manager, &PropertyManager::propertyChanged,
                this, &AbstractPropertyBrowser::onPropertyChanged);

        connect(d->manager, &PropertyManager::propertyValueChanged,
                this, &AbstractPropertyBrowser::onPropertyValueChanged);

        connect(d->manager, &PropertyManager::propertyHintChanged,
                this, &AbstractPropertyBrowser::onPropertyHintChanged);

        connect(d->manager, &PropertyManager::propertyReadOnlyChanged,
                this, &AbstractPropertyBrowser::onPropertyReadOnlyChanged);

        connect(d->manager, &PropertyManager::propertyResetChanged,
                this, &AbstractPropertyBrowser::onPropertyResetChanged);

        connect(d->manager, &PropertyManager::propertyDefaultValueChanged,
                this, &AbstractPropertyBrowser::onPropertyDefaultValueChanged);
    }
}

void AbstractPropertyBrowser::disconnectManager()
{
    Q_D(AbstractPropertyBrowser);

    if (d->manager) {
        disconnect(d->manager, &PropertyManager::propertyAdded,
                   this, &AbstractPropertyBrowser::onPropertyAdded);

        disconnect(d->manager, &PropertyManager::propertyRemoved,
                   this, &AbstractPropertyBrowser::onPropertyRemoved);

        disconnect(d->manager, &PropertyManager::propertyChanged,
                   this, &AbstractPropertyBrowser::onPropertyChanged);

        disconnect(d->manager, &PropertyManager::propertyValueChanged,
                   this, &AbstractPropertyBrowser::onPropertyValueChanged);

        disconnect(d->manager, &PropertyManager::propertyHintChanged,
                   this, &AbstractPropertyBrowser::onPropertyHintChanged);

        disconnect(d->manager, &PropertyManager::propertyReadOnlyChanged,
                   this, &AbstractPropertyBrowser::onPropertyReadOnlyChanged);

        disconnect(d->manager, &PropertyManager::propertyResetChanged,
                   this, &AbstractPropertyBrowser::onPropertyResetChanged);

        disconnect(d->manager, &PropertyManager::propertyDefaultValueChanged,
                   this, &AbstractPropertyBrowser::onPropertyDefaultValueChanged);
    }
}

void AbstractPropertyBrowser::buildResettableEditor(Property *prop,
                                                    QWidget *editor,
                                                    QWidget *placeholder) const
{
    Q_UNUSED(editor);

    auto resetPropBtn = new QToolButton(placeholder);
    resetPropBtn->setObjectName(RESET_BTN_OBJECT_NAME);
    resetPropBtn->setText("r");

    connect(resetPropBtn, &QToolButton::pressed,
            this, [=] () {
        prop->reset();
    });

    auto placeholderLayout = qobject_cast<QHBoxLayout *>(placeholder->layout());
    placeholderLayout->addWidget(resetPropBtn);
}

void AbstractPropertyBrowser::unbuildResettableEditor(Property *prop,
                                                      QWidget *editor,
                                                      QWidget *placeholder) const
{
    Q_UNUSED(prop);
    Q_UNUSED(editor);

    auto placeholderLayout = qobject_cast<QHBoxLayout *>(placeholder->layout());

    auto resetBtn = placeholderLayout->itemAt(1)->widget();
    placeholderLayout->removeWidget(resetBtn);

    resetBtn->deleteLater();
}

void AbstractPropertyBrowser::onPropertyAdded(Property *prop)
{
    // Virtual call
    addPropertyField(prop);
}

void AbstractPropertyBrowser::onPropertyRemoved(Property *prop)
{
    // Virtual call
    removePropertyField(prop);
}

void AbstractPropertyBrowser::onPropertyChanged(Property *prop)
{
    // Virtual call
    updatePropertyField(prop);
}

void AbstractPropertyBrowser::onPropertyValueChanged(Property *prop,
                                                   const QVariant &value)
{    Q_D(AbstractPropertyBrowser);

     if (!d->editorFactoryManager) {
         qCritical_noEditorFactoryManager("propertyValueChanged");
         return;
     }

    // Update the editor associated with the property
    AbstractWidgetEditorFactory *factory =
            d->editorFactoryManager->editorFactory(prop->type());

    if (factory) {
        factory->updateEditorValue(prop, value);
    }

    // Virtual call
    updatePropertyFieldValue(prop, value);
}

void AbstractPropertyBrowser::onPropertyHintChanged(Property *prop,
                                                  const QString &name,
                                                  const QVariant &value)
{
    Q_D(AbstractPropertyBrowser);

    if (!d->editorFactoryManager) {
        qCritical_noEditorFactoryManager("propertyHintChanged");
        return;
    }

    // Update the hints of the editor associated with the property
    AbstractWidgetEditorFactory *factory =
            d->editorFactoryManager->editorFactory(prop->type());

    if (factory) {
        factory->setEditorHint(prop, name, value);
    }

    // Virtual call
    updatePropertyFieldHint(prop, name, value);
}

void AbstractPropertyBrowser::onPropertyReadOnlyChanged(Property *prop,
                                                      bool readOnly)
{
    Q_D(AbstractPropertyBrowser);

    if (!d->editorFactoryManager) {
        qCritical_noEditorFactoryManager("propertyReadOnlyChanged");
        return;
    }

    AbstractWidgetEditorFactory *editorFactory =
            d->editorFactoryManager->editorFactory(prop->type());

    if (!editorFactory) {
        return;
    }

    QWidget *editor = editorFactory->editorFromProperty(prop);

    if (editor) {
        QWidget *editorPlaceholder = editor->parentWidget();
        Q_ASSERT(editorPlaceholder);

        editorPlaceholder->setEnabled(!readOnly);
    }

    // Virtual call
    updatePropertyFieldReadOnly(prop, readOnly);
}

void AbstractPropertyBrowser::onPropertyResetChanged(Property *prop,
                                                   bool resettable)
{
    Q_D(AbstractPropertyBrowser);

    if (!d->editorFactoryManager) {
        qCritical_noEditorFactoryManager("propertyResetChanged");
        return;
    }

    const AbstractWidgetEditorFactory *editorFactory =
            d->editorFactoryManager->editorFactory(prop->type());

    if (!editorFactory) {
        return;
    }

    QWidget *editor = editorFactory->editorFromProperty(prop);

    if (!editor) {
        return;
    }

    QWidget *editorPlaceholder = editor->parentWidget();

    const bool editorIsResettable =
            (editorPlaceholder->findChild<QWidget *>(Core::RESET_BTN_OBJECT_NAME));

    if (!editorIsResettable) {
        buildResettableEditor(prop, editor, editorPlaceholder);
    }
    else {
        unbuildResettableEditor(prop, editor, editorPlaceholder);
    }

    // Virtual call
    updatePropertyFieldResettable(prop, resettable);
}

void AbstractPropertyBrowser::onPropertyDefaultValueChanged(Property *prop,
                                                          const QVariant &defaultValue)
{
    // Virtual call
    updatePropertyFieldDefaultValue(prop, defaultValue);
}

} // namespace Widgets
} // namespace PE
