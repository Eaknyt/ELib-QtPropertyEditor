#include "standardpropertybrowser.h"

#include <QtCore/QDebug>

#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QScrollArea>

#include <PECore/pecore_constants.h>
#include <PECore/property.h>

#include <PEWidgets/private/abstractpropertybrowser_p.h>

#include <PEWidgets/editorfactorymanager.h>
#include <PEWidgets/abstractwidgeteditorfactory.h>

namespace PE {
namespace Widgets {


////////////////////// StandardPropertyBrowserPrivate //////////////////////

struct StandardPropertyField
{
    QLabel *label = nullptr;
    QWidget *editor = nullptr;
};


class StandardPropertyBrowserPrivate : public AbstractPropertyBrowserPrivate
{
public:
    StandardPropertyBrowserPrivate(StandardPropertyBrowser *qq);

    /* Methods */
    void removeRowFromLayout(const StandardPropertyField &propField);

    /* Attributes */
    QMap<Core::Property *, StandardPropertyField> propertyToPropertyField;

    QWidget *propertiesWidget;
    QFormLayout *propertiesLayout;
};

StandardPropertyBrowserPrivate::StandardPropertyBrowserPrivate
(StandardPropertyBrowser *qq) :
    AbstractPropertyBrowserPrivate(qq),
    propertyToPropertyField(),
    propertiesWidget(nullptr),
    propertiesLayout(nullptr)
{}

void StandardPropertyBrowserPrivate::removeRowFromLayout(const StandardPropertyField &propField)
{
    int row = -1;

    // Remove the label and its layout item
    propertiesLayout->getWidgetPosition(propField.label, &row, nullptr);
    Q_ASSERT(row != -1);

    QLayoutItem *layoutItem =
            propertiesLayout->itemAt(row, QFormLayout::LabelRole);

    propertiesLayout->removeItem(layoutItem);
    layoutItem->widget()->deleteLater();
    delete layoutItem;

    // Remove the editor layout item but delegate the delete to the super class
    propertiesLayout->getWidgetPosition(propField.editor, &row, nullptr);

    if (row == -1) {
        return;
    }

    layoutItem = propertiesLayout->itemAt(row, QFormLayout::FieldRole);
    propertiesLayout->removeItem(layoutItem);
    delete layoutItem;
}


////////////////////// StandardPropertyBrowser //////////////////////

StandardPropertyBrowser::StandardPropertyBrowser(QWidget *parent) :
    AbstractPropertyBrowser(*new StandardPropertyBrowserPrivate(this), parent)
{
    Q_D(StandardPropertyBrowser);

    auto mainLayout = new QVBoxLayout(this);
    mainLayout->setMargin(0);

    auto scrollArea = new QScrollArea(this);
    scrollArea->setWidgetResizable(true);

    mainLayout->addWidget(scrollArea);

    // Placeholder for properties
    d->propertiesWidget = new QWidget(scrollArea);
    d->propertiesLayout = new QFormLayout(d->propertiesWidget);

    scrollArea->setWidget(d->propertiesWidget);
}

void StandardPropertyBrowser::addPropertyField(Core::Property *prop)
{
    Q_D(StandardPropertyBrowser);

    auto propertyFieldLabel = new QLabel(prop->name());
    propertyFieldLabel->setToolTip(prop->toolTip());
    propertyFieldLabel->setStatusTip(prop->statusTip());
    propertyFieldLabel->setWhatsThis(prop->whatsThis());

    QWidget *editor = createEditor(prop, d->propertiesWidget);

    d->propertiesLayout->addRow(propertyFieldLabel, editor);

    // Store the relation [property -> property field]
    StandardPropertyField propertyField {propertyFieldLabel, editor};

    d->propertyToPropertyField.insert(prop, propertyField);
}

void StandardPropertyBrowser::removePropertyField(Core::Property *prop)
{
    Q_D(StandardPropertyBrowser);

    StandardPropertyField propertyField =
            d->propertyToPropertyField.value(prop);

    d->removeRowFromLayout(propertyField);

    const int removedCount = d->propertyToPropertyField.remove(prop);
    Q_ASSERT(removedCount == 1);

    AbstractPropertyBrowser::destroyEditor(prop);
}

void StandardPropertyBrowser::updatePropertyField(Core::Property *prop)
{
    Q_D(StandardPropertyBrowser);

    const QString propName = prop->name();
    const QString propStatusTip = prop->statusTip();
    const QString propWhatsThis = prop->whatsThis();

    const StandardPropertyField propField =
            d->propertyToPropertyField.value(prop);

    QLabel *propFieldLabel = propField.label;
    Q_ASSERT(propFieldLabel);

    if (propName != propFieldLabel->text()) {
        propFieldLabel->setText(propName);
    }
    else if (propStatusTip != propFieldLabel->statusTip()) {
        propFieldLabel->setStatusTip(propStatusTip);
    }
    else if (propWhatsThis != propFieldLabel->whatsThis()) {
        propFieldLabel->setWhatsThis(propWhatsThis);
    }
}

} // namespace Widgets
} // namespace PE
