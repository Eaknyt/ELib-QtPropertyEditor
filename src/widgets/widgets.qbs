import qbs

import Reusables

Reusables.PublicLibrary {
    name: "PEWidgets"
    targetName: "pewidgets"

    Depends { name: "PECore" }

    Depends {
        name: "Qt"
        submodules: [ "core", "gui", "widgets" ]
    }

    cpp.defines: "PEWIDGETS_LIB"
    cpp.cxxLanguageVersion: project.cppLanguageVersion
    cpp.includePaths: [ project.includePath ]

    files: [
        "private/abstractpropertybrowser_p.h",
        "private/mvpropertybrowseritemdelegate.cpp",
        "private/mvpropertybrowseritemdelegate_p.h",
        "abstractpropertybrowser.cpp",
        "abstractpropertybrowser.h",
        "abstractwidgeteditorfactory.cpp",
        "abstractwidgeteditorfactory.h",
        "pewidgets_global.h",
        "treepropertybrowser.cpp",
        "treepropertybrowser.h",
        "editorfactorymanager.h",
        "editorfactorymanager.cpp",
        "standardpropertybrowser.cpp",
        "standardpropertybrowser.h"
    ]

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: project.libDir
    }
}
