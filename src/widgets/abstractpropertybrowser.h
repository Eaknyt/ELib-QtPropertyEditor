#ifndef PEWIDGETS_ABSTRACTPROPERTYBROWSER_H
#define PEWIDGETS_ABSTRACTPROPERTYBROWSER_H

#include <QtWidgets/QWidget>

#include <PECore/ipropertybrowser.h>

namespace PE {

namespace Core {
class PropertyManager;
} // namespace Core

namespace Widgets {

class AbstractPropertyBrowserPrivate;
class AbstractWidgetEditorFactory;
class EditorFactoryManager;

class AbstractPropertyBrowser : public Core::IPropertyBrowser<QWidget>
{
    Q_OBJECT

    Q_PROPERTY(PE::Core::PropertyManager *manager READ manager WRITE setManager NOTIFY managerChanged)

public:
    virtual ~AbstractPropertyBrowser();

    Core::PropertyManager *manager() const;
    EditorFactoryManager *editorFactoryManager() const;

    QWidget *createEditor(Core::Property *prop, QWidget *parent) const override;
    void destroyEditor(Core::Property *prop) const override;

public Q_SLOTS:
    void setManager(Core::PropertyManager *manager);
    void setEditorFactoryManager(EditorFactoryManager *manager);

Q_SIGNALS:
    void managerChanged(Core::PropertyManager *manager);
    void editorFactoryManagerChanged(EditorFactoryManager *manager);

protected:
    explicit AbstractPropertyBrowser(QWidget *parent = nullptr);
    AbstractPropertyBrowser(AbstractPropertyBrowserPrivate &dd,
                            QWidget *parent = nullptr);

    virtual void connectManager();
    virtual void disconnectManager();

    void buildResettableEditor(Core::Property *prop, QWidget *editor,
                               QWidget *placeholder) const;
    void unbuildResettableEditor(Core::Property *prop, QWidget *editor,
                                 QWidget *placeholder) const;
private Q_SLOTS:
    void onPropertyAdded(Core::Property *prop);
    void onPropertyRemoved(Core::Property *prop);
    void onPropertyChanged(Core::Property *prop);
    void onPropertyValueChanged(Core::Property *prop,
                                const QVariant &value);
    void onPropertyHintChanged(Core::Property *prop, const QString &name,
                               const QVariant &value);
    void onPropertyReadOnlyChanged(Core::Property *prop, bool readOnly);
    void onPropertyResetChanged(Core::Property *prop, bool resettable);
    void onPropertyDefaultValueChanged(Core::Property *prop,
                                       const QVariant &defaultValue);

protected:
    Q_DISABLE_COPY(AbstractPropertyBrowser)
    Q_DECLARE_PRIVATE(AbstractPropertyBrowser)

    const QScopedPointer<AbstractPropertyBrowserPrivate> d_ptr;
};

} // namespace Widgets
} // namespace PE

#endif // PEWIDGETS_ABSTRACTPROPERTYBROWSER_H
