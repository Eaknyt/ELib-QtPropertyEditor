#ifndef PEWIDGETS_GLOBAL_H
#define PEWIDGETS_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(PEWIDGETS_LIB)
#  define PEWIDGETS_EXPORT Q_DECL_EXPORT
#else
#  define PEWIDGETS_EXPORT Q_DECL_IMPORT
#endif

#endif // PEWIDGETS_GLOBAL_H
