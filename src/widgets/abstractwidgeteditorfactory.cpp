#include "abstractwidgeteditorfactory.h"

#include <QtCore/QMap>

#include <QtWidgets/QWidget>

#include <PECore/property.h>

namespace PE {

using namespace Core;

namespace Widgets {

using PropertyToEditor = QMap<Property *, QWidget *>;
using EditorToProperty = QMap<QWidget *, Property *>;


////////////////////// AbstractWidgetEditorFactoryPrivate //////////////////////

class AbstractWidgetEditorFactoryPrivate
{
public:
    AbstractWidgetEditorFactoryPrivate(AbstractWidgetEditorFactory *qq);

    void registerPropertyAndEditor(Property *prop, QWidget *editor);
    PropertyToEditor::iterator unregisterProperty(PropertyToEditor::iterator it);

    void addEditorHints(QWidget *editor, const QVariantMap &hints) const;

    AbstractWidgetEditorFactory *q_ptr;
    PropertyToEditor propertyToEditor;
    EditorToProperty editorToProperty;
};

AbstractWidgetEditorFactoryPrivate::AbstractWidgetEditorFactoryPrivate
(AbstractWidgetEditorFactory *qq) :
    q_ptr(qq),
    propertyToEditor(),
    editorToProperty()
{}

void AbstractWidgetEditorFactoryPrivate::registerPropertyAndEditor(Property *prop,
                                                                   QWidget *editor)
{
    propertyToEditor.insert(prop, editor);
    editorToProperty.insert(editor, prop);
}

PropertyToEditor::iterator AbstractWidgetEditorFactoryPrivate::unregisterProperty
(PropertyToEditor::iterator it)
{
    if (it == propertyToEditor.end()) {
        return it;
    }

    QWidget *editor = (*it);

    const int removedCount = editorToProperty.remove(editor);
    Q_ASSERT(removedCount == 1);

    if (editor->isVisible()) {
        editor->close();
    }

    editor->disconnect();
    editor->deleteLater();

    auto ret = propertyToEditor.erase(it);

    return ret;
}

void AbstractWidgetEditorFactoryPrivate::addEditorHints
(QWidget *editor, const QVariantMap &hints) const
{
    QVariantMap::const_iterator it = hints.cbegin();

    for (; it != hints.cend(); ++it) {
        q_ptr->updateEditorHint(editor, it.key(), it.value());
    }
}


////////////////////// AbstractWidgetEditorFactory //////////////////////

AbstractWidgetEditorFactory::AbstractWidgetEditorFactory(QObject *parent) :
    QObject(parent),
    d(new AbstractWidgetEditorFactoryPrivate(this))
{}

AbstractWidgetEditorFactory::~AbstractWidgetEditorFactory()
{
    auto it = d->propertyToEditor.begin();

    while (it != d->propertyToEditor.end()) {
        it = d->unregisterProperty(it);
    }
}

QWidget *AbstractWidgetEditorFactory::addProperty(Property *prop,
                                                  QWidget *parent)
{
    Q_ASSERT(type() == prop->type());

    QWidget *editor = createEditor(prop, parent);

    d->registerPropertyAndEditor(prop, editor);

    updateEditor(editor, prop->value());

    d->addEditorHints(editor, prop->hints());

    return editor;
}

void AbstractWidgetEditorFactory::removeProperty(Property *prop)
{
    PropertyToEditor::iterator it = d->propertyToEditor.find(prop);

    d->unregisterProperty(it);
}

QWidget *AbstractWidgetEditorFactory::editorFromProperty(Property *prop) const
{
    return d->propertyToEditor.value(prop);
}

Property *AbstractWidgetEditorFactory::propertyFromEditor(QWidget *editor) const
{
    return d->editorToProperty.value(editor);
}

void AbstractWidgetEditorFactory::updateEditorValue(Property *prop,
                                                    const QVariant &value) const
{
    QWidget *editor = editorFromProperty(prop);

    if (editor) {
        updateEditor(editor, value);
    }
}

void AbstractWidgetEditorFactory::setEditorHint(Property *prop,
                                                const QString &name,
                                                const QVariant &value) const
{
    QWidget *editor = editorFromProperty(prop);

    if (editor) {
        updateEditorHint(editor, name, value);
    }
}

void AbstractWidgetEditorFactory::updateEditorHint(QWidget *editor,
                                                   const QString &name,
                                                   const QVariant &value) const
{
    Q_UNUSED(editor);
    Q_UNUSED(name);
    Q_UNUSED(value);
}

void AbstractWidgetEditorFactory::updateProperty(const QVariant &value)
{
    auto editor = qobject_cast<QWidget *>(sender());

    Property *prop = propertyFromEditor(editor);
    prop->setValue(value);
}

} // namespace Widgets
} // namespace PE
