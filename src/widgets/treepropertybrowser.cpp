#include "treepropertybrowser.h"

#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>

#include <PECore/property.h>
#include <PECore/propertymanager.h>
#include <PECore/propertyitemmodel.h>

#include <PEWidgets/private/abstractpropertybrowser_p.h>
#include <PEWidgets/private/mvpropertybrowseritemdelegate_p.h>

namespace PE {

using namespace Core;

namespace Widgets {


////////////////////// TreePropertyBrowserPrivate //////////////////////

class TreePropertyBrowserPrivate : public AbstractPropertyBrowserPrivate
{
public:
    TreePropertyBrowserPrivate(TreePropertyBrowser *qq);
    PropertyItemModel *model;
    QTreeView *treeView;

    QMap<Property *, PropertyItem *> propertyToItem;
};

TreePropertyBrowserPrivate::TreePropertyBrowserPrivate(TreePropertyBrowser *qq) :
    AbstractPropertyBrowserPrivate(qq),
    model(nullptr),
    treeView(nullptr),
    propertyToItem()
{

}


////////////////////// TreePropertyBrowser //////////////////////

/*!
 \class The TreePropertyBrowser class provides
*/

TreePropertyBrowser::TreePropertyBrowser(QWidget *parent) :
    AbstractPropertyBrowser(*new TreePropertyBrowserPrivate(this), parent)
{
    Q_D(TreePropertyBrowser);

    d->model = new PropertyItemModel(this);

    d->treeView = new QTreeView(this);
    d->treeView->setAlternatingRowColors(true);
    d->treeView->setModel(d->model);

    connect(d->treeView->selectionModel(), &QItemSelectionModel::currentChanged,
            [=] (const QModelIndex &current, const QModelIndex &previous) {
        Q_UNUSED(previous);

        PropertyItem *propItem = d->model->item(current);

        Q_EMIT propertySelected(propItem);
    });

    auto delegate = new MVPropertyBrowserItemDelegate(this);
    d->treeView->setItemDelegateForColumn(1, delegate);

    auto mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);

    mainLayout->addWidget(d->treeView);
}

/*!
 \brief

*/
TreePropertyBrowser::~TreePropertyBrowser()
{}

void TreePropertyBrowser::setHeaderNames(const QString &propertyHeader,
                                         const QString &valueHeader)
{
    Q_D(TreePropertyBrowser);

    d->model->setHeaderData(0, Qt::Horizontal, propertyHeader, Qt::DisplayRole);
    d->model->setHeaderData(1, Qt::Horizontal, valueHeader, Qt::DisplayRole);
}

PropertyItem *TreePropertyBrowser::itemFromProperty(Property *prop) const
{
    Q_D(const TreePropertyBrowser);

    return d->propertyToItem.value(prop);
}

Property *TreePropertyBrowser::propertyFromItem(PropertyItem *item) const
{
    Q_D(const TreePropertyBrowser);

    return d->propertyToItem.key(item);
}

PropertyItem *TreePropertyBrowser::currentItem() const
{
    Q_D(const TreePropertyBrowser);

    const QModelIndex currentIndex = d->treeView->currentIndex();

    PropertyItem *ret = d->model->item(currentIndex);

    return ret;
}

void TreePropertyBrowser::setCurrentItem(PropertyItem *item)
{
    Q_D(TreePropertyBrowser);

    const QModelIndex index = d->model->indexFromItem(item, 0);

    if (!index.isValid()) {
        return;
    }

    d->treeView->setCurrentIndex(index);
}

/*!
 \brief

*/
void TreePropertyBrowser::addPropertyField(Property *prop)
{
    Q_D(TreePropertyBrowser);

    auto item = new PropertyItem(prop);

    d->model->addItem(item);

    d->propertyToItem.insert(prop, item);
}

/*!
 \brief

*/
void TreePropertyBrowser::removePropertyField(Property *prop)
{
    Q_D(TreePropertyBrowser);

    PropertyItem *toRemove = d->propertyToItem.value(prop);
    Q_ASSERT(toRemove);

    const int removed = d->propertyToItem.remove(prop);
    Q_ASSERT(removed);

    d->model->removeItem(toRemove);
}

/*!
 \brief

*/
void TreePropertyBrowser::updatePropertyField(Property *prop)
{
    Q_D(TreePropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItem(item);
}

/*!
 \brief

*/
void TreePropertyBrowser::updatePropertyFieldValue(Property *prop,
                                                   const QVariant &value)
{
    Q_UNUSED(value);

    Q_D(TreePropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemValue(item);
}

/*!
 \brief

*/
void TreePropertyBrowser::updatePropertyFieldHint(Property *prop,
                                                  const QString &name,
                                                  const QVariant &value)
{
    Q_UNUSED(name)
    Q_UNUSED(value);

    Q_D(TreePropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemHints(item);
}

void TreePropertyBrowser::updatePropertyFieldReadOnly(Property *prop,
                                                      bool readOnly)
{
    Q_UNUSED(readOnly);

    Q_D(TreePropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemReadOnly(item);
}

void TreePropertyBrowser::updatePropertyFieldResettable(Property *prop,
                                                        bool resettable)
{
    Q_UNUSED(resettable);

    Q_D(TreePropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemResettable(item);
}

void TreePropertyBrowser::updatePropertyFieldDefaultValue
(Property *prop, const QVariant &defaultValue)
{
    Q_UNUSED(defaultValue);

    Q_D(TreePropertyBrowser);

    PropertyItem *item = d->propertyToItem.value(prop);
    Q_ASSERT(item);

    d->model->updateItemDefaultValue(item);
}

} // namespace Widgets
} // namespace PE
