#ifndef PEWIDGETS_WIDGETEDITORFACTORY_H
#define PEWIDGETS_WIDGETEDITORFACTORY_H

#include <QtCore/QObject>

#include <PEWidgets/pewidgets_global.h>

class QWidget;

namespace PE {

namespace Core {
class Property;
} // namespace Core

namespace Widgets {

class AbstractWidgetEditorFactoryPrivate;

class PEWIDGETS_EXPORT AbstractWidgetEditorFactory : public QObject
{
    Q_OBJECT

public:
    explicit AbstractWidgetEditorFactory(QObject *parent = nullptr);
    ~AbstractWidgetEditorFactory();

    virtual int type() const = 0;

    QWidget *addProperty(Core::Property *prop, QWidget *parent);
    void removeProperty(Core::Property *prop);

    QWidget *editorFromProperty(Core::Property *prop) const;
    Core::Property *propertyFromEditor(QWidget *editor) const;

    void updateEditorValue(Core::Property *prop, const QVariant &value) const;
    void setEditorHint(Core::Property *prop, const QString &name,
                       const QVariant &value) const;

protected:
    virtual QWidget *createEditor(Core::Property *prop,
                                  QWidget *parent) const = 0;
    virtual void updateEditor(QWidget *editor, const QVariant &value) const = 0;
    virtual void updateEditorHint(QWidget *editor, const QString &name,
                                  const QVariant &value) const;

protected Q_SLOTS:
    void updateProperty(const QVariant &value);

private:
    friend class AbstractWidgetEditorFactoryPrivate;
    const QScopedPointer<AbstractWidgetEditorFactoryPrivate> d;
};

template <typename T>
class PEWIDGETS_EXPORT WidgetEditorFactory : public AbstractWidgetEditorFactory
{
public:
    explicit WidgetEditorFactory(QObject *parent = nullptr) :
        AbstractWidgetEditorFactory(parent)
    {}

    constexpr int type() const override final
    {
        return qMetaTypeId<T>();
    }
};

} // namespace Widgets
} // namespace PE

#endif // PEWIDGETS_WIDGETEDITORFACTORY_H
