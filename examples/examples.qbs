import qbs

Project {
    name: "Examples"

    condition: project.enableExamples

    references: [
        "parametereditor-widgets/parametereditor-widgets.qbs"
    ]
}
