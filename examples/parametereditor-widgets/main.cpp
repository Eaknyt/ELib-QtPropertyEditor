#include <QtCore/QDebug>

#include <QtWidgets/QApplication>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

#include <PECore/abstractpropertyhandler.h>
#include <PECore/property.h>
#include <PECore/propertyfactory.h>
#include <PECore/propertyitemmodel.h>
#include <PECore/propertymanager.h>

#include <PECoreExtras/enumtype.h>
#include <PECoreExtras/propertyfactories.h>

#include <PEWidgets/standardpropertybrowser.h>
#include <PEWidgets/treepropertybrowser.h>
#include <PEWidgets/editorfactorymanager.h>

#include <PEWidgetsExtras/editorfactorymanagers.h>


using namespace PE;


////////////////////// MainWindow //////////////////////

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    void createPropertyBrowsers();
    void createConnections();
    void assembleUI();

    void removeParameter(Core::PropertyItem *propItem);

private Q_SLOTS:
    void addParameter(QListWidgetItem *item);
    void showParameterProperties(Core::Property *prop);

    void onParameterPropertyValueChanged(Core::Property *prop,
                                         const QVariant &value);

private:
    QListWidget *m_typesListWidget;

    Core::PropertyFactory m_propertyFactory;

    Widgets::TreePropertyBrowser *m_parameterView;
    Core::PropertyManager *m_parameterViewManager;

    Widgets::AbstractPropertyBrowser *m_parameterEditor;
    Core::PropertyManager *m_parameterEditorManager;

    Core::Property *m_parameterNameProp;
    Core::Property *m_parameterDescProp;
    Core::Property *m_parameterStatusTipProp;
    Core::Property *m_parameterWhatsThisProp;
    Core::Property *m_parameterIsReadOnlyProp;
    Core::Property *m_parameterIsResettableProp;
    Core::Property *m_parameterDefaultValueProp;

    Core::Property *m_currentSelectedParameter;
};


MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    m_typesListWidget(new QListWidget(this)),
    m_propertyFactory(),
    m_parameterView(nullptr),
    m_parameterViewManager(nullptr),
    m_parameterEditor(nullptr),
    m_parameterEditorManager(nullptr),
    m_parameterNameProp(nullptr),
    m_parameterDescProp(nullptr),
    m_parameterStatusTipProp(nullptr),
    m_parameterWhatsThisProp(nullptr),
    m_parameterIsReadOnlyProp(nullptr),
    m_parameterIsResettableProp(nullptr),
    m_parameterDefaultValueProp(nullptr),
    m_currentSelectedParameter(nullptr)
{
    createPropertyBrowsers();
    createConnections();
    assembleUI();
}

void MainWindow::createPropertyBrowsers()
{
    Core::PropertyFactory::setDefaultFactory(Core::Extras::qtPropertyFactory());

    Widgets::EditorFactoryManager *editorFactoryManager =
            Widgets::Extras::qtEditorFactoryManager();

    m_parameterView = new Widgets::TreePropertyBrowser(this);
    m_parameterView->setEditorFactoryManager(editorFactoryManager);
    m_parameterView->setHeaderNames("Parameter", "Value");

    m_parameterViewManager = new Core::PropertyManager(m_parameterView);

    m_parameterEditor = new Widgets::StandardPropertyBrowser(this);
    m_parameterEditor->setEditorFactoryManager(editorFactoryManager);

    m_parameterEditorManager = new Core::PropertyManager(m_parameterEditor);

    for (const int type : Core::PropertyFactory::defaultFactory()->supportedTypes()) {
        // Populate the list widget with available parameter types
        const QString typeName = QMetaType::typeName(type);

        auto item = new QListWidgetItem(typeName, m_typesListWidget);
        item->setData(Qt::UserRole, type);

        m_typesListWidget->addItem(item);
    }

    // Create the property managers
    m_parameterEditor->setManager(m_parameterEditorManager);
    m_parameterView->setManager(m_parameterViewManager);
}

void MainWindow::createConnections()
{
    connect(m_typesListWidget, &QListWidget::itemActivated,
            this, &MainWindow::addParameter);

    connect(m_parameterEditorManager, &Core::PropertyManager::propertyValueChanged,
            this, &MainWindow::onParameterPropertyValueChanged);

    connect(m_parameterView, &Widgets::TreePropertyBrowser::propertySelected,
            [=] (Core::PropertyItem *propItem) {
        Core::Property *prop = (propItem) ? propItem->property() : nullptr;

        showParameterProperties(prop);
    });
}

void MainWindow::assembleUI()
{
    m_typesListWidget->setAlternatingRowColors(true);

    auto typesListPlaceholder = new QWidget(this);
    auto typesListPlaceholderLayout = new QVBoxLayout(typesListPlaceholder);
    typesListPlaceholderLayout->addWidget(new QLabel("Create Parameter"));
    typesListPlaceholderLayout->addWidget(m_typesListWidget);

    auto createOrRemoveParamWidget = new QWidget(this);
    auto createOrRemoveParamWidgetLayout = new QVBoxLayout(createOrRemoveParamWidget);

    auto createParamBtn = new QToolButton(createOrRemoveParamWidget);
    createParamBtn->setText("->");

    connect(createParamBtn, &QToolButton::clicked,
            [=] () {
        QListWidgetItem *currentItem = m_typesListWidget->currentItem();

        addParameter(currentItem);
    });

    auto removeParamBtn = new QToolButton(createOrRemoveParamWidget);
    removeParamBtn->setText("<-");

    connect(removeParamBtn, &QToolButton::clicked,
            [=] () {
        Core::PropertyItem *currentItem = m_parameterView->currentItem();

        if (currentItem) {
            removeParameter(currentItem);
        }
    });

    createOrRemoveParamWidgetLayout->addStretch();
    createOrRemoveParamWidgetLayout->addWidget(createParamBtn);
    createOrRemoveParamWidgetLayout->addWidget(removeParamBtn);
    createOrRemoveParamWidgetLayout->addStretch();

    auto paramViewPlaceholder = new QWidget(this);
    auto paramViewPlaceholderLayout = new QVBoxLayout(paramViewPlaceholder);
    paramViewPlaceholderLayout->addWidget(new QLabel("Parameters"));
    paramViewPlaceholderLayout->addWidget(m_parameterView);

    auto paramEditorPlaceholder = new QWidget(this);
    auto paramEditorPlaceholderLayout = new QVBoxLayout(paramEditorPlaceholder);
    paramEditorPlaceholderLayout->addWidget(new QLabel("Parameter description"));
    paramEditorPlaceholderLayout->addWidget(m_parameterEditor);

    auto mainLayout = new QHBoxLayout(this);
    mainLayout->addWidget(typesListPlaceholder);
    mainLayout->addWidget(createOrRemoveParamWidget);
    mainLayout->addWidget(paramViewPlaceholder);
    mainLayout->addWidget(paramEditorPlaceholder);
}

void MainWindow::removeParameter(Core::PropertyItem *propItem)
{
    if (!propItem) {
        return;
    }

    Core::Property *prop = propItem->property();

    m_parameterViewManager->removeProperty(prop);
}

void MainWindow::addParameter(QListWidgetItem *item)
{
    const int propType = item->data(Qt::UserRole).toInt();

    Core::Property *prop = m_parameterViewManager->addProperty(propType,
                                                               "New parameter");

    m_parameterView->setCurrentItem(m_parameterView->itemFromProperty(prop));
}

void MainWindow::showParameterProperties(Core::Property *prop)
{
    if (!prop) {
        m_parameterEditorManager->clear();
        return;
    }

    if (m_currentSelectedParameter == prop) {
        return;
    }

    m_parameterEditorManager->clear();

    m_currentSelectedParameter = prop;

    // Add parameter properties
    m_parameterNameProp = m_parameterEditorManager->addProperty<QString>("Name");
    m_parameterNameProp->setValue(m_currentSelectedParameter->name());

    m_parameterDescProp = m_parameterEditorManager->addProperty<QString>("ToolTip");
    m_parameterDescProp->setValue(m_currentSelectedParameter->toolTip());

    m_parameterStatusTipProp = m_parameterEditorManager->addProperty<QString>("Status tip");
    m_parameterStatusTipProp->setValue(m_currentSelectedParameter->statusTip());

    m_parameterWhatsThisProp = m_parameterEditorManager->addProperty<QString>("Whats this ?");
    m_parameterWhatsThisProp->setValue(m_currentSelectedParameter->whatsThis());

    m_parameterIsReadOnlyProp = m_parameterEditorManager->addProperty<bool>("Read only");
    m_parameterIsReadOnlyProp->setValue(m_currentSelectedParameter->isReadOnly());

    m_parameterIsResettableProp = m_parameterEditorManager->addProperty<bool>("Resettable");
    m_parameterIsResettableProp->setValue(m_currentSelectedParameter->isResettable());

    m_parameterDefaultValueProp = m_parameterEditorManager->addProperty(m_currentSelectedParameter->type(), "Default value");
    m_parameterDefaultValueProp->setValue(m_currentSelectedParameter->defaultValue());

    // Add parameter hints
    const QVariantMap hints = m_currentSelectedParameter->hints();

    for (auto hintIt = hints.begin(); hintIt != hints.end(); ++hintIt) {
        const QString hintName = hintIt.key();
        const QVariant hintValue = hintIt.value();
        const int hintType = hintValue.userType();

        Core::Property *prop = m_parameterEditorManager->addProperty(hintType,
                                                                     hintName);
        prop->setValue(hintValue);
    }
}

void MainWindow::onParameterPropertyValueChanged(Core::Property *prop,
                                                 const QVariant &value)
{
    const QString propName = prop->name();

    if (prop == m_parameterNameProp) {
        m_currentSelectedParameter->setName(value.toString());
    }
    else if (prop == m_parameterDescProp) {
        m_currentSelectedParameter->setToolTip(value.toString());
    }
    else if (prop == m_parameterStatusTipProp) {
        m_currentSelectedParameter->setStatusTip(value.toString());
    }
    else if (prop == m_parameterWhatsThisProp) {
        m_currentSelectedParameter->setWhatsThis(value.toString());
    }
    else if (prop == m_parameterIsReadOnlyProp) {
        m_currentSelectedParameter->setReadOnly(value.toBool());
    }
    else if (prop == m_parameterIsResettableProp) {
        m_currentSelectedParameter->setResettable(value.toBool());
    }
    else if (prop == m_parameterDefaultValueProp) {
        m_currentSelectedParameter->setDefaultValue(value);
    }
    else if (m_currentSelectedParameter->hasHint(propName)) {
        m_currentSelectedParameter->setHint(propName, value);
    }
}


////////////////////// main //////////////////////

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    auto mainWindow = new MainWindow;
    mainWindow->show();

    return app.exec();
}

#include "main.moc"
