import qbs

import PE

PE.Example {
    name: "parametereditor-widgets"

    Depends {
        name: "Qt"
        submodules: ["core", "gui", "widgets"]
    }

    Depends { name: "PECore" }
    Depends { name: "PECoreExtras" }
    Depends { name: "PEWidgets" }
    Depends { name: "PEWidgetsExtras" }

    files: [
        "main.cpp",
    ]
}
