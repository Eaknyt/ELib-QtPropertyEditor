ELib Qt PropertyEditor
======================

A Qt library to implement property browsers for QtQuick and QtWidgets.

Requirements
------------

- Qt >= 5.7.0

- qbs >= 1.6

Note
-----------

- WIP !

- CMake support is not a high priority feature. 
