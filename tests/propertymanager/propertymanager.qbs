import qbs
import qbs.FileInfo

import PE

PE.Test {
    name: "PropertyManagerTests"

    files: [
        "tst_propertymanager.cpp",
    ]
}
