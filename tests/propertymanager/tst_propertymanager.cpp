#include <QtTest/QtTest>

#include <PECore/property.h>
#include <PECore/propertymanager.h>

using namespace PE::Core;


////////////////////// MyCustomRegisteredType //////////////////////

struct MyCustomRegisteredType
{
    int m_fooData = 0;
};
Q_DECLARE_METATYPE(MyCustomRegisteredType)


////////////////////// TestPropertyManagement //////////////////////

class TestPropertyManager : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void cleanup();

    void checkPropertyTypes();
    void addPropertyDirectlyFromManager();
    void addPropertyManuallyToManager();
    void removeProperties();
    void setPropertyValue();
    void setPropertyDefaultValue();

private:
    PropertyManager *m_manager;
};

void TestPropertyManager::initTestCase()
{
    m_manager = new PropertyManager(this);
}

void TestPropertyManager::cleanup()
{
    m_manager->clear();
}

void TestPropertyManager::checkPropertyTypes()
{
    // Int property
    //  WHEN
    Property *prop = m_manager->addProperty(QMetaType::Int, "");

    //  THEN
    QVERIFY(prop);

    // String property
    //  WHEN
    prop = m_manager->addProperty(QMetaType::QString, "");

    //  THEN
    QVERIFY(prop);

    // Invalid property
    //  WHEN
    prop = m_manager->addProperty(QMetaType::UnknownType, "");

    //  THEN
    QVERIFY(!prop);

    // User-defined type property
    //  WHEN
    prop = m_manager->addProperty(qMetaTypeId<MyCustomRegisteredType>(), "");

    //  THEN
    QVERIFY(prop);

    // Invalid user-defined type property
    //  WHEN
    prop = m_manager->addProperty(QMetaType::User + 1000, "");

    //  THEN
    QVERIFY(!prop);
}

void TestPropertyManager::addPropertyDirectlyFromManager()
{
    // WHEN
    Property *prop = m_manager->addProperty(QMetaType::Int, "first prop");
    prop->setToolTip("This first property is for testing");

    // THEN
    QCOMPARE(m_manager->count(), 1);

    // WHEN
    prop = m_manager->addProperty(QMetaType::QString, "second prop");
    prop->setStatusTip("A dummy status for this second property");

    // THEN
    QCOMPARE(m_manager->count(), 2);

    // WHEN
    m_manager->clear();

    // THEN
    QCOMPARE(m_manager->count(), 0);
}

void TestPropertyManager::addPropertyManuallyToManager()
{
    // WHEN
    auto prop = std::make_unique<Property>(QMetaType::Int, "int prop");

    Property *validPropHandle = m_manager->addProperty(std::move(prop));

    // THEN
    QVERIFY(validPropHandle);
    QVERIFY(m_manager->contains(validPropHandle));

    // WHEN
    m_manager->removeProperty(validPropHandle);

    // THEN
    QVERIFY(!validPropHandle);
}

void TestPropertyManager::removeProperties()
{
    // WHEN
    Property *prop1 = m_manager->addProperty(QMetaType::Int, "int prop");

    m_manager->removeProperty(prop1);

    // THEN
    QVERIFY(!prop1);
    QVERIFY(!m_manager->contains(prop1));
}

void TestPropertyManager::setPropertyValue()
{
    // Int test
    //  WHEN
    Property *intProp = m_manager->addProperty(QMetaType::Int, "int prop");

    m_manager->setValue(intProp, 2);

    //  THEN
    QCOMPARE(intProp->value(), QVariant::fromValue<int>(2));

    // QString test
    //  WHEN
    Property *stringProp = m_manager->addProperty(QMetaType::QString,
                                                     "string prop");

    m_manager->setValue(stringProp, "some text");

    //  THEN
    QCOMPARE(stringProp->value(),
             QVariant::fromValue<QString>("some text"));

    // Default constructed value test
    //  WHEN
    intProp = m_manager->addProperty(QMetaType::Int, "");

    //  THEN
    QVERIFY(intProp->value().isNull());
}

void TestPropertyManager::setPropertyDefaultValue()
{
    // WHEN
    Property *intProp = m_manager->addProperty(QMetaType::Int, "int prop");
    intProp->setResettable(true);

    // THEN
    QVERIFY(intProp->defaultValue().isNull());

    // WHEN
    const int intPropDefaultVal = 5;
    intProp->setDefaultValue(intPropDefaultVal);

    // THEN
    QCOMPARE(intProp->defaultValue().toInt(), intPropDefaultVal);

    // WHEN
    intProp->setValue(2);
    intProp->reset();

    // THEN
    QCOMPARE(intProp->value().toInt(), intPropDefaultVal);
}

QTEST_MAIN(TestPropertyManager)

#include "tst_propertymanager.moc"
