#include <QtTest/QtTest>

#include <PECore/property.h>
#include <PECore/propertyitemmodel.h>
#include <PECore/propertymanager.h>

using namespace PE::Core;


////////////////////// TestPropertyModel //////////////////////

class TestPropertyModel : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void cleanup();

private:
    PropertyManager *m_manager;
    PropertyItemModel *m_model;
};

void TestPropertyModel::initTestCase()
{
    m_manager = new PropertyManager(this);
    m_model = new PropertyItemModel(this);
}

void TestPropertyModel::cleanup()
{
    m_manager->clear();
    m_model->clear();
}

QTEST_MAIN(TestPropertyModel)

#include "tst_propertymodel.moc"
