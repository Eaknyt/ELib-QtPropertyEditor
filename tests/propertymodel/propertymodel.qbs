import qbs
import qbs.FileInfo

import PE

PE.Test {
    name: "PropertyModelTests"

    files: [
        "tst_propertymodel.cpp",
    ]
}
