import qbs

Project {
    name: "Tests"

    condition: project.enableTests

    references: [
        "propertymanager/propertymanager.qbs",
        "propertymodel/propertymodel.qbs"
    ]
}
